<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username');
            $table->string('password');
            $table->string('email', 100)->unique();
            $table->string('full_name', 100);
            $table->string('avatar', 100)->nullable();
            $table->string('active_code', 100);
            $table->string('group_id', 100);
            $table->boolean('is_root');
            $table->enum('status', ['inactive', 'deactive', 'active', 'blocked']);
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('last_visit')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
