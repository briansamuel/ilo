<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


if (!defined('FM_USE_ACCESS_KEYS')) {
    define('FM_USE_ACCESS_KEYS', true); // TRUE or FALSE
}

if (!defined('FM_DEBUG_ERROR_MESSAGE')) {
    define('FM_DEBUG_ERROR_MESSAGE', false); // TRUE or FALSE
}

Route::domain(Config::get('domain.web.domain'))->group(function () {
    Route::namespace('FrontSite')->group(function () {
        Route::get('/', ['uses' => 'HomeController@index'])->name('frontsite.home');
        Route::get('/about.html', ['uses' => 'AboutController@index'])->name('frontsite.about');
        Route::get('/curriculum.html', ['uses' => 'CurriculumController@index'])->name('frontsite.curriculum');
        Route::get('/enrollment.html', ['uses' => 'EnrollmentController@index'])->name('frontsite.enrollment');
        Route::get('/news-event.html', ['uses' => 'NewsController@index'])->name('frontsite.news');
        Route::get('/contact.html', ['uses' => 'ContactController@index'])->name('frontsite.contact');
        Route::post('/send-contact', ['uses' => 'ContactController@sendContact'])->name('frontsite.sendContact');
        Route::get('refresh-captcha', 'CaptchaController@refreshCaptcha');
    
        Route::get('/{slug}.html', ['uses' => 'NewsController@newsDetail'])->name('frontsite.news.detail');
//        Route::get('/{slug}', ['uses' => 'PageController@index'])->name('frontsite.page');
        
        Route::get('/slideImage/{slug}.{ext}', ['uses' => 'CropImageController@slideImage'])->name('frontsite.slideImage');

        // Đổi ngôn ngữ
        Route::get('lang/{locale}', 'LangController@changeLang')->name('lang');
    });
});

Route::prefix('admincp')->group(function () {
    Route::namespace('Admin')->group(function () {
        Route::get('denied-permission', function () {
            return view('admin.pages.permission_denied');
        })->name('permission_denied');
        // login
        Route::get('/login', ['uses' => 'LoginController@login'])->name('login');
        Route::post('/login', ['uses' => 'LoginController@loginAction'])->name('loginAction');
        Route::get('/logout', ['uses' => 'LoginController@logout'])->name('logout');
        Route::get('/reset_password', ['uses' => 'LoginController@resetPassword'])->name('resetPassword');
        Route::get('/active-user', ['uses' => 'LoginController@activeUser'])->name('activeUser');
        Route::get('/active-agent', ['uses' => 'LoginController@activeAgent'])->name('activeAgent');
        Route::get('/kich-hoat-tai-khoan', ['uses' => 'LoginController@activeGuest'])->name('activeGuest');

        
    });




    Route::middleware(['auth'])->group(function () {


        Route::get('/dashboard', ['uses' => 'Admin\DashboardController@index'])->name('dash-board');
        Route::get('/welcome', ['uses' => 'Admin\WelcomeController@index'])->name('welcome');
        Route::get('/', ['uses' => 'Admin\WelcomeController@index'])->name('home');

        Route::namespace('Admin')->group(function () {
            // my profile
            Route::get('/my-profile', ['uses' => 'MyProfileController@profile'])->name('profile');
            Route::post('/my-profile', ['uses' => 'MyProfileController@updateProfile'])->name('profile.update');

            // change password
            Route::get('/change-password', ['uses' => 'MyProfileController@changePassword'])->name('profile.changePassword');
            Route::put('/change-password', ['uses' => 'MyProfileController@changePasswordAction'])->name('profile.changePasswordAction');

            Route::namespace('CMS')->group(function () {
                
                // Banner
                Route::get('/banner', ['uses' => 'BannersController@index'])->name('banner.list');
                Route::get('/banner/add', ['uses' => 'BannersController@add'])->name('banner.add');
                Route::post('/banner/add', ['uses' => 'BannersController@addAction'])->name('banner.add.action');
                Route::get('/banner/edit/{banner_id}', ['uses' => 'BannersController@edit'])->name('banner.edit');
                Route::post('/banner/edit/{banner_id}', ['uses' => 'BannersController@editAction'])->name('banner.edit.action');
                Route::get('/banner/delete/{banner_id}', ['uses' => 'BannersController@delete'])->name('banner.delete');
                Route::get('/banner/ajax/get-list', ['uses' => 'BannersController@ajaxGetList'])->name('banner.ajax.getList');

                // Block
                Route::get('/block', ['uses' => 'BlocksController@index'])->name('block.list');
                Route::get('/block/add', ['uses' => 'BlocksController@add'])->name('block.add');
                Route::post('/block/add', ['uses' => 'BlocksController@addAction'])->name('block.add.action');
                Route::get('/block/config/{block_id}', ['uses' => 'BlocksController@config'])->name('block.config');
                Route::post('/block/config/{block_id}', ['uses' => 'BlocksController@configAction'])->name('block.config.action');
                Route::get('/block/edit/{block_id}', ['uses' => 'BlocksController@edit'])->name('block.edit');
                Route::post('/block/edit/{block_id}', ['uses' => 'BlocksController@editAction'])->name('block.edit.action');
                Route::get('/block/delete/{block_id}', ['uses' => 'BlocksController@delete'])->name('block.delete');
                Route::get('/block/ajax/get-list', ['uses' => 'BlocksController@ajaxGetList'])->name('block.ajax.getList');

                // Page
                Route::get('/page', ['uses' => 'PageController@index'])->name('page.list');
                Route::get('/page/add', ['uses' => 'PageController@add'])->name('page.add');
                Route::post('/page/add', ['uses' => 'PageController@add'])->name('page.add.action');
                Route::get('/page/edit/{page_id}', ['uses' => 'PageController@add'])->name('page.edit');
                Route::post('/page/edit/{page_id}', ['uses' => 'PageController@add'])->name('page.edit.action');
                Route::post('/page/edit', ['uses' => 'PageController@editManyAction'])->name('page.edit.many.action');
                Route::get('/page/delete/{page_id}', ['uses' => 'PageController@delete'])->name('page.delete');
                Route::post('/page/delete', ['uses' => 'PageController@deletemany'])->name('page.delete.many');
                Route::post('/page/save', ['uses' => 'PageController@save'])->name('page.save');
                Route::get('/page/ajax/get-list', ['uses' => 'PageController@ajaxGetList'])->name('page.ajax.getList');

                // News
                Route::get('/news', ['uses' => 'NewsController@index'])->name('news.list');
                Route::get('/news/add', ['uses' => 'NewsController@add'])->name('news.add');
                Route::get('/news/edit/{news_id}', ['uses' => 'NewsController@add'])->name('news.edit');
                Route::post('/news/edit', ['uses' => 'NewsController@editManyAction'])->name('news.edit.many.action');
                Route::get('/news/delete/{news_id}', ['uses' => 'NewsController@delete'])->name('news.delete');
                Route::post('/news/delete', ['uses' => 'NewsController@deletemany'])->name('news.delete.many');
                Route::post('/news/save', ['uses' => 'NewsController@save'])->name('news.save');
                Route::get('/news/ajax/get-list', ['uses' => 'NewsController@ajaxGetList'])->name('news.ajax.getList');


                // subcribe Emails
                Route::get('/subcribe-emails', ['uses' => 'SubcribeEmailsController@index'])->name('subcribe_email.list');
                Route::get('/subcribe-emails/edit/{id}', ['uses' => 'SubcribeEmailsController@edit'])->name('subcribe_email.edit');
                Route::post('/subcribe-emails/edit/{id}', ['uses' => 'SubcribeEmailsController@editAction'])->name('subcribe_email.edit.action');
                Route::get('/subcribe-emails/delete/{id}', ['uses' => 'SubcribeEmailsController@delete'])->name('subcribe_email.delete');
                Route::get('/subcribe-emails/ajax/get-list', ['uses' => 'SubcribeEmailsController@ajaxGetList'])->name('subcribe_email.ajax.getList');

                

                // Category
                Route::get('/category', ['uses' => 'CategoryController@index'])->name('category.list');
                Route::post('/category/save', ['uses' => 'CategoryController@save'])->name('category.save');
                Route::get('/category/edit/{category_id}', ['uses' => 'CategoryController@edit'])->name('category.edit');
                Route::post('/category/edit', ['uses' => 'CategoryController@editManyAction'])->name('category.edit.many.action');
                Route::post('/category/edit/{category_id}', ['uses' => 'CategoryController@editAction'])->name('category.edit.action');
                Route::get('/category/delete/{category_id}', ['uses' => 'CategoryController@delete'])->name('category.delete');
                Route::post('/category/delete', ['uses' => 'CategoryController@deleteMany'])->name('category.deleteMany');
                Route::get('/category/ajax/get-list', ['uses' => 'CategoryController@ajaxGetList'])->name('category.ajax.getList');
                // User
                Route::get('/user', ['uses' => 'UsersController@index'])->name('user.list');
                Route::get('/user/add', ['uses' => 'UsersController@add'])->name('user.add');
                Route::post('/user/add', ['uses' => 'UsersController@addAction'])->name('user.add.action');
                Route::get('/user/detail/{user_id}', ['uses' => 'UsersController@detail'])->name('user.detail');
                Route::get('/user/edit/{user_id}', ['uses' => 'UsersController@edit'])->name('user.edit');
                Route::post('/user/edit', ['uses' => 'UsersController@editManyAction'])->name('user.edit.many.action');
                Route::post('/user/edit/{user_id}', ['uses' => 'UsersController@editAction'])->name('user.edit.action');
                Route::get('/user/delete', ['uses' => 'UsersController@deleteMany'])->name('user.delete.many');
                Route::get('/user/delete/{user_id}', ['uses' => 'UsersController@delete'])->name('user.delete');
                Route::post('/user/delete', ['uses' => 'UsersController@delete'])->name('user.delete');
                Route::get('/user/ajax/get-list', ['uses' => 'UsersController@ajaxGetList'])->name('user.ajax.getList');


                // Agent
                Route::get('/user-group', ['uses' => 'UserGroupController@index'])->name('user_group.list');
                Route::get('/user-group/add', ['uses' => 'UserGroupController@add'])->name('user_group.add');
                Route::post('/user-group/add', ['uses' => 'UserGroupController@addAction'])->name('user_group.add.action');
                Route::get('/user-group/detail/{guest_id}', ['uses' => 'UserGroupController@detail'])->name('user_group.detail');
                Route::get('/user-group/edit/{guest_id}', ['uses' => 'UserGroupController@edit'])->name('user_group.edit');
                Route::post('/user-group/edit', ['uses' => 'UserGroupController@editManyAction'])->name('user_group.edit.many.action');
                Route::post('/user-group/edit/{guest_id}', ['uses' => 'UserGroupController@editAction'])->name('user_group.edit.action');
                Route::get('/user-group/delete', ['uses' => 'UserGroupController@deleteMany'])->name('user_group.delete.many');
                Route::get('/user-group/delete/{guest_id}', ['uses' => 'UserGroupController@delete'])->name('user_group.delete');
                Route::post('/user-group/delete', ['uses' => 'UserGroupController@delete'])->name('user_group.delete');
                Route::get('/user-group/ajax/get-list', ['uses' => 'UserGroupController@ajaxGetList'])->name('user_group.ajax.getList');

                // Logs User
                Route::get('/logs-user', ['uses' => 'LogsUserController@index'])->name('logs_user.list');
                Route::get('/logs-user/detail/{id}', ['uses' => 'LogsUserController@detail'])->name('logs_user.detail');
                Route::get('/logs-user/ajax/get-list', ['uses' => 'LogsUserController@ajaxGetList'])->name('logs_user.ajax.getList');

                // Menu
                Route::get('/menus', ['uses' => 'MenuController@index'])->name('menu.index');

                // Custom Css
                Route::get('/theme-option', ['uses' => 'ThemeOptionsController@option'])->name('theme_option.index');
                Route::post('/theme-option', ['uses' => 'ThemeOptionsController@optionAction'])->name('theme_option.action');

                // Custom Css
                Route::get('/custom-css', ['uses' => 'CustomCssController@index'])->name('custom_css.index');
                Route::post('/custom-css', ['uses' => 'CustomCssController@editAction'])->name('custom_css.editAction');

                // Custom Template
                Route::get('/template', ['uses' => 'TemplateController@index'])->name('template.index');
                Route::post('/template', ['uses' => 'TemplateController@editAction'])->name('template.editAction');
                // Contacts
                Route::get('/contact', ['uses' => 'ContactController@index'])->name('contact.index');
                Route::get('/contact/ajax/get-list', ['uses' => 'ContactController@ajaxGetList'])->name('contact.ajax.getList');
                Route::get('/contact/edit/{id}', ['uses' => 'ContactController@edit'])->name('contact.edit');
                Route::post('/contact/edit', ['uses' => 'ContactController@editManyAction'])->name('contact.edit.many.action');
                Route::post('/contact/edit/{id}', ['uses' => 'ContactController@editAction'])->name('contact.edit.action');
                Route::get('/contact/delete', ['uses' => 'ContactController@deleteMany'])->name('contact.delete.many');
                Route::get('/contact/delete/{id}', ['uses' => 'ContactController@delete'])->name('contact.delete');
                Route::post('/contact/delete', ['uses' => 'ContactController@delete'])->name('contact.delete');
                Route::get('/contact/ajax/get-list', ['uses' => 'ContactController@ajaxGetList'])->name('contact.ajax.getList');
                Route::post('/contact/{id}/reply', ['uses' => 'ContactController@replyAction'])->name('contact.reply.action');

                // Setting
                Route::get('/settings-general', ['uses' => 'SettingController@general'])->name('setting.general');
                Route::post('/settings-general', ['uses' => 'SettingController@generalAction'])->name('setting.general.action');
                Route::post('/settings-login-social', ['uses' => 'SettingController@loginSocialAction'])->name('setting.login_social.action');
                Route::get('/settings-email', ['uses' => 'SettingController@email'])->name('setting.email');
                Route::get('/settings-social-login', ['uses' => 'SettingController@loginSocial'])->name('setting.login_social');
                Route::get('/settings-notification', ['uses' => 'SettingController@notification'])->name('setting.notification');
                Route::post('/settings-notification', ['uses' => 'SettingController@notificationAction'])->name('setting.notification.action');

                // Gallery
                Route::get('/gallery', ['uses' => 'GalleryController@index'])->name('gallery.index');
            });

            
        });
        Route::namespace('General')->group(function () {
            Route::post('/upload-image', ['uses' => 'UpLoadImageController@uploadImage'])->name('uploadImage');
            Route::post('/destroy-image', ['uses' => 'UpLoadImageController@imageDestroy'])->name('imageDestroy');
            Route::get('/language', ['uses' => 'MultiLanguageController@index'])->name('language.index');
        });
    });
});

