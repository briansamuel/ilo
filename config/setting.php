<?php

$result = [
    'general' => [
        'admin_appearance' => [
            'name' => 'Admin Appearance',
            'description' => 'Setting admin appearance such as editor, language...',
            'data' => [
                'site-name' => [
                    'name' => 'Site Name',
                    'description' => 'general::admin_appearance::site-name',
                    'view' => 'text'
                ],
                'site-name-mini' => [
                    'name' => 'Site Name Mini',
                    'description' => 'general::admin_appearance::site-name-mini',
                    'view' => 'text',
                ],
                'site-description' => [
                    'name' => 'Site Description',
                    'description' => 'general::admin_appearance::site-description',
                    'view' => 'textarea'
                ],
                'admin-favicon' => [
                    'name' => 'Admin Favicon',
                    'description' => 'general::admin_appearance::admin-favicon',
                    'view' => 'file'
                ],
                'admin-logo' => [
                    'name' => 'Admin Logo',
                    'description' => 'general::admin_appearance::admin-logo',
                    'view' => 'file'
                ],
            ]
        ],
        'google_analytics' => [
            'name' => 'Google Analytics',
            'description' => 'Config Credentials for Google Analytics',
            'data' => [
                'tracking_code' => [
                    'name' => 'Tracking Code',
                    'description' => 'general::google_analytics::tracking_code',
                    'view' => 'text'
                ],
                'view_id' => [
                    'name' => 'View ID',
                    'description' => 'general::google_analytics::view_id',
                    'view' => 'text'
                ],
            ]
        ]
    ],
    'social_login' => [
        'social_login' => [
            'name' => 'Social Login settings',
            'description' => 'Configure social login options.',
            'data' => [
                'enable' => [
                    'name' => 'Enable?',
                    'description' => 'social_login::social_login::enable',
                    'view' => 'checkbox'
                ],
            ]
        ],
        'facebook_login' => [
            'name' => 'Facebook login settings',
            'description' => 'Enable/disable & configure app credentials for Facebook login.',
            'data' => [
                'enable' => [
                    'name' => 'Enable?',
                    'description' => 'social_login::facebook_login::enable',
                    'view' => 'checkbox'
                ],
                'app_id' => [
                    'name' => 'App ID',
                    'description' => 'social_login::facebook_login::app_id',
                    'view' => 'text'
                ],
            ]
        ],
        'google_login' => [
            'name' => 'Google login settings',
            'description' => 'Enable/disable & configure app credentials for Google login.',
            'data' => [
                'enable' => [
                    'name' => 'Enable?',
                    'description' => 'social_login::google_login::enable',
                    'view' => 'checkbox'
                ],
                'app_id' => [
                    'name' => 'App ID',
                    'description' => 'social_login::google_login::app_id',
                    'view' => 'text'
                ],
            ]
        ],
    ],
    'email' => [
        'email_template' => [
            'name' => 'Setting for email template',
            'description' => 'Email template using HTML & system variables.',
            'data' => [
                'driver' => [
                    'name' => 'Driver',
                    'description' => 'email::email_template::driver',
                    'view' => 'text'
                ],
                'port' => [
                    'name' => 'Port',
                    'description' => 'email::email_template::port',
                    'view' => 'text',
                ],
                'host' => [
                    'name' => 'Host',
                    'description' => 'email::email_template::host',
                    'view' => 'text'
                ],
                'username' => [
                    'name' => 'Username',
                    'description' => 'email::email_template::username',
                    'view' => 'text'
                ],
                'password' => [
                    'name' => 'Password',
                    'description' => 'email::email_template::password',
                    'view' => 'text'
                ],
                'encryption' => [
                    'name' => 'Encryption',
                    'description' => 'email::email_template::encryption',
                    'view' => 'text'
                ],
            ]
        ]
    ],
    'notification' => [
        'contact' => [
            'name' => 'Liên hệ',
            'description' => 'Gửi thông báo khi có liên hệ mới...',
            'data' => [
                'notify' => [
                    'name' => 'Thông báo',
                    'description' => 'notification::contact::notify',
                    'view' => 'checkbox'
                ],
                'name' => [
                    'name' => 'Gửi từ',
                    'description' => 'notification::contact::name',
                    'view' => 'text',
                ],
                'subject' => [
                    'name' => 'Tiêu đề thông báo',
                    'description' => 'notification::contact::subject',
                    'view' => 'text',
                ],
                'message' => [
                    'name' => 'Nội dung thông báo',
                    'description' => 'notification::contact::message',
                    'view' => 'textarea',
                ],
            ]
        ],
        'reply' => [
            'name' => 'Phản hồi',
            'description' => 'Phản hồi liên hệ...',
            'data' => [
                'notify' => [
                    'name' => 'Thông báo',
                    'description' => 'notification::reply::notify',
                    'view' => 'checkbox'
                ],
                'name' => [
                    'name' => 'Gửi từ',
                    'description' => 'notification::reply::name',
                    'view' => 'text',
                ],
                'subject' => [
                    'name' => 'Tiêu đề thông báo',
                    'description' => 'notification::reply::subject',
                    'view' => 'text',
                ],
            ]
        ],
        
    ]
];

return $result;