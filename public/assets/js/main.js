var App = {
    fullpagejs: function () {
        new fullpage('#fullpage', {
            //options here
            licenseKey: '558F2BA0-3BF94B75-94951419-B420B651',
            autoScrolling:true,
            paddingTop: '0px',
            parallax: false,
            easing: 'easeInOutQuart',
            slidesNavigation: true,
            scrollingSpeed: 1000,
            // normalScrollElements: '.normalscroll',
            /*scrollOverflow: true,
            scrollOverflowReset: true,
            scrollOverflowOptions:'.normalscroll',*/
            css3: true,
            // easingcss3: 'cubic-bezier(0.77, 0, 0.175, 1)',
            responsiveWidth: 768,
            navigation: true,
            navigationPosition: 'right',

        });
    },
    sliderhome: function () {
        jQuery('.slider_banner').slick({
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 3000,
            dots: false,
            // fade: true,
            infinite: true,
            arrows: true,
            prevArrow: jQuery('.nav_slider .prev_slider'),
            nextArrow: jQuery('.nav_slider .next_slider'),
        });
    },
    sliderabout: function () {
        jQuery('.slider_about').slick({
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 1500,
            dots: false,
            infinite: true,
            arrows: true,
            prevArrow: jQuery('.nav_about .prev_slider'),
            nextArrow: jQuery('.nav_about .next_slider'),
        });
    },
    slidervalue: function () {
        jQuery('.slider_value').slick({
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 1500,
            dots: false,
            infinite: true,
            arrows: true,
            prevArrow: '<div class="prev_value"><div class="arrow"><img src="assets/images/left.png" alt=""></div></div>',
            nextArrow: '<div class="next_value"><div class="arrow"><img src="assets/images/right.png" alt=""></div></div>',
        });
    },
    slideroffer: function () {
        jQuery('.slide_offer').slick({
            slidesToShow: 3,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 1500,
            dots: false,
            infinite: true,
            arrows: true,
            prevArrow: '<div class="prev_offer"><div class="arrow"><img src="assets/images/left.png" alt=""></div></div>',
            nextArrow: '<div class="next_offer"><div class="arrow"><img src="assets/images/right.png" alt=""></div></div>',
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: true,
                        arrows: false,
                        centerMode: true,
                    }
                },
            ]
        });
    },
    slidergallery: function () {
        jQuery('.slide_gallery').slick({
            slidesToShow: 3,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 1500,
            dots: false,
            infinite: true,
            arrows: true,
            prevArrow: '<div class="prev_offer"><div class="arrow"><img src="assets/images/left.png" alt=""></div></div>',
            nextArrow: '<div class="next_offer"><div class="arrow"><img src="assets/images/right.png" alt=""></div></div>',
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: true,
                        arrows: false,
                        centerMode: true,
                    }
                },
            ]
        });
    },
    slidercurriculum: function(){
        jQuery('.slider_img').slick({
            slidesToShow: 3,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 1500,
            centerMode: true,
            centerPadding: '0px',
            dots: false,
            infinite: true,
            arrows: true,
            prevArrow: '<div class="prev_offer"><div class="arrow"><img src="assets/images/left.png" alt=""></div></div>',
            nextArrow: '<div class="next_offer"><div class="arrow"><img src="assets/images/right.png" alt=""></div></div>',
            asNavFor: '.slider_description',
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: true,
                        arrows: false,
                        centerMode: false,
                    }
                },
            ]
        });
        jQuery('.slider_description').slick({
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 1500,
            dots: false,
            infinite: true,
            arrows: false,
            asNavFor: '.slider_img',
            responsive: [{
                breakpoint: 767,
                settings: {
                    adaptiveHeight: true,
                }
            }]
        })
    },
    sliderskill: function (){
        jQuery('.slider_skill').slick({
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 1500,
            dots: true,
            infinite: true,
            arrows: true,
            prevArrow: '<div class="prev_offer"><div class="arrow"><img src="assets/images/left.png" alt=""></div></div>',
            nextArrow: '<div class="next_offer"><div class="arrow"><img src="assets/images/right.png" alt=""></div></div>',
            asNavFor: '.bottom_slider',
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: true,
                        arrows: false,
                        adaptiveHeight: true
                    }
                },
            ]
        });
        jQuery('.bottom_slider').slick({
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 1500,
            speed: 1500,
            dots: false,
            infinite: true,
            arrows: false,
            asNavFor: '.slider_skill',
        })
    },
    animationtm: function () {
        // build scenes
        var controller = new ScrollMagic.Controller();
        var LeftToRight = document.getElementsByClassName("left_to_right");
        var RightToLeft = document.getElementsByClassName("right_to_left");
        var BottomToTop = document.getElementsByClassName("bottom_to_top");
        var TopToBottom = document.getElementsByClassName("top_to_bottom");
        var Parallaxitem = document.getElementsByClassName("parallax_item");

        for (var i=0; i<LeftToRight.length; i++) { // create a scene for each element
            new ScrollMagic.Scene({
                triggerElement: LeftToRight[i], // y value not modified, so we can use element as trigger as well
                offset: 50,												 // start a little later
                triggerHook: 0.9,
            })
                .setClassToggle(LeftToRight[i], "show") // add class toggle
                .addTo(controller);
        }
        for (var i=0; i<RightToLeft.length; i++) { // create a scene for each element
            new ScrollMagic.Scene({
                triggerElement: RightToLeft[i], // y value not modified, so we can use element as trigger as well
                offset: 50,												 // start a little later
                triggerHook: 0.9,
            })
                .setClassToggle(RightToLeft[i], "show") // add class toggle
                .addTo(controller);
        }
        for (var i=0; i<BottomToTop.length; i++) { // create a scene for each element
            new ScrollMagic.Scene({
                triggerElement: BottomToTop[i], // y value not modified, so we can use element as trigger as well
                offset: 50,												 // start a little later
                triggerHook: 0.9,
            })
                .setClassToggle(BottomToTop[i], "show") // add class toggle
                .addTo(controller);
        }
        for (var i=0; i<TopToBottom.length; i++) { // create a scene for each element
            new ScrollMagic.Scene({
                triggerElement: TopToBottom[i], // y value not modified, so we can use element as trigger as well
                offset: 50,												 // start a little later
                triggerHook: 0.9,
            })
                .setClassToggle(TopToBottom[i], "show") // add class toggle
                .addTo(controller);
        }
        for (var i=0; i<Parallaxitem.length; i++){
            new ScrollMagic.Scene({triggerElement: Parallaxitem[i]})
                .setTween(Parallaxitem[i]+" > .bg_section", {y: "80%", ease: Linear.easeNone})
                .addTo(controller);
        }
    },
    menumobile: function () {
        jQuery('.navbar-toggler').click(function () {
            jQuery('.overlay_menu').toggleClass('show');
        });
        jQuery('.overlay_menu').click(function () {
            jQuery('.navbar-toggler').click();
        });
        if(jQuery('#iloMenu .has-children').length){
            jQuery('#iloMenu .has-children').each(function () {
                jQuery(this).append('<div class="arrow_menu"></div>')
            });
        }
        jQuery('body #iloMenu .has-children').on('click','.arrow_menu', function () {
           jQuery(this).parent('.has-children').find('.sub-menu').slideToggle();
           jQuery(this).parent('.has-children').toggleClass('showmenu');
        });
    },
    mapcontact: function () {
        // Initialize and add the map
        if(jQuery('#map').length){
            // The location of Uluru
            //var $mapinfo = jQuery('#map').data('location').split("|");

            var locations = [];
            $center = jQuery('#map ul li:first-child .latitude').text()+'|'+jQuery('#map ul li:first-child .longitude').text();
            jQuery('#map ul li').each(function () {
                var $info = jQuery(this).find('.map_title').html()+jQuery(this).find('.map_address').html()+'|'+jQuery(this).find('.latitude').text()+'|'+jQuery(this).find('.longitude').text();
                locations.push($info.split('|'));
            });
            var image = 'assets/images/marker.png';
            // The map, centered at Uluru
            var map = new google.maps.Map(
                document.getElementById('map'), {zoom: 13, center: {lat: $center.split('|')[0]*1,lng:$center.split('|')[1]*1},
                    zoomControl: true,
                    mapTypeControl: false,
                    scaleControl: false,
                    streetViewControl: false,
                    rotateControl: false,
                    fullscreenControl: true,
                    styles: [
                        {
                            "stylers": [
                                {
                                    "hue": "#007fff"
                                },
                                {
                                    "saturation": 89
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative.country",
                            "elementType": "labels",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        }
                    ],});
            var infowindow = new google.maps.InfoWindow();
            var marker, i;
            var markers = [];
            // The marker, positioned at Uluru
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    mapTypeId: 'roadmap',animation: google.maps.Animation.DROP, icon: image,
                });
                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                        toggleBounce(marker);
                        setTimeout(function () {
                            marker.setAnimation(null);
                        }, 1000);
                    };
                })(marker, i));
                markers.push(marker);
                if(i==0){
                    var contentString = locations[i][0];
                    var infowindow = new google.maps.InfoWindow({
                        content: contentString
                    });
                    infowindow.open(map, marker);
                }
            }
            function toggleBounce(marker) {
                if (marker.getAnimation() !== null) {
                    marker.setAnimation(null);
                } else {
                    marker.setAnimation(google.maps.Animation.BOUNCE);
                }
            }
            function newLocation(newLat,newLng)
            {
                map.setCenter({
                    lat : newLat,
                    lng : newLng
                });
            }
            function showinfo(id){
                google.maps.event.trigger(markers[id], 'click');
            }
            jQuery('[data-centermap]').click(function () {
                $centermap = jQuery(this).data('centermap').split('~');
                newLocation($centermap[0]*1,$centermap[1]*1);
                showinfo($centermap[2]);
            });

        }
    }
};

jQuery(document).ready(function () {
    App.fullpagejs();
    App.sliderhome();
    App.sliderabout();
    App.slidervalue();
    App.slideroffer();
    App.slidergallery();
    App.slidercurriculum();
    App.sliderskill();
    App.mapcontact();
    App.menumobile();
    App.animationtm();
    jQuery('.showmap').click(function (e) {
        e.preventDefault();
        jQuery('.map_img').slideToggle();
    });
});

(function($){
    $(window).on("load",function(){
        //$.mCustomScrollbar.defaults.scrollButtons.enable=true; //enable scrolling buttons by default
        $(".content_promotion").mCustomScrollbar({
            theme:"dark-3"
        });
        $(".area_location").mCustomScrollbar({
            theme:"dark-3"
        });
        $(".info_joinus").mCustomScrollbar({
            theme:"dark-3"
        });
    });
})(jQuery);
