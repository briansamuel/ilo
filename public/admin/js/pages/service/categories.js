! function(t) {
    var e = {};

    function a(n) {
        if (e[n])
            return e[n].exports;
        var o = e[n] = {
            i: n,
            l: !1,
            exports: {}
        };
        return t[n].call(o.exports, o, o.exports, a),
            o.l = !0,
            o.exports
    }
    a.m = t,
        a.c = e,
        a.d = function(t, e, n) {
            a.o(t, e) || Object.defineProperty(t, e, {
                enumerable: !0,
                get: n
            })
        },
        a.r = function(t) {
            "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
                    value: "Module"
                }),
                Object.defineProperty(t, "__esModule", {
                    value: !0
                })
        },
        a.t = function(t, e) {
            if (1 & e && (t = a(t)),
                8 & e)
                return t;
            if (4 & e && "object" == typeof t && t && t.__esModule)
                return t;
            var n = Object.create(null);
            if (a.r(n),
                Object.defineProperty(n, "default", {
                    enumerable: !0,
                    value: t
                }),
                2 & e && "string" != typeof t)
                for (var o in t)
                    a.d(n, o, function(e) {
                            return t[e]
                        }
                        .bind(null, o));
            return n
        },
        a.n = function(t) {
            var e = t && t.__esModule ? function() {
                    return t.default
                } :
                function() {
                    return t
                };
            return a.d(e, "a", e),
                e
        },
        a.o = function(t, e) {
            return Object.prototype.hasOwnProperty.call(t, e)
        },
        a.p = "",
        a(a.s = 703)
}({
    703: function(t, e, a) {
        "use strict";
        var n, o = (n = {
            data: {
                type: "remote",
               
                source: {
                    read: {
                        url: "category/ajax/get-list?category_type="+$('#category_type').val().toLowerCase(),
                        method: 'get',
                        map: function (t) {
                            var e = t;
                            return void 0 !== t.data && (e = t.data), e
                        }
                    }
                },
                pageSize: 20,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: !0
            },
            layout: {
                // scroll: !0,
                // height: 350,
                footer: !1
            },
            sortable: !0,
            pagination: !0,
            columns: [{
                field: "id",
                title: "#",
                sortable: !1,
                width: 20,
                selector: {
                    class: "kt-checkbox--solid"
                },
                textAlign: "center"
            }, {
                field: "category_name",
                title: "TÊN DANH MỤC",
                template: function(t) {
                    return "<a<strong>" + t.category_name + "</strong>"
                }
            }, {
                field: "category_slug",
                title: "SLUG",
                
            }, {
                field: "post_count",
                title: "BÀI VIẾT",
                width: 50,
            }, {
                field: "language",
                title: "NGÔN NGỮ",
                width: 50,
                
            }, {
                field: "Actions",
                title: "THAO TÁC",
                sortable: !1,
                width: 110,
                overflow: "visible",
                textAlign: "center",
                autoHide: !1,
                template: function(t) {
                    return '    <a target="_blank" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Xem bài viết">                        <i class="flaticon-eye"></i>                    </a>                 <a href="category/edit/' + t.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Sửa bài viết">                        <i class="flaticon2-pen"></i>                    </a>                    <a href="category/delete/' + t.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Xóa bài viết" onclick="return confirm(\'Bạn có chắc muốn xóa bài viết này đi không ???\');">                        <i class="flaticon2-delete"></i>                    </a>                '
                }
            }]
        }, {
            init: function() {
                ! function() {
                    n.search = {
                        input: $("#category_name")
                    };
                }(),
                function() {
                    n.extensions = {
                            checkbox: {}
                        },
                        n.search = {
                            input: $("#category_name")
                        };
                    var t = $("#server_category_selection").KTDatatable(n);
                        $("#kt_datatable_delete_all").on("click", (function() {
                            var list_checked_id = t.checkbox().getSelectedId();
                            var total_checked_id = list_checked_id.length;
                            if(total_checked_id === 0) return false;
                            $.ajax({
                                url: "category/delete",
                                method: 'post',
                                data: {
                                    _token: $("input[name='_token']").val(),
                                    ids: list_checked_id,
                                    total: total_checked_id
                                },
                                success: function(){
                                    window.location.reload();
                                },
                                error: function(errors) {
                                    errors = errors.responseJSON;
                                    var message = !errors.error ? 'Có lỗi xảy ra, vui lòng thử lại sau' : errors.error.message;
                                    KTApp.unprogress(e), swal.fire({
                                        title: "",
                                        text: message,
                                        type: 'error',
                                        confirmButtonClass: "btn btn-secondary"
                                    });
                                }
                            });
                        })),
                        $("#kt_form_status1").on("change", (function() {
                            t.search($(this).val().toLowerCase(), "Status")
                        })),
                        $("#kt_form_type1").on("change", (function() {
                            t.search($(this).val().toLowerCase(), "Type")
                        })),
                        $("#kt_form_status1,#kt_form_type1").selectpicker(),
                        t.on("kt-datatable--on-click-checkbox kt-datatable--on-layout-updated", (function(e) {
                            var a = t.checkbox().getSelectedId().length;
                            $("#kt_datatable_selected_number1").html(a),
                                a > 0 ? $("#kt_datatable_group_action_form1").collapse("show") : $("#kt_datatable_group_action_form1").collapse("hide")
                        })),
                        $("#kt_modal_fetch_id_server").on("show.bs.modal", (function(e) {
                            for (var a = t.checkbox().getSelectedId(), n = document.createDocumentFragment(), o = 0; o < a.length; o++) {
                                var r = document.createElement("li");
                                r.setAttribute("data-id", a[o]),
                                    r.innerHTML = "Selected record ID: " + a[o],
                                    n.appendChild(r)
                            }
                            $(e.target).find(".kt-datatable_selected_ids").append(n)
                        })).on("hide.bs.modal", (function(t) {
                            $(t.target).find(".kt-datatable_selected_ids").empty()
                        }))
                }()
            }
        });
        jQuery(document).ready((function() {
            o.init()
        }))
    }
});