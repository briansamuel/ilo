<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class PostModel
{
    //
    protected static $table = 'posts';

    public static function search($search, $filter)
    {
        return DB::table(self::$table)->where($filter)->where('post_title', 'LIKE', $search.'%')->get();
    }

    
    public static function getMany($columns = ['*'],$pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];

        $query = DB::table(self::$table)->select($columns)->skip($offset)->take($pagination['perpage']);

        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('post_status', '=', $filter['status']);
        }

        if(isset($filter['post_title']) && $filter['post_title'] != ""){
            $query->where('post_title', 'like', "%".$filter['post_title']."%");
        }

        if(isset($filter['post_type']) && $filter['post_type'] != ""){
            $query->where('post_type', '=', $filter['post_type']);
        }

        if(isset($filter['language']) && $filter['language'] != ""){
            $query->where('language', $filter['language']);
        }

        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }

        if(isset($sort['field']) && $sort['field'] != ""){
            $query->orderBy($sort['field'], $sort['sort']);
        }

        return $query->get();
    }

    public static function totalRows($filter) {

        $query = DB::table(self::$table);
        
        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('post_status', '=', $filter['status']);
        }
        if(isset($filter['post_title']) && $filter['post_title'] != ""){
            $query->where('post_title', 'like', "%".$filter['post_title']."%");
        }

        if(isset($filter['post_type']) && $filter['post_type'] != ""){
            $query->where('post_type', '=', $filter['post_type']);
        }
        
        if(isset($filter['language']) && $filter['language'] != ""){
            $query->where('language', $filter['language']);
        }

        if(isset($filter['ids']) && $filter['ids'] != ""){
            $query->whereIn('id', $filter['ids']);
        }
        
        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        return $query->count();

    }

    public static function findByKey($key, $value, $columns = ['*'], $with = [])
    {
        $data = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $data ? $data : [];
    }

    public static function findByMultiKey($filter , $columns = ['*'], $with = [])
    {
        $query = DB::table(self::$table)->select($columns);

        if(isset($filter['post_slug']) && $filter['post_slug'] != ""){
            $query->where('post_slug', $filter['post_slug']);
        }
        
        if(isset($filter['language']) && $filter['language'] != ""){
            $query->where('language', $filter['language']);
        }
        $data = $query->first();
        return $data ? $data : [];
    }

    public static function insert($params)
    {
        return DB::table(self::$table)->insertGetId($params);

    }

    public static function update($id, $params)
    {
        return DB::table(self::$table)->where('id', $id)->update($params);

    }

    public static function updateView($id, $view)
    {
        return DB::table(self::$table)->where('id', $id)->increment('total_view', $view);

    }

    public static function updateManyPost($ids, $data)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->update($data);
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();

    }

    public static function deleteManyPost($ids)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->delete();
    }

    public static function takeNew($quantity, $filter)
    {   
        $offset = isset($filter['offset']) ? $filter['offset'] : 0;
        $query = DB::table(self::$table)->skip($offset)->take($quantity);

        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('post_status', '=', $filter['status']);
        }
        if(isset($filter['post_title']) && $filter['post_title'] != ""){
            $query->where('post_title', 'like', $filter['post_title']."%");
        }
        
        if(isset($filter['post_type']) && $filter['post_type'] != ""){
            $query->where('post_type', '=', $filter['post_type']);
        }
        
        if(isset($filter['ids']) && $filter['ids'] != ""){
            $query->whereIn('id', $filter['ids']);
        }

        if(isset($filter['exclude']) && $filter['exclude'] != ""){
            $query->where('id', '!=', $filter['exclude']);
        }

        if(isset($filter['language']) && $filter['language'] != ""){
            $query->where('language', $filter['language']);
        }
        
        
        $query->orderBy('post_feature', 'desc');
        $query->orderBy('updated_at', 'desc');
        return $query->get();
    }

    public static function takeHotNew($quantity, $filter)
    {
        $offset = isset($filter['offset']) ? $filter['offset'] : 0;
        $query = DB::table(self::$table)->skip($offset)->take($quantity);

        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('post_status', '=', $filter['status']);
        }
        if(isset($filter['post_title']) && $filter['post_title'] != ""){
            $query->where('post_title', 'like', "%".$filter['post_title']."%");
        }

        if(isset($filter['post_type']) && $filter['post_type'] != ""){
            $query->where('post_type', '=', $filter['post_type']);
        }

        if(isset($filter['ids']) && $filter['ids'] != ""){
            $query->whereIn('id', $filter['ids']);
        }

        if(isset($filter['exclude']) && $filter['exclude'] != ""){
            $query->where('id', '!=', $filter['exclude']);
        }

        return $query->orderBy('total_view', 'DESC')->get();
    }
}
