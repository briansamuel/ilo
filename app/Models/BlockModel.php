<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class BlockModel
{

    protected static $table = 'blocks';

    public static function getAll()
    {
        $result = DB::table(self::$table)->get();
        return $result ? $result : [];
    }

    public static function getForFrontEnd($slug, $lang)
    {
        $result = DB::table(self::$table)
                    ->where('active', 'yes')
                    ->where('slug', $slug)
                    ->where('language', $lang)
                    ->first();
        return $result ? $result : [];
    }

    public static function getMany($pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table)->skip($offset)->take($pagination['perpage']);

        if(isset($filter['page']) && $filter['page'] != ""){
            $query->where('slug', 'like', "%".$filter['page']."%");
        }

        if(isset($filter['name']) && $filter['name'] != ""){
            $query->where('name', 'like', "%".$filter['name']."%");
        }

        if(isset($filter['language']) && $filter['language'] != ""){
            $query->where('language', 'like', "%".$filter['language']."%");
        }

        if(isset($sort['field']) && $sort['field'] === "created_at"){
            $query->orderBy('created_at', $sort['sort']);
        }
        return $query->get();
    }

    public static function totalRows($filter) {
        
        $query = DB::table(self::$table);

        if(isset($filter['page']) && $filter['page'] != ""){
            $query->where('slug', 'like', "%".$filter['page']."%");
        }

        if(isset($filter['name']) && $filter['name'] != ""){
            $query->where('name', 'like', "%".$filter['name']."%");
        }
        
        if(isset($filter['language']) && $filter['language'] != ""){
            $query->where('language', 'like', "%".$filter['language']."%");
        }

        return $query->count();
    }

    public static function findByKey($key, $value, $columns = ['*'])
    {
        $result = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $result ? $result : [];
    }

    public static function findById($id, $columns = ['*'])
    {
        $result = DB::table(self::$table)->select($columns)->where('id', $id)->first();
        return $result ? $result : [];
    }

    public static function edit($id, $data)
    {
        return DB::table(self::$table)->where('id', $id)->update($data);
    }

    public static function add($data)
    {
        return DB::table(self::$table)->insert($data);
    }

    public static function delete($ids)
    {
        return DB::table(self::$table)->where('id', $ids)->delete();
    }

    public static function checkSlugExist($slug)
    {
        return DB::table(self::$table)->where('slug', $slug)->exists();
    }

}
