<?php

namespace App\Models;

use App\Services\UserGroupService;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class UserModel extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    protected $guard = 'admin';

    public function getPermissionAttribute()
    {
        //Get Permission
        $groups = UserGroupService::getGroupById(explode(",", Auth::user()->group_id));
        $permission = array();
        foreach ($groups as $group) {
            $permission = array_merge($permission, json_decode($group->permission));
        }
        $permission = array_unique($permission);
        $permission = json_encode($permission);
        return $permission;
    }

    public static function getAll()
    {
        $result = UserModel::get();
        return $result ? $result : [];
    }

    public static function getMany($pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = UserModel::skip($offset)->take($pagination['perpage']);
        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('status', '=', $filter['status']);
        }
        if(isset($filter['email']) && $filter['email'] != ""){
            $query->where('email', 'like', "%".$filter['email']."%");
        }
        if(isset($filter['username']) && $filter['username'] != ""){
            $query->where('username', 'like', "%".$filter['username']."%");
        }
        if(isset($filter['is_root']) && $filter['is_root'] != ""){
            $query->where('is_root', '=', $filter['is_root']);
        }
        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        if(isset($sort['field']) && $sort['field'] === "created_at"){
            $query->orderBy('created_at', $sort['sort']);
        }
        return $query->get();
    }

    public static function totalRows() {
        $result = UserModel::count();
        return $result;
    }

    public static function findByKey($key, $value, $columns = ['*'])
    {
        $result = UserModel::select($columns)->where($key, $value)->first();
        return $result ? $result : [];
    }

    public static function findById($id, $columns = ['*'])
    {
        $result = UserModel::select($columns)->where('id', $id)->first();
        return $result ? $result : [];
    }

    public static function updateUser($id, $data)
    {
        return UserModel::where('id', $id)->update($data);
    }

    public static function deleteManyUser($ids)
    {
        return UserModel::whereIn('id', $ids)->delete();
    }

    public static function updateManyUser($ids, $data)
    {
        return UserModel::whereIn('id', $ids)->update($data);
    }

    public static function deleteUser($id)
    {
        return UserModel::where('id', $id)->delete();
    }

    public static function takeNewUser($quantity)
    {
        return UserModel::orderBy('id', 'DESC')->limit($quantity)->get();
    }
}
