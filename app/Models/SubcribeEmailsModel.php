<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class SubcribeEmailsModel
{
    protected static $table = 'subcribe_emails';

    public static function getAll()
    {
        $result = DB::table(self::$table)->get();
        return $result ? $result : [];
    }

    public static function getMany($pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table)->skip($offset)->take($pagination['perpage']);
        if(isset($filter['active']) && $filter['active'] != ""){
            $query->where('active', '=', $filter['active']);
        }
        if(isset($filter['email']) && $filter['email'] != ""){
            $query->where('email', 'like', "%".$filter['email']."%");
        }
        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        if(isset($sort['field']) && $sort['field'] === "created_at"){
            $query->orderBy('created_at', $sort['sort']);
        }
        return $query->get();
    }

    public static function totalRows() {
        $result = DB::table(self::$table)->count();
        return $result;
    }

    public static function findByKey($key, $value, $columns = ['*'])
    {
        $result = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $result ? $result : [];
    }

    public static function findById($id, $columns = ['*'])
    {
        $result = DB::table(self::$table)->select($columns)->where('id', $id)->first();
        return $result ? $result : [];
    }

    public static function add($data)
    {
        return DB::table(self::$table)->insert($data);
    }

    public static function update($id, $data)
    {
        return DB::table(self::$table)->where('id', $id)->update($data);
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();
    }

    public static function checkEmailExist($email)
    {
        return DB::table(self::$table)->where('email', $email)->exists();
    }

    public static function updateUser($id, $data)
    {
        return DB::table(self::$table)->where('id', $id)->update($data);
    }
}
