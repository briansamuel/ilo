<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class CategoryPostModel
{
    //
    protected static $table = 'category_post_relations';


    public static function getAllByKey($columns = ['*'], $filter, $array = false)
    {
        $query = DB::table(self::$table)->select($columns);
        if(isset($filter['category_id']) && $filter['category_id'] != ""){
            $query->where('category_id', '=', $filter['category_id']);
        }

        if(isset($filter['post_id']) && $filter['post_id'] != ""){
            $query->where('post_id', '=', $filter['post_id']);
        }
          
        return $query->get();
    }

    public static function getManyByKey($columns = ['*'], $filter, $pagination)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table)->select($columns)->skip($offset)->take($pagination['perpage']);
        if(isset($filter['category_id']) && $filter['category_id'] != ""){
            $query->where('category_id', '=', $filter['category_id']);
        }

        if(isset($filter['post_id']) && $filter['post_id'] != ""){
            $query->where('post_id', '=', $filter['post_id']);
        }
          
        return $query->get();
    }


    public static function totalRows($filter) {
        $query = DB::table(self::$table);

        if(isset($filter['category_id']) && $filter['ccategory_id'] != ""){
            $query->where('category_id', '=', $filter['category_id']);
        }

       
    
        $result = $query->count();
        return $result;
    }

    public static function findByKey($key, $value, $columns = ['*'], $with = [])
    {
        $data = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $data ? $data : [];
    }

    public static function countByKey($key, $value) {
        return DB::table(self::$table)->where($key, $value)->count();
    }

    public static function insert($params)
    {
        return DB::table(self::$table)->insertGetId($params);

    }

    public static function update($id, $params)
    {
        return DB::table(self::$table)->where('id', $id)->update($params);

    }

    public static function deleteManyByKey($filter)
    {   
        $query = DB::table(self::$table);
        if(isset($filter['category_id']) && $filter['category_id'] != ""){
            $query->where('category_id', '=', $filter['category_id']);
        }

        if(isset($filter['post_id']) && $filter['post_id'] != ""){
            $query->where('post_id', '=', $filter['post_id']);
        }
        
        return $query->delete();
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();

    }
}
