<?php

namespace App\Services\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthPermissionService
{

    //CI instance
    private $CI;
    private $request;
    private $admin = 'no';
    private $is_root = 'no';
    private $requireRoot = 'no';
    //current user's permissions
    private $authobj;

    private $MyProfileController = array(
        'MyProfileController' => 'Quản lý toàn quyền(Tài Khoản => Thông tin tài khoản)',
        'MyProfileController/profile' => 'Xem thông tin cá nhân',
        'MyProfileController/updateProfile' => 'Cập nhập thông tin cá nhân',
        'MyProfileController/changePassword' => 'Thay đổi mật khẩu',
        'MyProfileController/changePasswordAction' => 'Action thay đổi mật khẩu',
    );

    private $UsersController = array(
        'UsersController' => 'Quản lý toàn quyền(Tài Khoản => Tài Khoản Hệ Thống)',
        'UsersController/index' => 'Danh sách hệ thống',
        'UsersController/ajaxGetList' => 'Action Danh sách hệ thống',
        'UsersController/add' => 'Tạo mới 1 tài khoản hệ thống',
        'UsersController/addAction' => 'Action xử lý tạo mới 1 tài khoản hệ thống',
        'UsersController/edit' => 'Sửa thông tin 1 tài khoản hệ thống',
        'UsersController/editAction' => 'Action xử lý sửa thông tin 1 tài khoản hệ thống',
        'UsersController/editManyAction' => 'Action xử lý sửa nhiều thông tin tài khoản hệ thống',
        'UsersController/delete' => 'Xóa thông tin 1 tài khoản hệ thống',
        'UsersController/deleteMany' => 'Xóa nhiều thông tin tài khoản hệ thống',
    );

    private $UserGroupController = array(
        'UserGroupController' => 'Quản lý toàn quyền(Tài Khoản => Quản lý phân quyền)',
        'UserGroupController/index' => 'Danh sách phân quyền',
        'UserGroupController/ajaxGetList' => 'Action Danh sách phân quyền',
        'UserGroupController/add' => 'Tạo mới 1 tài khoản phân quyền',
        'UserGroupController/addAction' => 'Action xử lý tạo mới 1 phân quyền',
        'UserGroupController/edit' => 'Sửa thông tin 1 phân quyền',
        'UserGroupController/editAction' => 'Action xử lý sửa thông tin 1 phân quyền',
        'UserGroupController/editManyAction' => 'Action xử lý sửa nhiều thông tin phân quyền',
        'UserGroupController/delete' => 'Xóa thông tin 1 phân quyền',
        'UserGroupController/deleteMany' => 'Xóa nhiều thông tin phân quyền',
    );

    private $LogsUserController = array(
        'LogsUserController' => 'Quản lý toàn quyền(Tài Khoản => Logs User)',
        'LogsUserController/index' => 'Danh sách Logs User',
        'LogsUserController/ajaxGetList' => 'Action Danh sách Logs User',
    );

    // Group quyền tin tức
    private $NewsController = array(
        'NewsController' => 'Quản lý toàn quyền (Tin tức)',
        'NewsController/index' => 'Danh sách tin tức',
        'NewsController/add' => 'Trang Thêm và Sửa tin tức',
        'NewsController/save' => 'Action Lưu dữ liệu',
        'NewsController/delete' => 'Xóa tin tức',
    );

    // Group banner
    private $BannersController = array(
        'BannersController' => 'Quản lý toàn quyền (Banner)',
        'BannersController/index' => 'Danh sách banner',
        'BannersController/add' => 'Tạo mới banner',
        'BannersController/addAction' => 'Action Tạo mới banner',
        'BannersController/edit' => 'Sửa thông tin 1 banner',
        'BannersController/editAction' => 'Action xử lý sửa thông tin 1 banner',
        'BannersController/editManyAction' => 'Action xử lý sửa nhiều banner',
        'BannersController/delete' => 'Xóa 1 banner',
        'BannersController/deleteMany' => 'Xóa nhiều banner',
    );

    // Group brand
    private $BrandsController = array(
        'BrandsController' => 'Quản lý toàn quyền (Brand)',
        'BrandsController/index' => 'Danh sách brand',
        'BrandsController/add' => 'Tạo mới brand',
        'BrandsController/addAction' => 'Action Tạo mới brand',
        'BrandsController/edit' => 'Sửa thông tin 1 brand',
        'BrandsController/editAction' => 'Action xử lý sửa thông tin 1 brand',
        'BrandsController/editManyAction' => 'Action xử lý sửa nhiều brand',
        'BrandsController/delete' => 'Xóa 1 brand',
        'BrandsController/deleteMany' => 'Xóa nhiều brand',
    );


    // Group thư viện
    private $GalleryController = array(
        'GalleryController' => 'Quản lý toàn quyền (Thư viện)',
        'GalleryController/index' => 'Quản lý thư viện',
    );

    // Group thư viện
    private $MultiLanguageController = array(
        'MultiLanguageController' => 'Quản lý toàn quyền (Đa ngôn ngữ)',
        'MultiLanguageController/index' => 'Quản lý đa ngôn ngữ',
    );
    private $MenusController = array(
        'MenusController' => 'Quản lý toàn quyền (Menu)',
        'MenusController/index' => 'Quản lý Menu',
    );

    private $ThemeOptionsController = array(
        'ThemeOptionsController' => 'Quản lý toàn quyền (Tùy biến)',
        'ThemeOptionsController/index' => 'Tùy biến',
        'ThemeOptionsController/editAction' => 'Action Tùy biến',
    );
    /*
     * construct
     */

    public function __construct(Request $request, $requireRoot = false)
    {
        $this->CI = new \stdClass();

        $this->request = $request;
        $this->requireRoot = $requireRoot;

        //current permission
        $this->authobj = json_decode(Auth::user()->permission);
        $this->authobj = is_array($this->authobj) ? $this->authobj : array();
    }

    function check()
    {
        //is root
        if (Auth::user()->is_root == 1) {
            
            return true;
        }
        //only root
        if ($this->requireRoot && Auth::user()->is_root != 1) {
            return false;
        }
        //lấy controller - function hiện tại qua router
        $currentAction = \Route::currentRouteAction();
        list($controllers, $method) = explode('@', $currentAction);
        // $controller now is "App\Http\Controllers\FooBarController"
        $controller = preg_replace('/.*\\\/', '', $controllers);
        if($controller === 'WelcomeController') {
            return true;
        }
        //tên controller/function
        $function = $controller . '/' . $method;
        //full access controller
        if (in_array($controller, $this->authobj)) {
            return true;
        }
        //can access
        if (in_array($function, $this->authobj)) { //|| in_array($method, $this->ignores)) {
            return true;
        }
        //no permission
        return false;
    }

    /*
     * get list controller
     */

    function listController()
    {
        $this->controllers = array(
            'MyProfileController' => $this->MyProfileController,
            'UsersController' => $this->UsersController,
            'UserGroupController' => $this->UserGroupController,
            'LogsUserController' => $this->LogsUserController,
            'NewsController' => $this->NewsController,
            'BannersController' => $this->BannersController,
            'BrandsController' => $this->BrandsController,
            'TopDealsController' => $this->TopDealsController,
            'MenusController' => $this->MenusController,
            'GalleryController' => $this->GalleryController,
            'MultiLanguageController' => $this->MultiLanguageController,
            'ThemeOptionsController' => $this->ThemeOptionsController,
        );
        return $this->controllers;
    }

    /*
     * return admin
     */

    public function isAdmin()
    {
        if ($this->admin == 'yes') {
            return true;
        } else {
            return false;
        }
    }

    /*
     * check function show in header
     */

    function checkHeader($controller)
    {
        if (Auth::user()->is_root == 1) {
            return true;
        }
        foreach ($this->authobj as $permission) {
            if (strpos($controller, $permission) === 0) {
                return true;
            }
        }
        return false;
    }

    public function isHeader($controller)
    {
        if (Auth::user()->is_root == 1) {
            return true;
        }
        if (in_array($controller, $this->authobj)) {
            return true;
        } else {
            return false;
        }
    }

    public function getListPermission($encode = true)
    {
        $permissions = [];
        foreach ($this->listController() as $key => $value) {
            //neu co 1 chuc nang dc lua chon
            if ($this->request->input($key, '')) {
                $permission = $this->request->input($key, '');
                //neu la toan quyen
                if ($permission[0] == $key) {
                    $permissions[] = $key;
                } else {
                    foreach ($permission as $x) {
                        $permissions[] = $x;
                    }
                }
            }
        }

        return $encode ? json_encode($permissions) : $permissions;
    }

}

?>
