<?php
namespace App\Services;
use App\Models\SettingModel;
use Auth;
use App;
class SettingService
{
    public static function getMultipleLanguages() {
        return [
            'theme_option::footer::address_1',
            'theme_option::footer::address_2',
            'theme_option::footer::phone_number',
            'theme_option::footer::email',
            'theme_option::general::option_background',
            'theme_option::general::intro_video_option',
            'theme_option::header::hotline',
            'theme_option::header::email',
            'theme_option::general::image',
            'theme_option::general::intro_video_upload',
            'theme_option::home::about_content'
        ];
    }

    public static function add($data, $lang = 'vi')
    {
        $user_id = Auth::guard('admin')->user()->id;
        foreach($data as $key=>$value) {
            $param = [];
            // key là multiple và đã tồn tại
            if(!in_array($key, self::getMultipleLanguages()) && !$detail = SettingModel::findByName($key)) {
                $param['setting_key'] = $key;
                $param['setting_value'] = $value;
                $param['language'] = 'vi';
                $param['created_by_user'] = $user_id;
                $param['updated_by_user'] = $user_id;
                $param['created_at'] = date("Y-m-d H:i:s");
                $param['updated_at'] = date("Y-m-d H:i:s");

                SettingModel::add($param);
            } else if(in_array($key, self::getMultipleLanguages()) && !$detail = SettingModel::findByName($key, $lang)) {
                $param['setting_key'] = $key;
                $param['setting_value'] = $value;
                $param['language'] = $lang;
                $param['created_by_user'] = $user_id;
                $param['updated_by_user'] = $user_id;
                $param['created_at'] = date("Y-m-d H:i:s");
                $param['updated_at'] = date("Y-m-d H:i:s");

                SettingModel::add($param);
            } else {
                $param['setting_value'] = $value;

                SettingModel::update($detail->id, $param);
            }
        }

        return true;
    }

    public static function savedSettings()
    {
        $dbSettings = SettingModel::all();
        return $dbSettings;
    }

    public static function deleteGroup($group)
    {
        $conditions = [
            ['setting_key', 'like', "%$group%"]
        ];
        return SettingModel::deletebyCondition($conditions);
    }

    /*
     *
     */

    public static function get($key, $default = '')
    {
        $locale = App::getlocale();
        if(!in_array($key, self::getMultipleLanguages())) {
            $locale = 'vi';
        }
        if($setting = SettingModel::findByName($key, $locale)){
            return $setting->setting_value;
        }

        return $default;
    }

    public static function getSetting($type, $lang = 'vi')
    {
        $dbSettings = SettingModel::get($type, $lang);
        return $dbSettings;
    }
}