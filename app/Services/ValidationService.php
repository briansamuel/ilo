<?php
namespace App\Services;


use Validator;

class ValidationService
{

    /*
    * function make validation
    */
    public function make($params, $type)
    {
        $validator = Validator::make(
            $params,
            $this->getRules($type),
            $this->getCustomMessages(),
            $this->attributes()
        );
        return $validator;
    }

    /*
    * function get rule config
    */
    public function getRules($type)
    {
        $rules = [
            'update_my_profile_fields' => [
                'full_name' => self::getRule('require_field'),
            ],
            'change_password_fields' => [
                'password' => self::getRule('password'),
                'new_password' => self::getRule('password'),
                'confirm_new_password' => self::getRule('password'),
            ],
            'add_user_fields' => [
                'email' => self::getRule('email'),
                'username' => self::getRule('username'),
                'full_name' => self::getRule('full_name'),
                'password' => self::getRule('password')
            ],
            'edit_user_fields' => [
                'full_name' => self::getRule('full_name'),
                'status' => self::getRule('require_field')
            ],
            'add_agent_fields' => [
                'email' => self::getRule('email'),
                'username' => self::getRule('username'),
                'full_name' => self::getRule('full_name'),
                'password' => self::getRule('password'),
                'agent_address' => self::getRule('require_field'),
                'agent_phone' => self::getRule('phone_number'),
                'agent_birthday' => self::getRule('require_field'),
            ],
            'add_page_fields' => [
                'page_title' => self::getRule('page_unique'),
                'page_description' => self::getRule('require_field'),
                'page_content' => self::getRule('require_field'),
                
            ],
            'edit_page_fields' => [
                'page_title' => self::getRule('require_field'),
                'page_description' => self::getRule('require_field'),
                'page_content' => self::getRule('require_field'),
                
            ],
            'add_news_fields' => [
                'post_title' => self::getRule('require_field'),
                'post_description' => self::getRule('require_field'),
                'post_content' => self::getRule('require_field'),
                
            ],
            'edit_news_fields' => [
                'post_title' => self::getRule('require_field'),
                'post_description' => self::getRule('require_field'),
                'post_content' => self::getRule('require_field'),
                
            ],
            'add_project_fields' => [
                'project_title' => self::getRule('require_field'),
                'project_description' => self::getRule('require_field'),
                'project_content' => self::getRule('require_field'),
                'project_thumbnail' => self::getRule('require_field'),
                'project_location' => self::getRule('require_field'),
            ],
            'edit_project_fields' => [
                'project_title' => self::getRule('require_field'),
                'project_description' => self::getRule('require_field'),
                'project_content' => self::getRule('require_field'),
                'project_thumbnail' => self::getRule('require_field'),
                'project_location' => self::getRule('require_field'),
            ],
            'add_partner_fields' => [
                'partner_title' => self::getRule('post_unique'),
                'partner_description' => self::getRule('require_field'),
                'partner_thumbnail' => self::getRule('require_field'),
            ],
            'edit_partner_fields' => [
                'post_title' => self::getRule('require_field'),
                'post_description' => self::getRule('require_field'),
                'post_content' => self::getRule('require_field'),
            ],
            'add_recruitment_fields' => [
                'post_title' => self::getRule('post_unique'),
                'post_description' => self::getRule('require_field'),
                'post_content' => self::getRule('require_field'),
            ],
            'edit_recruitment_fields' => [
                'post_title' => self::getRule('require_field'),
                'post_description' => self::getRule('require_field'),
                'post_content' => self::getRule('require_field'),
            ],
            'add_category_fields' => [
                'category_name' => self::getRule('require_field'),
                'category_description' => self::getRule('require_field'),
                'category_parent' => self::getRule('require_field'),
                'language' => self::getRule('require_field'),
                'category_type' => self::getRule('require_field'),
                
            ],
            'add_user_group_fields' => [
                'group_name' => self::getRule('require_field'),
                'enabled' => self::getRule('require_field'),
                'short_description' => self::getRule('require_field')
            ],
            'add_host_fields' => [
                'host_name' => self::getRule('host_name'),
                'host_description' => self::getRule('require_field'),
                'host_convenient' => self::getRule('require_field'),
                'host_address' => self::getRule('require_field'),
                'host_lat' => self::getRule('require_field'),
                'host_lng' => self::getRule('require_field'),
                'province_name' => self::getRule('require_field'),
                'district_name' => self::getRule('require_field'),
                'created_by_agent' => self::getRule('require_field'),
            ],
            'edit_host_fields' => [
                'host_name' => self::getRule('require_field'),
                'host_description' => self::getRule('require_field'),
                'host_convenient' => self::getRule('require_field'),
                'host_address' => self::getRule('require_field'),
                'host_lat' => self::getRule('require_field'),
                'host_lng' => self::getRule('require_field'),
                'province_name' => self::getRule('require_field'),
                'district_name' => self::getRule('require_field'),
                'created_by_agent' => self::getRule('require_field'),
            ],

            'add_booking_fields' => [
                'host_id' => self::getRule('require_field'),
                'room_id' => self::getRule('require_field'),
                'guest_id' => self::getRule('require_field'),
                'checkin_date' => self::getRule('require_field'),
                'checkout_date' => self::getRule('require_field'),
                'booking_price' => self::getRule('require_field'),
                'night_booking' => self::getRule('require_field'),
                'created_by_agent' => self::getRule('require_field'),
            ],
            

            'add_review_fields' => [
                'review_title' => self::getRule('require_field'),
                'review_content' => self::getRule('require_field'),
                'rating_review' => self::getRule('require_field'),
                'host_id' => self::getRule('require_field'),
                'created_by_guest' => self::getRule('require_field'),
            ],

        

            'edit_contact_fields' => [
                'status' => self::getRule('require_field')
            ],
            'add_contact_reply_fields' => [
                'contact_id' => self::getRule('require_field'),
                'message' => self::getRule('require_field')
            ],
            'edit_comment_fields' => [
                'comment_status' => self::getRule('require_field')
            ],
            'edit_review_fields' => [
                'review_status' => self::getRule('require_field')
            ],
            'add_top_deals_fields' => [
                'title' => self::getRule('require_field'),
                'image' => self::getRule('require_field'),
                'description' => self::getRule('require_field'),
                'label' => self::getRule('require_field'),
                'location' => self::getRule('require_field'),
                'start_time' => self::getRule('require_field'),
                'end_time' => self::getRule('require_field'),
                'regular_price' => self::getRule('require_field'),
                'sale_price' => self::getRule('require_field'),
                'link' => self::getRule('require_field'),
                'position' => self::getRule('require_field'),
                'language' => self::getRule('require_field'),
                'deal_group' => self::getRule('require_field'),
            ],
            'edit_top_deals_fields' => [
                'title' => self::getRule('require_field'),
                'description' => self::getRule('require_field'),
                'label' => self::getRule('require_field'),
                'location' => self::getRule('require_field'),
                'start_time' => self::getRule('require_field'),
                'end_time' => self::getRule('require_field'),
                'regular_price' => self::getRule('require_field'),
                'sale_price' => self::getRule('require_field'),
                'link' => self::getRule('require_field'),
                'position' => self::getRule('require_field'),
                'language' => self::getRule('require_field'),
                'deal_group' => self::getRule('require_field'),
            ],
            'add_banner_fields' => [
                'title' => self::getRule('require_field'),
                'image' => self::getRule('require_field'),
                'description' => self::getRule('require_field'),
                'position' => self::getRule('require_field'),
                'language' => self::getRule('require_field'),
                'type' => self::getRule('require_field'),
            ],
            'edit_banner_fields' => [
                'title' => self::getRule('require_field'),
                'description' => self::getRule('require_field'),
                'position' => self::getRule('require_field'),
                'language' => self::getRule('require_field'),
                'caption' => self::getRule('require_field'),
                'type' => self::getRule('require_field'),
            ],
            'add_block_fields' => [
                'name' => self::getRule('require_field'),
                'slug' => self::getRule('require_field'),
                'language' => self::getRule('require_field'),
                'active' => self::getRule('require_field')
            ],
            'edit_block_fields' => [
                'name' => self::getRule('require_field'),
                'language' => self::getRule('require_field'),
                'active' => self::getRule('require_field')
            ],
            'edit_subcribe_emails_fields' => [
                'email' => self::getRule('email'),
                'active' => self::getRule('require_field')
            ],
            'add_frontend_agent_fields' => [
                'name' => self::getRule('require_field'),
                'phone_number' => self::getRule('require_field'),
                'email' => self::getRule('email'),
                'content' => self::getRule('require_field'),
            ],
            'add_frontend_subcribe_email_fields' => [
                'email' => self::getRule('email'),
            ],
            'add_frontend_send_contact_fields' => [
                'phone_number' => self::getRule('require_field'),
                'email' => self::getRule('require_field'),
                'name' => self::getRule('require_field'),
                'type' => self::getRule('require_field'),
            ],
            'check_frontend_captcha_fields' => [
                'captcha' => self::getRule('captcha'),
            ],
        ];

        return isset($rules[$type]) ? $rules[$type] : array();
    }

    /*
    * function get Attrbutes
    */
    public function attributes()
    {
        return [
            
            'post_title' => 'Tiêu đề',
            'post_description' => 'Mô tả',
            'post_content' => 'Nội dung',
            'host_name' => 'Tên khách sạn, resort',
            'host_descrition' => 'Mô tả khách sạn, resort',
            

        ];
       
    }

    /*
    * function get rule
    */
    public function getRule($rule)
    {
        $rules = [
            'limit' => 'numeric',
            'offset' => 'numeric',
            'user_id' => 'numeric|required',
            'username' => 'required',
            'full_name' => 'required',
            'phone_number' => 'digits_between:10,15|required',
            'password' => 'nullable|min:5|required',
            'require_field' => 'required',
            'email' => 'email',
            'payment_method' => 'required',
            'ip' => 'ip',
            'amount' => 'numeric|required',
            'number' => 'numeric',
            'post_unique' => 'required|unique:posts',
            'page_unique' => 'required|unique:pages',
            'host_name' => 'required|unique:hosts',
            'captcha' => 'required|captcha',
        ];

        return $rules[$rule];
    }

    /**
     * function get custom messages
     */
    public function getCustomMessages()
    {
        $messages = [
            'required' => 'Không bỏ trống trường :attribute',
            'unique' => ':attribute đã tồn tại, vui lòng thay đổi nội dung',
            'captcha' => 'Mã captcha bạn nhập không đúng'
        ];

        return $messages;
    }
}
