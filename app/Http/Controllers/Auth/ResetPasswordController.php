<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\PasswordReset;
use App\Notifications\ResetPasswordRequest;

class ResetPasswordController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendMail(Request $request)
    {
        $user = UserService::getUserInfoByEmail($request->email);
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'Email này không tồn tại trong hệ thống!'
            ]);
        }
        $passwordReset = PasswordReset::updateOrCreate([
            'email' => $user->email,
            'token' => Str::random(60)
        ]);
        if ($passwordReset) {
            $user->notify(new ResetPasswordRequest($passwordReset->token));
        }

        return response()->json([
            'success' => true,
            'message' => 'Chúng tôi đã gửi email liên kết đặt lại mật khẩu cho bạn!'
        ]);
    }

    public function reset(Request $request)
    {
        $token = $request->input('token');
        $passwordReset = PasswordReset::where('token', $token)->first();
        // 12 minutes

        if ($passwordReset) {
            if(Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
                $passwordReset->delete();

                return response()->json([
                    'success' => false,
                    'message' => 'Mã token đặt lại mật khẩu đã hết hạn.',
                ]);
            }

            $updatePasswordUser = UserService::updatePasswordByEmail($passwordReset->email, $request->input('password'));
            $passwordReset->delete();
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Mã token đặt lại mật khẩu không hợp lệ.',
            ]);
        }


        return response()->json([
            'success' => true,
            'message' => 'Mật khẩu đã được đặt lại thành công'
        ]);
    }
}
