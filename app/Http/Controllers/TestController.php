<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Harimayco\Menu\Facades\Menu;

class TestController extends Controller
{


    public function __construct()
    {


    }

    /**
     * ======================
     * Method:: INDEX
     * ======================
     */

    public function testMenu()
    {

        return view('admin.test.menu');
    }
}
