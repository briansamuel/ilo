<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Helpers\ArrayHelper;
use App\Helpers\Message;
use App\Http\Controllers\Controller;
use App\Services\LogsUserService;
use App\Services\PageService;
use App\Services\ValidationService;
use App\Services\BlockService;
use Illuminate\Http\Request;

class BlocksController extends Controller
{
    protected $request;
    protected $blockService;
    protected $validator;

    function __construct(Request $request, ValidationService $validator, BlockService $blockService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->blockService = $blockService;
        session()->start();
        session()->put('RF.subfolder', "block");
    }


    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function index()
    {
        return view('admin.blocks.index');
    }


    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function ajaxGetList()
    {
        $params = $this->request->all();

        $result = $this->blockService->getList($params);

        return response()->json($result);
    }

    /**
     * METHOD viewInsert - VIEW ADD, EDIT NEWS
     *
     * @return void
     */

    public function add()
    {
        $sections = ArrayHelper::listBlockTemplate();

        return view('admin.blocks.add', compact('sections'));
    }

    public function addAction()
    {
        $params = $this->request->only('name', 'content', 'slug', 'language', 'active');
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'add_block_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all()), 400);
        }
        $params['content'] = isset($params['content']) ? $params['content'] : '';

        // if (BlockService::checkSlugExist($params['slug'])) {
        //     return response()->json(Message::get(40), 400);
        // }

        $add = BlockService::add($params);
        if ($add) {
            //add log
            $log['action'] = "Thêm mới 1 block có id = " . $add;
            $log['content'] = json_encode($params);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            $data['success'] = true;
            $data['message'] = "Thêm mới block thành công !!!";
        } else {
            $data['message'] = "Lỗi khi thêm mới block !";
        }

        return response()->json($data);
    }

    public function delete($id)
    {
        $detail = $this->blockService->detail($id);
        $delete = $this->blockService->delete($id);
        if ($delete) {
            //add log
            $log['action'] = "Xóa block thành công có ID = " . $id;
            $log['content'] = json_encode($detail);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);
            Message::alertFlash('Bạn đã xóa block thành công', 'success');
        } else {
            Message::alertFlash('Bạn đã xóa block không thành công', 'danger');
        }

        return redirect()->route('block.list');
    }

    public function config($id)
    {
        $blockInfo = $this->blockService->detail($id);
        $config = isset($blockInfo->content) ? json_decode($blockInfo->content) : [];

        return view('admin.blocks.elements.' . $blockInfo->slug, compact('blockInfo', 'config'));
    }

    public function configAction($id)
    {
        $params = $this->request->all();
        if (isset($params['_id'])) {
            unset($params['_id']);
        }
        $params = ArrayHelper::removeArrayNull($params);
        $dataUpdate['content'] = json_encode($params);

        $edit = $this->blockService->edit($id, $dataUpdate);
        if (!$edit) {
            return response()->json(Message::get(13, $lang = '', []), 400);
        }

        //add log
        $log['action'] = "Config block thành công có ID = " . $id;
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Config block thành công !!!";
        return response()->json($data);
    }

    public function edit($id)
    {
        $blockInfo = $this->blockService->detail($id);

        return view('admin.blocks.edit', compact('blockInfo', 'pages'));
    }

    public function editAction($id)
    {
        $params = $this->request->only('name', 'language', 'active');
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'edit_block_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all(), 400));
        }

        $edit = $this->blockService->edit($id, $params);
        if (!$edit) {
            return response()->json(Message::get(13, $lang = '', []), 400);
        }

        //add log
        $log['action'] = "Cập nhập block thành công có ID = " . $id;
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Cập nhập block thành công !!!";
        return response()->json($data);
    }

}
