<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Helpers\ArrayHelper;
use App\Helpers\Message;
use App\Helpers\UploadImage;
use App\Http\Controllers\Controller;
use App\Services\LogsUserService;
use App\Services\UserGroupService;
use App\Services\ValidationService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Session;

class UsersController extends Controller
{
    protected $request;
    protected $userService;
    protected $validator;

    function __construct(Request $request, ValidationService $validator, UserService $userService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->userService = $userService;
    }


    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function index()
    {
        return view('admin.users.index');
    }


    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function ajaxGetList()
    {
        $params = $this->request->all();

        $result = $this->userService->getList($params);

        return response()->json($result);
    }

    /**
     * METHOD viewInsert - VIEW ADD, EDIT NEWS
     *
     * @return void
     */

    public function add()
    {
        $listGroup = UserGroupService::getGroupActive();

        return view('admin.users.add', ['listGroup' => $listGroup]);
    }

    public function addAction()
    {
        $params = $this->request->only('email', 'username', 'full_name', 'password', 'group_id', 'profile_avatar');
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'add_user_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all()), 400);
        }

        $upload = UploadImage::uploadAvatar($params['profile_avatar'], 'user');
        if (!$upload['success']) {
            return response()->json(Message::get(13, $lang = '', []), 400);
        }

        $params['url_avatar'] = $upload['url'];

        $add = UserService::add($params);
        if ($add) {
            //add log
            $log['action'] = "Thêm mới 1 User có id = " . $add;
            $log['content'] = json_encode($params);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            $this->userService->sendMailActiveUser($params['email']);
            $data['success'] = true;
            $data['message'] = "Thêm mới User thành công !!!";
        } else {
            $data['message'] = "Lỗi khi thêm mới User !";
        }

        return response()->json($data);
    }

    public function deleteMany()
    {
        $params = $this->request->only('ids', 'total');
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $delete = $this->userService->deleteMany($params['ids']);
        if (!$delete) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã xóa tổng cộng " . $params['total'] . " User thành công !!!", 'success');

        //add log
        $log['action'] = "Xóa " . $params['total'] . " User thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã xóa tổng cộng " . $params['total'] . " User thành công !!!";
        return response()->json($data);
    }

    public function delete($id)
    {
        $user = $this->userService->detail($id);
        $delete = $this->userService->delete($id);
        if($delete) {
            //add log
            $log['action'] = "Xóa User thành công có ID = " . $id;
            $log['content'] = json_encode($user);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            Message::alertFlash('Bạn đã xóa user thành công', 'success');
        } else {
            Message::alertFlash('Bạn đã xóa user không thành công', 'danger');
        }

        return redirect()->route("user.list");
    }

    public function detail($id)
    {
        $userInfo = $this->userService->detail($id);
        $listGroup = UserGroupService::getGroupActive();

        return view('admin.users.detail', ['userInfo' => $userInfo, 'listGroup' => $listGroup]);
    }

    public function edit($id)
    {
        $userInfo = $this->userService->detail($id);
        $listGroup = UserGroupService::getGroupActive();

        return view('admin.users.edit', ['userInfo' => $userInfo, 'listGroup' => $listGroup]);
    }

    public function editAction($id)
    {
        $params = $this->request->only(['full_name', 'password', 'group_id', 'avatar', 'status']);
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'edit_user_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all(), 400));
        }

        if(isset($params['avatar'])) {
            $upload = UploadImage::uploadAvatar($params['avatar'], 'user');
            if (!$upload['success']) {
                return response()->json(Message::get(13, $lang = '', []), 400);
            }
            $params['avatar'] = $upload['url'];
        }

        if(isset($params['password']) && $params['password'] != '') {
            $params['password'] = bcrypt($params['password']);
        }


        $edit = $this->userService->edit($id, $params);
        if (!$edit) {
            return response()->json(Message::get(13, $lang = '', []), 400);
        }

        //add log
        $log['action'] = "Cập nhập User thành công có ID = " . $id;
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Cập nhập User thành công !!!";
        return response()->json($data);
    }

    public function editManyAction()
    {
        $params = $this->request->only(['status', 'ids', 'total']);
        $params = ArrayHelper::removeArrayNull($params);
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $update = $this->userService->updateMany($params['ids'], ['status' => $params['status']]);
        if (!$update) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã cập nhập tổng cộng " . $params['total'] . " User thành công !!!", 'success');

        //add log
        $log['action'] = "Cập nhập nhiều User thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã cập nhập tổng cộng " . $params['total'] . " User thành công !!!";
        return response()->json($data);
    }

}
