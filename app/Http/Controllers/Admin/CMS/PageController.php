<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Http\Controllers\Controller;
use App\Services\ValidationService;
use App\Services\PageService;
use App\Services\LogsUserService;
use Illuminate\Http\Request;
use App\Helpers\Message;
use App\Helpers\ArrayHelper;

class PageController extends Controller
{
    protected  $request;
    protected  $validator;
    protected  $pageService;

    function __construct(Request $request, ValidationService $validator, PageService $pageService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->pageService = $pageService;
    }

    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function index()
    {
      
        return view('admin.pages.index');
    }

    /**
     * METHOD index - Ajax Get List News
     *
     * @return void
     */

    public function ajaxGetList()
    {
        $params = $this->request->all();
        $column = ['id', 'page_title', 'page_slug' , 'page_status', 'language', 'created_at', 'page_author'];
        $result = $this->pageService->getList( $column, $params );

        return response()->json($result);
    }


    /**
     * METHOD viewInsert - VIEW ADD, EDIT NEWS
     *
     * @return void
     */

    public function add($id = 0)
    {

        $listPageTemplate = ArrayHelper::listPageTemplate();
        $data['listPageTemplate'] = $listPageTemplate;

        if($id > 0) {

            $page = $this->pageService->findByKey('id', $id);

            return view('admin.pages.edit', ['page' => $page, 'listPageTemplate' => $listPageTemplate]);

        } else {

            return view('admin.pages.add', $data);
        }

    }

    public function save() {
        $params = $this->request->all();
        $user = auth()->user();
        $params['created_by_user'] = $user->id;
        $params['updated_by_user'] = $user->id;
        $params['post_author'] = $user->full_name;


        if(isset($params['id']) && $params['id'] > 0) {
            $validator = $this->validator->make($params, 'edit_page_fields');

            if ($validator->fails()) {
                return redirect()->back()->withErrors(['error' => $validator->errors()->all()]);
            }

            $edit = $this->pageService->update($params['id'],$params);

            if ($edit) {



                Message::alertFlash('Bạn đã cập nhật trang thành công', 'success');

                $log['action'] = "Cập nhật trang có ID = ".$params['id']." viết thành công";
                $log['content'] = json_encode($params);
                $log['ip'] = $this->request->ip();
                LogsUserService::add($log);

            } else {

                Message::alertFlash('Đã xảy ra lỗi khi cập nhật trang, vui lòng liên hệ quản trị viên!', 'danger');

            }
        } else {
            $validator = $this->validator->make($params, 'add_page_fields');

            if ($validator->fails()) {
                return redirect()->back()->withErrors(['error' => $validator->errors()->all()]);
            }

            $add = $this->pageService->insert($params);

            if ($add) {


                Message::alertFlash('Bạn đã thêm trang mới thành công', 'success');
                $log['action'] = "Thêm trang có ID = ".$add." viết thành công";
                $log['content'] = json_encode($params);
                $log['ip'] = $this->request->ip();
                LogsUserService::add($log);

            } else {

                Message::alertFlash('Đã xảy ra lỗi khi tạo trang mới, vui lòng liên hệ quản trị viên!', 'danger');

            }
        }


        return redirect()->back()->withInput();
    }

    /**
     * METHOD deleteMany - Delete Array Post with IDs
     *
     * @return json
     */


    public function deleteMany()
    {
        $params = $this->request->only('ids', 'total');
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $delete = $this->pageService->deleteMany($params['ids']);
        if (!$delete) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã xóa tổng cộng " . $params['total'] . " trang thành công !!!", 'success');

        $log['action'] = "Xóa các trang có IDs = ".implode(", ", $params['ids'])." viết thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã xóa tổng cộng " . $params['total'] . " trang thành công !!!";
        return response()->json($data);
    }

    public function delete($id)
    {
        $delete = $this->pageService->delete($id);
        if($delete) {
            Message::alertFlash('Bạn đã xóa trang thành công', 'success');

            $log['action'] = "Xóa trang có ID = ".$id." thành công";
            $log['content'] = '';
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);
        } else {
            Message::alertFlash('Bạn đã xóa trang thất bại', 'danger');
        }

        return redirect()->route('page.list');
    }

    public function editManyAction()
    {
        $params = $this->request->only(['status', 'ids', 'total']);
        $params = ArrayHelper::removeArrayNull($params);
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $update = $this->pageService->updateMany($params['ids'], ['post_status' => $params['status']]);
        if (!$update) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã cập nhập tổng cộng " . $params['total'] . " trang thành công !!!", 'success');

        //add log
        $log['action'] = "Cập nhập các trang có IDs = ".implode(", ", $params['ids'])." viết thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã cập nhập tổng cộng " . $params['total'] . " trang thành công !!!";
        return response()->json($data);
    }


}
