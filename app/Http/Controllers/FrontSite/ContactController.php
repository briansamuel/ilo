<?php

namespace App\Http\Controllers\FrontSite;

use App\Helpers\ArrayHelper;
use App\Helpers\Message;
use App\Services\BlockService;
use App\Services\ContactService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ValidationService;

class ContactController extends Controller
{

    protected $request;
    protected $validator;

    function __construct(Request $request, ValidationService $validator)
    {
        $this->request = $request;
        $this->validator = $validator;
    }

    public function index()
    {
        $data['section1'] = BlockService::getForFrontEnd('contact_section_1');
        $data['section2'] = BlockService::getForFrontEnd('contact_section_2');
        $data['section3'] = BlockService::getForFrontEnd('contact_section_3');
        return view('frontsite.contact.index', $data);
    }

    public function sendContact()
    {
        $params = $this->request->only('name', 'email', 'phone_number', 'date_picker', 'type', 'content', 'captcha');
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'add_frontend_send_contact_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all()), 200);
        }
        $validator = $this->validator->make($params, 'check_frontend_captcha_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(41, $lang = '', $validator->errors()->all()), 200);
        }

        if(isset($params['captcha'])) {
            unset($params['captcha']);
        }

        $add = ContactService::addFromFrontEnd($params);
        if ($add) {
            $data['success'] = true;
            $data['message'] = "Phản hồi của bạn đã được gửi đi thành công!!!";
        } else {
            $data['message'] = "Có lỗi xảy ra, vui lòng thử lại sau!";
        }

        return response()->json($data);
    }
}
