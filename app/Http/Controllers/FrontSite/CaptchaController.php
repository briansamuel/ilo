<?php

namespace App\Http\Controllers\FrontSite;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ValidationService;

class CaptchaController extends Controller
{

    protected $request;
    protected $validator;

    function __construct(Request $request, ValidationService $validator)
    {
        $this->request = $request;
        $this->validator = $validator;
    }

    public function refreshCaptcha()
    {
        return response()->json(['captcha'=> captcha_img('inverse')]);
    }

}
