<?php

namespace App\Http\Controllers\FrontSite;

use App\Services\BannerService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PageService;
use App\Helpers\ArrayHelper;
use App;
class PageController extends Controller
{
    //
    protected $request;
    protected $pageService;
    protected $locale;
    function __construct(Request $request, PageService $pageService)
    {
        $this->request = $request;
        $this->pageService = $pageService;
        
        
    }


    /**
     * ======================
     * Method:: View Home
     * ======================
     */

    public function index($slug)
    {
        $page = $this->pageService->findByKey('page_slug', $slug);
        $data['page'] = $page;
        $data['banners'] = BannerService::getForFrontEndByTypeValue($slug);
        $arrayLang = ArrayHelper::arrayLang();
        $locale = App::getLocale();
        if(!in_array($locale)) {
            $locale = 'vi';
        }
        if($page) {
           
            switch ($page->page_type) {
                case 'page':
                    if($page->page_template == 'about-page') {
                        
                        return view('frontsite.pages.'.$locale.'.'.$page->page_template, $data);
                    }
                    return view('frontsite.pages.'.$page->page_template, $data);
                case 'service':
                    
                    return view('frontsite.services.'.$locale.'.'.$page->page_template, $data);
                default:
                return view('frontsite.pages.default', $data);
                    
    
            }
        }
        
        
    }
}
