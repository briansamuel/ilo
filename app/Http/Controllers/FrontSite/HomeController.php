<?php

namespace App\Http\Controllers\FrontSite;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\BannerService;
use App\Services\BlockService;
use View;
use App;

class HomeController extends Controller
{
    //
    protected $request;
    protected $projectService;
    protected $bannerService;

    function __construct(Request $request, BannerService $bannerService)
    {
        $this->request = $request;
        $this->bannerService = $bannerService;
    }

    /**
     * ======================
     * Method:: View Home
     * ======================
     */

    public function index()
    {
        $data['banners'] = BannerService::getForFrontEnd('home');
        $data['section2'] = BlockService::getForFrontEnd('home_section_2');
        $data['section3'] = BlockService::getForFrontEnd('home_section_3');
        $data['section4'] = BlockService::getForFrontEnd('home_section_4');
        $data['section4']->position_image = ['left_to_right', 'left_to_right', 'top_to_bottom', 'right_to_left', 'right_to_left', 'right_to_left'];
        $data['section5'] = BlockService::getForFrontEnd('home_section_5');

        return view('frontsite.home.index', $data);
    }
}
