<?php

namespace App\Http\Controllers\FrontSite;

use App;
use App\Services\BannerService;
use App\Services\SettingService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PostService;
use App\Services\CategoryPostService;
use App\Services\CategoryService;
use App\Services\BlockService;
class NewsController extends Controller
{
    //
    //
    protected $request;
    protected $postService;
    protected $categoryPostService;

    function __construct(
        Request $request, 
        PostService $postService, 
        CategoryPostService 
        $categoryPostService, 
        CategoryService $categoryService
    )
    {
        $this->request = $request;
        $this->postService = $postService;
        $this->categoryPostService = $categoryPostService;
        $this->categoryService = $categoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = $this->request->input('search', '');
        
        if($search || $search !== '') {
            $filter['post_title'] = $search;
        }
        
        $locale = App::getLocale();

        $filter = array('status' => 'publish',  'language' => $locale);
        $feature_news = $this->postService->takeNew(1, $filter);

        $filter = array('status' => 'publish',  'offset' => 1, 'language' => $locale);
        $news = $this->postService->takeNew(3, $filter);

        $filter = array('status' => 'publish',  'offset' => 4, 'language' => $locale);
        $slide_news = $this->postService->takeNew(6, $filter);
        $section3 = BlockService::getForFrontEnd('news_event_section_3');
        return view('frontsite.news.index', compact('feature_news', 'news', 'slide_news', 'section3'));
    }

    public function search(){
        $search = $this->request->input('search', '');
        $list = [];
        if(!$search || $search === '') {
            return [];
        }
        // check if ajax request is coming or not
        if($this->request->ajax()) {
            // select country name from database
            $data = $this->postService->searchNews($this->request->search);

            foreach($data as $item) {
                $list[] = array('title' => $item->post_title, 'url' => '//'.config('domain.web.domain').'/'.$item->post_slug.'.html');
            }
        }

        return $list;
    }

    /**
     * ======================
     * Method:: View News Detail
     * ======================
     */

    public function newsDetail($slug)
    {
        
        $locale = App::getLocale();

        $filter = array('slug' => $slug, 'language' => $locale);
        
        $news = $this->postService->findByMultiKey($filter);
        if(!$news) {
            return redirect()->route('frontsite.news');
        }
        $breadcrumbs[] = (object) array('slug' => 'tin-tuc', 'title' => __('seo.news.title'));
        $filter = array('status' => 'publish', 'post_type' => 'news');
        $data['hot_news'] =  $this->postService->takeHotNew(4, $filter);
        $filter['exclude'] = $news->id;
        $data['related_news'] =  $this->postService->takeNew(6, $filter);
        $filter = array('status' => 'publish', 'post_type' => 'video');
        $data['news'] = $news;
        $data['breadcrumbs'] = $breadcrumbs;
        // update view
        $this->postService->updateView($news->id);
        return view('frontsite.news.detail', $data);
        
    }
}
