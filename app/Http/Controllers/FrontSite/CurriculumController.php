<?php

namespace App\Http\Controllers\FrontSite;

use App\Services\BlockService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ValidationService;

class CurriculumController extends Controller
{

    protected $request;
    protected $validator;

    function __construct(Request $request, ValidationService $validator)
    {
        $this->request = $request;
        $this->validator = $validator;
    }

    public function index()
    {
        $data['section1'] = BlockService::getForFrontEnd('curriculum_section_1');
        $data['section2'] = BlockService::getForFrontEnd('curriculum_section_2');
        $data['section3'] = BlockService::getForFrontEnd('curriculum_section_3');
        $data['section4'] = BlockService::getForFrontEnd('curriculum_section_4');

        return view('frontsite.curriculum.index', $data);
    }
}
