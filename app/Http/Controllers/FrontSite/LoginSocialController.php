<?php

namespace App\Http\Controllers\FrontSite;

use App\Http\Controllers\Controller;
use App\Services\GuestService;
use Laravel\Socialite\Facades\Socialite;
use Auth;

class LoginSocialController extends Controller
{
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function callback($provider)
    {
        $getInfo = Socialite::driver($provider)->stateless()->user();
        $user = $this->createUser($getInfo, $provider);
        Auth::guard('user')->loginUsingId($user, true);

        return redirect()->to('/');
    }

    function createUser($getInfo, $provider)
    {
        if($user = GuestService::getByProviderId($getInfo->id)) {
            return $user->id;
        }

        if($user = GuestService::getByEmail($getInfo->email)) {
            return $user->id;
        }

        $nickname = explode("@", $getInfo->getEmail())[0];

        $params['guest_avatar'] = $getInfo->getAvatar();

        $params['username'] = $getInfo->getNickname() !== null ? $getInfo->getNickname() : $nickname;
        $params['full_name'] = $getInfo->getName();
        $params['email'] = $getInfo->getEmail();
        $params['password'] = '';
        $params['provider'] = $provider;
        $params['provider_id'] = $getInfo->id;

        $add = GuestService::addBySocial($params);

        return $add;
    }

}
