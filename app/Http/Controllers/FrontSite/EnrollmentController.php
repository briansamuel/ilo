<?php

namespace App\Http\Controllers\FrontSite;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ValidationService;
use App\Services\BannerService;
use App\Services\BlockService;

class EnrollmentController extends Controller
{

    protected $request;
    protected $validator;

    function __construct(Request $request, ValidationService $validator)
    {
        $this->request = $request;
        $this->validator = $validator;
    }

    public function index()
    {

        $data['banners'] = BannerService::getForFrontEnd('enrollment');
        $data['section1'] = BlockService::getForFrontEnd('enrollment_section_1');
        $data['section2'] = BlockService::getForFrontEnd('enrollment_section_2');
        $data['section3'] = BlockService::getForFrontEnd('enrollment_section_3');
        
        return view('frontsite.enrollment.index', $data);
    }
}
