<?php

namespace App\Http\Middleware;

use App\Services\Auth\AuthPermissionService;
use App\Services\Auth\AuthService;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Authenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        Log::info('Log request', ['uri' => $request->path(), 'params' => $request->all()]);
        if (!AuthService::checkLogin()) {
            return redirect()->route('login');
        }

        //check permission
        $AuthPermission = new AuthPermissionService($request);
        if (!$AuthPermission->check()) {
            return redirect()->route('permission_denied');
        }

        return $next($request);
    }
}
