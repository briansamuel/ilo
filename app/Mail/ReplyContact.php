<?php

namespace App\Mail;

use App\Services\SettingService;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReplyContact extends Mailable
{
    use Queueable, SerializesModels;

    public $contact;
    public $contactReply;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($contact, $contactReply)
    {
        $this->contact = $contact;
        $this->contactReply = $contactReply;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($address = 'noreply@gmail.com', $name = SettingService::get('notification::reply::name'))
                    ->subject(SettingService::get('notification::reply::subject'))
                    ->view('emails.reply_contact')
                    ->with([
                        'orderName' => $this->contact,
                        'contactReply' => $this->contactReply,
                    ]);
    }
}
