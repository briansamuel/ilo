<?php

return array (
  'home' => 
  array (
    'title' => 'Trang chủ',
    'keyword' => 'SenHos',
    'description' => 'Trang chủ',
  ),
  'news' => 
  array (
    'title' => 'Tin tức Test',
    'title_ab' => 'Tin tức',
    'keyword' => 'Tin tức của SenHos',
    'description' => 'Tin tức của SenHos',
  ),
  'partner' => 
  array (
    'title' => 'Đối tác',
    'title_ab' => 'Đối tác',
    'keyword' => 'Đối tác',
    'description' => 'Đối tác',
  ),
  'project' => 
  array (
    'title' => 'Dự án',
    'title_ab' => 'Dự án',
    'keyword' => 'Dự án',
    'description' => 'Dự án',
  ),
  'recruitment' => 
  array (
    'title' => 'Tuyển dụng',
  ),
  'video' => 
  array (
    'title' => 'Thư viện video',
  ),
);
