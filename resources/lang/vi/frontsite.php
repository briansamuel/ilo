<?php

return array (
  'header' => 
  array (
    'site_name' => 'ILO',
    'show_by' => 'Chương trình bởi',
    'performed_by' => 'Thực hiện bởi',
  ),
  'news' => 
  array (
    'load_more' => 'Xem thêm',
    'all' => 'Tất cả',
    'type_news' => 'Tin tức',
    'type_event' => 'Sự kiện',
    'new_feature' => 'Mới nhất',
    'new_and_event' => 'Tin tức & Sự kiện',
    'related_news' => 'Tin liên quan',
  ),
  'elements' => 
  array (
    'contact' => 
    array (
      'title' => 'Ghé thăm ILO Academy nhé!',
      'description' => '“Vui lòng liên hệ khi bạn cần thêm thông tin chi tiết.”',
      'title_contact' => 'Đặt lịch tham quan:',
      'name' => 'Tên *',
      'phone' => 'Số điện thoại *',
      'type_captcha' => 'Nhập captcha',
      'comfirm' => 'Xác nhận',
      'email' => 'Email *',
      'date' => 'Chọn ngày',
      'sub_title' => 'Đăng ký để nhận tư vấn',
      'register_now' => 'Đăng ký ngay',
    ),
    'contact_advisory' => 
    array (
      'title' => 'ILO Academy',
      'sub_title' => 'Đăng ký để nhận tư vấn',
      'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque nec imperdiet turpis, a pretium nunc. Duis vestibulum nibh quis tellus ultrices, vel placerat nisi convallis. Curabitur et ligula non felis consequat fringilla sed pretium erat. In suscipit elementum nisl, non eleifend sapien auctor et. Nunc sed ullamcorper felis. Mauris tristique feugiat nisl sit amet pharetra. Nam varius porttitor purus.',
      'contact_title' => 'Đăng ký để nhận tư vấn các khóa học:',
      'advisory_about' => 'Phụ huynh muốn được tư vấn về*',
    ),
  ),
  'enrollment' => 
  array (
    'load_more' => 'Xem thêm',
  ),
  'home' => 
  array (
    'preschool' => 'Trường mầm non',
    'bilingual' => 'Song ngữ',
  ),
);
