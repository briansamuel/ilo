<?php

return array (
  'guest' => 
  array (
    'denied-permission' => 'Bạn không có quyền truy cập vào trang này',
  ),
  'add_review' => 
  array (
    'success' => 'Đánh giá thành công!',
    'fails' => 'Đánh giá thất bại, vui lòng liên hệ quản trị viên SenHos!',
  ),
  'add_booking' => 
  array (
    'fails' => 'Đặt phòng thất bại, vui lòng liên hệ quản trị viên SenHos!',
  ),
);
