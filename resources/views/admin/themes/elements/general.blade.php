<div class="tab-pane active" id="general" role="tabpanel">
    

    <!-- Ring Phone Setting -->
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Ring Phone?:</label>
        <div class="col-lg-6">
            <span class="kt-switch">
                <label>
                    <input type="checkbox" class="form-control" name="theme_option::general::ring_phone" id="ring_phone" onclick="showOptionRingPhone()" <?= isset($option['theme_option::general::ring_phone']) && $option['theme_option::general::ring_phone'] === 'on' ? 'checked' : '' ?>>
                    <input type="hidden" name="theme_option::general::ring_phone" id="ring_phone_hidden">
                    <span></span>
                </label>
            </span>
        </div>
    </div>
    <div class="div_wrap_ring_phone">
        <div class="form-group row">
            <label class="col-lg-3 col-form-label">Ring Phone Text:</label>
            <div class="col-lg-6">
                <input type="text" class="form-control" name="theme_option::general::ring_phone_text" value="{{isset($option['theme_option::general::ring_phone_text']) ? $option['theme_option::general::ring_phone_text'] : ''}}">
            </div>
        </div>
        <div class="form-group row" >
            <label class="col-lg-3 col-form-label">Số điện thoại:</label>
            <div class="col-lg-6">
                <input type="text" class="form-control" name="theme_option::general::ring_phone_number" value="{{isset($option['theme_option::general::ring_phone_number']) ? $option['theme_option::general::ring_phone_number'] : ''}}">
            </div>
        </div>
    </div>
    
</div>