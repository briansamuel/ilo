@extends('admin.index')
@section('page-header', 'Block')
@section('page-sub_header', 'Cập nhập block')
@section('style')
    <link rel="stylesheet" href="admin/plugins/fancybox/jquery.fancybox.min.css"/>
@endsection
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Quản Lý Block </h3>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <form id="kt_edit_form">
                <div class="row">
                    <div class="col-md-8 col-lg-9">
                        <div class="kt-portlet">
                            <div class="kt-portlet__head kt-portlet__head--right">
                                <div class="kt-portlet__head-label ">
                                    <span class="kt-font-danger"><i class="fa fa-star"></i> Bắt buộc phải nhập / chọn nội dung</span>
                                </div>
                            </div>
                            <!--begin::Form-->
                            <div class="kt-form">
                                <div class="kt-portlet__body">
                                    <div class="form-group row">
                                        <label for="title" class="col-12 col-lg-12 col-xl-3 col-form-label">Name:</label>
                                        <div class="col-12 col-lg-12 col-xl-9">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_id" id="_id" value="{{$blockInfo->id}}"/>
                                            <input class="form-control" type="text" value="{{$blockInfo->name}}"
                                                   id="name" name="name"
                                                   placeholder="Tiêu đề bắt buộc phải nhập nội dung">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="slug" class="col-12 col-lg-12 col-xl-3 col-form-label">Section:</label>
                                        <div class="col-12 col-lg-12 col-xl-9">
                                            <input type="text" class="form-control" disabled="" value="{{$blockInfo->slug}}" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="active"
                                               class="col-12 col-lg-12 col-xl-3 col-form-label">Active:</label>
                                        <div class="col-12 col-lg-12 col-xl-9">
                                            <select class="form-control kt-select2" id="active" name="active">
                                                <option value="yes" <?= $blockInfo->active === 'yes' ? 'selected' : ''; ?>>
                                                    Yes
                                                </option>
                                                <option value="no" <?= $blockInfo->active === 'no' ? 'selected' : ''; ?>>
                                                    No
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="kt-portlet">
                            <div class="kt-portlet__body p-3">
                                <div class="form-group row mb-3">
                                    <label for="language" class="col-4 col-form-label">Ngôn ngữ:</label>
                                    <div class="col-8">
                                        <select class="form-control kt-select2" id="language" name="language">
                                            @foreach($arrayLang as $key=>$lang)
                                                <option value="{{$key}}" {{ $blockInfo->language === $key ? 'selected' : '' }}>{{$lang}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot kt-align-right p-2">
                                <div>
                                    <button type="button" class="btn btn-primary" id="btn_edit"><i
                                                class="la la-save"></i> Lưu dữ liệu
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- end:: Content -->
    </div>
@endsection
@section('script')
    <!--end::Page Vendors -->
    <script src="assets/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="admin/js/pages/block/edit-block.js?v1.3" type="text/javascript"></script>
    <script src="admin/plugins/fancybox/jquery.fancybox.min.js"></script>
    <script src="admin/plugins/fancybox/jquery.observe_field.js"></script>
@endsection