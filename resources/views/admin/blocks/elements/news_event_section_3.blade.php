@extends('admin.index')
@section('page-header', 'Block')
@section('page-sub_header', 'Config Block')
@section('style')
<link rel="stylesheet" href="admin/plugins/fancybox/jquery.fancybox.min.css" />
@endsection
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    {{$blockInfo->name}} </h3>
            </div>
        </div>
    </div>

    <!-- end:: Subheader -->

    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <form id="kt_edit_form">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="kt-portlet">
                        <div class="kt-portlet__head kt-portlet__head--right">
                            <div class="kt-portlet__head-label ">
                                <span class="kt-font-danger"><i class="fa fa-star"></i> Bắt buộc phải nhập / chọn nội dung</span>
                            </div>
                        </div>
                        <!--begin::Form-->
                        <div class="kt-form">
                            <div class="kt-portlet__body">
                                <div class="form-group row">
                                    <label for="title" class="col-12 col-lg-12 col-xl-3 col-form-label">Tiêu
                                        đề:</label>
                                    <div class="col-12 col-lg-12 col-xl-9">
                                        {{csrf_field()}}
                                        <input type="hidden" name="_id" id="_id" value="{{$blockInfo->id}}" />
                                        <input class="form-control" type="text" value="{{isset($config->title) ? $config->title : ''}}" id="title" name="title" placeholder="Tiêu đề bắt buộc phải nhập nội dung">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="content" class="col-12 col-lg-12 col-xl-3 col-form-label">Nội dung:</label>
                                    <div class="col-12 col-lg-12 col-xl-9">
                                        <textarea class="form-control" name="content" id="content" cols="30" rows="10">{{isset($config->content) ? $config->content : ''}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Slider</label>
                                    <div class="col-md-9">
                                        <div id="div_slider">
                                            @if(isset($config->slider))
                                            @foreach($config->slider as $key=>$slider)
                                            <div class="row row_slider" id="div_slider_{{$key}}">
                                                <div class="col-md-1">
                                                    <a id="div_image" data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=image_{{$key}}&lang=vi&akey=@filemanager_get_key()" class="iframe-btn" data-fancybox data-fancybox data-type="iframe" href="javascript:;">
                                                        @if(isset($slider->image) && $slider->image != '')
                                                        <img id="preview_thumbnail" class="img-fluid" src="{{ $slider->image }}" style="min-height: 45px">
                                                        @else
                                                        <img id="preview_thumbnail" class="img-fluid" src="admin/images/upload-thumbnail.png">
                                                        @endif
                                                    </a>
                                                </div>
                                                <div class="col-md-4">
                                                    <input class="form-control upload_image" placeholder="Nhập đường dẫn slider vào đây" name="slider[{{$key}}][image]" id="image_{{$key}}" value="{{isset($slider->image) ? $slider->image : ''}}" />
                                                </div>
                                                <div class="col-md-6">
                                                    <select id="category_{{$key}}" class="form-control" name="slider[{{$key}}][category]">
                                                        <option value="student" {{isset($slider->category) && $slider->category == 'student'  ? 'selected' : ''}}> Học viên</option>
                                                        <option value="infrastructure" {{isset($slider->category) && $slider->category == 'infrastructure'  ? 'selected' : ''}}> Cơ sở vật chất</option>
                                                        <option value="curricular" {{isset($slider->category) && $slider->category == 'curricular'  ? 'selected' : ''}}> Chương trình ngoại khóa</option>

                                                    </select>

                                                </div>
                                                @if($key !== 0)
                                                <div class="col-md-1">
                                                    <a href="javascript:;" class="btn btn-danger delete_row" style="width: 43px; height:39px;">
                                                        <i class="flaticon2-delete"></i>
                                                    </a>
                                                </div>
                                                @else
                                                <div class="col-md-1"></div>
                                                @endif
                                                <div class="col-md-1"></div>
                                                <div class="col-md-5">
                                                    <input class="form-control" placeholder="Nhập tiêu đề slide vào đây" name="slider[{{$key}}][title]" id="title_{{$key}}" value="{{isset($slider->title) ? $slider->title : ''}}" />
                                                </div>
                                                <div class="col-md-5">
                                                    <input class="form-control" placeholder="Nhập link slide" name="slider[{{$key}}][link]" id="link_{{$key}}" value="{{isset($slider->link) ? $slider->link : ''}}" />
                                                </div>
                                            </div>
                                            <hr>
                                            @endforeach
                                            @else
                                            <div class="row" id="div_slider_0">
                                                <div class="col-md-1">
                                                    <a id="div_image" data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=image_0&lang=vi&akey=@filemanager_get_key()" class="iframe-btn" data-fancybox data-fancybox data-type="iframe" href="javascript:;">
                                                        <img id="preview_thumbnail" class="img-fluid" src="admin/images/upload-thumbnail.png">
                                                    </a>
                                                </div>
                                                <div class="col-md-4">
                                                    <input class="form-control" placeholder="Nhập đường dẫn vào đây" name="slider[0][image]" id="image_0" />
                                                </div>
                                                <div class="col-md-6">
                                                    <select id="category_0" class="form-control" name="slider[0][category]">
                                                        <option value="student"> Học viên</option>
                                                        <option value="infrastructure"> Cơ sở vật chất</option>
                                                        <option value="curricular"> Chương trình ngoại khóa</option>

                                                    </select>

                                                </div>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-5">
                                                    <input class="form-control" placeholder="Nhập tiêu đề slide vào đây" name="slider[0][title]" id="title_0" value="" />
                                                </div>
                                                <div class="col-md-5">
                                                    <input class="form-control" placeholder="Nhập link slide" name="slider[0][link]" id="link_0" value="" />
                                                </div>
                                            </div>
                                            <hr>
                                            @endif
                                        </div>
                                        <a href="javascript:;" class="btn btn-success mt-repeater-add pull-right" onclick="add_attribute()">
                                            <i class="fa fa-plus"></i> Thêm slider
                                        </a>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="content" class="col-12 col-lg-12 col-xl-3 col-form-label"></label>
                                    <button type="button" class="btn btn-primary" id="btn_edit"><i class="la la-save"></i> Lưu dữ liệu
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- end:: Content -->
</div>
@endsection
@section('script')
<!--end::Page Vendors -->
<script src="assets/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js" type="text/javascript"></script>
<script src="admin/js/pages/block/config/config-home-block-5.js?v1.1" type="text/javascript"></script>
<script src="admin/plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="assets/js/pages/crud/file-upload/dropzonejs.js?v2" type="text/javascript"></script>
<script src="admin/plugins/fancybox/jquery.observe_field.js"></script>


<script>
    $("#upload").dropzone({
        url: "/upload-image",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: 5,
        addRemoveLinks: !0,
        sending: function(file, xhr, formData) {
            formData.append("_token", "{{ csrf_token() }}");
        },
        removedfile: function(file) {
            var name = $("#image_1").val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                },
                type: 'POST',
                url: "destroy-image",
                data: {
                    filename: name
                },
                success: function(data) {
                    $("#image_1").val('');
                },
                error: function(e) {
                    console.log(e);
                }
            });
            var fileRef;
            return (fileRef = file.previewElement) != null ?
                fileRef.parentNode.removeChild(file.previewElement) : void 0;
        },
        success: function(file, response) {
            if (response.success) {

            } else {
                alert('Có lỗi xảy ra, vui lòng thử lại sau');
            }
        },
        error: function(file, response) {
            alert('Có lỗi xảy ra, vui lòng thử lại sau');
        }
    });

    $('.iframe-btn').fancybox({
        'iframe': {
            'css': {
                'width': '90%',
                'height': '90%',
            }
        },
    });
</script>

<script>
    var total = {{isset($config->slider) ? count($config->slider) : 0}};

    function add_attribute() {
        total++;
        let container_html = `
                <div class="row row_slider">
                    <div class="col-md-1">
                        <a id="div_image"
                           data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=image_${total}&lang=vi&akey=@filemanager_get_key()"
                           class="iframe-btn" data-fancybox data-fancybox data-type="iframe"
                           href="javascript:;">
                            <img id="preview_thumbnail" class="img-fluid" src="admin/images/upload-thumbnail.png">
                        </a>
                    </div>
                    <div class="col-md-4">
                        <input class="form-control" placeholder="Nhập đường dẫn vào đây" name="slider[${total}][image]" id="image_${total}"/>
                    </div>
                    <div class="col-md-6">
                        <select id="category_${total}" class="form-control" name="slider[${total}][category]">
                            <option value="student"> Học viên</option>
                            <option value="infrastructure"> Cơ sở vật chất</option>
                            <option value="curricular"> Chương trình ngoại khóa</option>

                        </select>
                    </div>
                    <div class="col-md-1">
                        <a href="javascript:;" class="btn btn-danger delete_row" style="width: 43px; height:39px;">
                            <i class="flaticon2-delete"></i>
                        </a>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                                                    <input class="form-control" placeholder="Nhập tiêu đề slide vào đây" name="slider[${total}][title]" id="title_${total}" value="" />
                                                </div>
                                                <div class="col-md-5">
                                                    <input class="form-control" placeholder="Nhập link slide" name="slider[${total}][link]" id="link_${total}" value="" />
                                                </div>
                </div>
                <hr>
            `;
        $("#div_slider").append(container_html);
    }

    $(document).on('click', 'a.delete_row', function() {
        if (!window.confirm('Bạn có chắc muốn xóa dòng này đi không?')) return true;

        $(this).closest('.row_slider').remove();
    });
</script>

@endsection