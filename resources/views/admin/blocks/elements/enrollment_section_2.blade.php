@extends('admin.index')
@section('page-header', 'Block')
@section('page-sub_header', 'Config Block')
@section('style')
<link rel="stylesheet" href="admin/plugins/fancybox/jquery.fancybox.min.css" />
@endsection
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    {{$blockInfo->name}} </h3>
            </div>
        </div>
    </div>

    <!-- end:: Subheader -->

    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <form id="kt_edit_form">
            {{csrf_field()}}
            <input type="hidden" name="_id" id="_id" value="{{$blockInfo->id}}" />
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="kt-portlet">
                        <div class="kt-portlet__head kt-portlet__head--right">
                            <div class="kt-portlet__head-label ">
                                <span class="kt-font-danger"><i class="fa fa-star"></i> Bắt buộc phải nhập / chọn nội dung</span>
                            </div>
                        </div>
                        <!--begin::Form-->
                        <div class="kt-form">
                            <div class="kt-portlet__body">
                                <div class="form-group row">
                                    <label for="title" class="col-12 col-lg-12 col-xl-3 col-form-label">Tiêu
                                        đề:</label>
                                    <div class="col-12 col-lg-12 col-xl-9">
                                        {{csrf_field()}}
                                        <input type="hidden" name="_id" id="_id" value="{{$blockInfo->id}}" />
                                        <input class="form-control" type="text" value="{{isset($config->title) ? $config->title : ''}}" id="title" name="title" placeholder="Tiêu đề bắt buộc phải nhập nội dung">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="sub_title" class="col-12 col-lg-12 col-xl-3 col-form-label">Tiêu đề phụ:</label>
                                    <div class="col-12 col-lg-12 col-xl-9">
                                        <input class="form-control" type="text" value="{{isset($config->sub_title) ? $config->sub_title : ''}}" id="sub_title" name="sub_title" placeholder="Tiêu đề phụ bắt buộc phải nhập nội dung">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="sub_title" class="col-12 col-lg-12 col-xl-3 col-form-label">Tiêu đề tab:</label>
                                    <div class="col-12 col-lg-12 col-xl-3">
                                        <input class="form-control" type="text" value="{{isset($config->tab[0]) ? $config->tab[0] : ''}}" id="tab[0]" name="tab[0]" placeholder="Tiêu đề tab bắt buộc phải nhập nội dung">
                                    </div>
                                    <div class="col-12 col-lg-12 col-xl-3">
                                        <input class="form-control" type="text" value="{{isset($config->tab[1]) ? $config->tab[1] : ''}}" id="tab[1]" name="tab[1]" placeholder="Tiêu đề tab bắt buộc phải nhập nội dung">
                                    </div>
                                    <div class="col-12 col-lg-12 col-xl-3">
                                        <input class="form-control" type="text" value="{{isset($config->tab[2]) ? $config->tab[2] : ''}}" id="tab[2]" name="tab[2]" placeholder="Tiêu đề tab bắt buộc phải nhập nội dung">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="title" class="col-12 col-lg-12 col-xl-3 col-form-label">Học phí chính khóa:</label>
                                    <div class="col-12 col-lg-12 col-xl-9">

                                        <div class="table-responsive">
                                            <table id="table_fee" class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Hệ đào tạo</th>
                                                        <th scope="col">Mức phí/ tháng</th>
                                                        <th scope="col">Kỳ hè</th>
                                                        <th scope="col">Kỳ 1</th>
                                                        <th scope="col">Kỳ 2</th>
                                                        <th scope="col">Cả năm</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(isset($config->fee))
                                                    @foreach($config->fee as $i => $row)

                                                    <tr>
                                                        @foreach($row as $j => $col)
                                                        @if($j == 0)
                                                        <th scope="row"><input type="text" name="fee[{{$i}}][{{$j}}]" value="{{isset($col) ? $col : ''}}"></th>
                                                        @else
                                                        <td><input type="text" name="fee[{{$i}}][{{$j}}]" value="{{isset($col) ? $col : ''}}"></td>
                                                        @endif
                                                        @endforeach
                                                    </tr>
                                                    @endforeach
                                                    @else
                                                    <tr>
                                                        <th scope="row"><input type="text" name="fee[0][0]" value=""></th>
                                                        <td><input type="text" name="fee[0][1]" value=""></td>
                                                        <td><input type="text" name="fee[0][2]" value=""></td>
                                                        <td><input type="text" name="fee[0][3]" value=""></td>
                                                        <td><input type="text" name="fee[0][4]" value=""></td>
                                                        <td><input type="text" name="fee[0][5]" value=""></td>
                                                    </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                        <hr>
                                        <a href="javascript:;" class="btn btn-success mt-repeater-add pull-right" onclick="add_attribute()">
                                            <i class="fa fa-plus"></i> Thêm hệ đào tạo
                                        </a>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="sub_title" class="col-12 col-lg-12 col-xl-3 col-form-label">Ghi chú:</label>
                                    <div class="col-12 col-lg-12 col-xl-9">
                                        <textarea class="form-control" rows="8" type="text" id="note" name="note" placeholder="Nhập nội dung ghi chú">{{isset($config->note) ? $config->note : ''}}</textarea>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Các phí dịch vụ khác</label>
                                    <div class="col-md-9">
                                        <textarea id="fee_service_other" name="fee_service_other" class="tox-target">{{isset($config->fee_service_other) ? $config->fee_service_other : ''}}</textarea>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Chính sách ưu đãi</label>
                                    <div class="col-md-9">
                                        <textarea id="incentives" name="incentives" class="tox-target">{{isset($config->incentives) ? $config->incentives : ''}}
                                        </textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="content" class="col-12 col-lg-12 col-xl-3 col-form-label"></label>
                                    <button type="button" class="btn btn-primary" id="btn_edit"><i class="la la-save"></i> Lưu dữ liệu
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- end:: Content -->
</div>
@endsection
@section('vendor-script')

<script src="assets/plugins/custom/tinymce/tinymce.bundle.js" type="text/javascript"></script>
@endsection
@section('script')
<!--end::Page Vendors -->
<script src="assets/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js" type="text/javascript"></script>
<script src="admin/js/pages/block/config/config-enrollment-block-2.js?v1.4" type="text/javascript"></script>
<script src="admin/plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="assets/js/pages/crud/file-upload/dropzonejs.js?v2" type="text/javascript"></script>
<script src="admin/plugins/fancybox/jquery.observe_field.js"></script>
<script src="http://keenthemes.com/preview/metronic/theme/assets/global/plugins/jquery-repeater/jquery.repeater.js" type="text/javascript"></script>

<script>
    $("#upload").dropzone({
        url: "/upload-image",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: 5,
        addRemoveLinks: !0,
        sending: function(file, xhr, formData) {
            formData.append("_token", "{{ csrf_token() }}");
        },
        removedfile: function(file) {
            var name = $("#image_1").val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                },
                type: 'POST',
                url: "destroy-image",
                data: {
                    filename: name
                },
                success: function(data) {
                    $("#image_1").val('');
                },
                error: function(e) {
                    console.log(e);
                }
            });
            var fileRef;
            return (fileRef = file.previewElement) != null ?
                fileRef.parentNode.removeChild(file.previewElement) : void 0;
        },
        success: function(file, response) {
            if (response.success) {

            } else {
                alert('Có lỗi xảy ra, vui lòng thử lại sau');
            }
        },
        error: function(file, response) {
            alert('Có lỗi xảy ra, vui lòng thử lại sau');
        }
    });

    $('.iframe-btn').fancybox({
        'iframe': {
            'css': {
                'width': '90%',
                'height': '90%',
            }
        },
    });
</script>
<script>
    ! function(t) {
        var e = {};

        function n(i) {
            if (e[i]) return e[i].exports;
            var r = e[i] = {
                i: i,
                l: !1,
                exports: {}
            };
            return t[i].call(r.exports, r, r.exports, n), r.l = !0, r.exports
        }
        n.m = t, n.c = e, n.d = function(t, e, i) {
            n.o(t, e) || Object.defineProperty(t, e, {
                enumerable: !0,
                get: i
            })
        }, n.r = function(t) {
            "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
                value: "Module"
            }), Object.defineProperty(t, "__esModule", {
                value: !0
            })
        }, n.t = function(t, e) {
            if (1 & e && (t = n(t)), 8 & e) return t;
            if (4 & e && "object" == typeof t && t && t.__esModule) return t;
            var i = Object.create(null);
            if (n.r(i), Object.defineProperty(i, "default", {
                    enumerable: !0,
                    value: t
                }), 2 & e && "string" != typeof t)
                for (var r in t) n.d(i, r, function(e) {
                    return t[e]
                }.bind(null, r));
            return i
        }, n.n = function(t) {
            var e = t && t.__esModule ? function() {
                return t.default
            } : function() {
                return t
            };
            return n.d(e, "a", e), e
        }, n.o = function(t, e) {
            return Object.prototype.hasOwnProperty.call(t, e)
        }, n.p = "", n(n.s = 677)
    }({
        677: function(t, e, n) {
            "use strict";
            var i = {
                init: function() {
                    tinymce.init({
                            selector: "#fee_service_other",
                            menubar: !1,
                            paste_data_images: true,
                            relative_urls: false,
                            remove_script_host: false,
                            toolbar: ["styleselect fontselect fontsizeselect", "undo redo | cut copy paste | bold italic | link image | alignleft aligncenter alignright alignjustify", "bullist numlist | outdent indent | blockquote subscript superscript | advlist | autolink | lists charmap | print preview |  code"],
                            plugins: [
                                "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                                "table contextmenu directionality emoticons paste textcolor code"
                            ],
                            image_advtab: true,
                            filemanager_access_key: '@filemanager_get_key()',
                            filemanager_sort_by: '',
                            filemanager_descending: '',
                            filemanager_subfolder: '',
                            filemanager_crossdomain: '',
                            external_filemanager_path: '@filemanager_get_resource(dialog.php)',
                            filemanager_title: "Responsive Filemanager",
                            external_plugins: {
                                "filemanager": "/vendor/responsivefilemanager/plugin.min.js"
                            }
                        }),
                        tinymce.init({
                            selector: "#incentives",
                            menubar: !1,
                            paste_data_images: true,
                            relative_urls: false,
                            remove_script_host: false,
                            toolbar: ["styleselect fontselect fontsizeselect", "undo redo | cut copy paste | bold italic | link image | alignleft aligncenter alignright alignjustify", "bullist numlist | outdent indent | blockquote subscript superscript | advlist | autolink | lists charmap | print preview |  code"],
                            plugins: [
                                "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                                "table contextmenu directionality emoticons paste textcolor code"
                            ],
                            image_advtab: true,
                            filemanager_access_key: '@filemanager_get_key()',
                            filemanager_sort_by: '',
                            filemanager_descending: '',
                            filemanager_subfolder: '',
                            filemanager_crossdomain: '',
                            external_filemanager_path: '@filemanager_get_resource(dialog.php)',
                            filemanager_title: "Responsive Filemanager",
                            external_plugins: {
                                "filemanager": "/vendor/responsivefilemanager/plugin.min.js"
                            }
                        })
                }
            };
            jQuery(document).ready((function() {
                i.init()
            }))
        }
    });
</script>
<script>
    var total = {
        {
            isset($config - > fee) ? count($config - > fee) : 0
        }
    };

    function add_attribute() {
        total++;
        let container_html = `
                <tr class="row_slider">
                    <th scope="row"><input type="text" name="fee[${total}][0]" value=""></th>
                    <td><input type="text" name="fee[${total}][1]" value=""></td>
                    <td><input type="text" name="fee[${total}][2]" value=""></td>
                    <td><input type="text" name="fee[${total}][3]" value=""></td>
                    <td><input type="text" name="fee[${total}][4]" value=""></td>
                    <td><input type="text" name="fee[${total}][5]" value=""></td>
                </tr>
            `;
        $("#table_fee").append(container_html);
    }

    $(document).on('click', 'a.delete_row', function() {
        if (!window.confirm('Bạn có chắc muốn xóa slider này đi không?')) return true;

        $(this).closest('.row_slider').remove();
    });
</script>

@endsection