@extends('admin.index')
@section('page-header', 'Block')
@section('page-sub_header', 'Config Block')
@section('style')
    <link rel="stylesheet" href="admin/plugins/fancybox/jquery.fancybox.min.css"/>
@endsection
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        {{$blockInfo->name}} </h3>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <form id="kt_edit_form">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="kt-portlet">
                            <div class="kt-portlet__head kt-portlet__head--right">
                                <div class="kt-portlet__head-label ">
                                    <span class="kt-font-danger"><i class="fa fa-star"></i> Bắt buộc phải nhập / chọn nội dung</span>
                                </div>
                            </div>
                            <!--begin::Form-->
                            <div class="kt-form">
                                <div class="kt-portlet__body">
                                    <div class="form-group row">
                                        <label for="title" class="col-12 col-lg-12 col-xl-3 col-form-label">Tiêu
                                            đề:</label>
                                        <div class="col-12 col-lg-12 col-xl-9">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_id" id="_id" value="{{$blockInfo->id}}"/>
                                            <input class="form-control" type="text" value="{{isset($config->title) ? $config->title : ''}}"
                                                   id="title" name="title"
                                                   placeholder="Tiêu đề bắt buộc phải nhập nội dung">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="content" class="col-12 col-lg-12 col-xl-3 col-form-label">Background:</label>
                                        <div class="col-12 col-lg-12 col-xl-9">
                                            <a id="div_image"
                                               data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=background&lang=vi&akey=@filemanager_get_key()"
                                               class="iframe-btn" data-fancybox data-fancybox data-type="iframe"
                                               href="javascript:;">
                                                @if(isset($config->background) && $config->background != '')
                                                    <img style="width: 200px" id="preview_thumbnail_background" class="img-fluid"
                                                         src="{{ $config->background }}" style="min-height: 45px">
                                                @else
                                                    <img style="width: 200px" id="preview_thumbnail_background" class="img-fluid"
                                                         src="admin/images/upload-thumbnail.png">
                                                @endif
                                                <input type="hidden" name="background" id="background" value="{{isset($config->background) ? $config->background : ''}}"/>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label">Các mục:</label>
                                        <div class="col-md-9">
                                            <div id="div_slider">
                                                @for($i = 0; $i < 3; $i++)
                                                <div class="row m-1">
                                                    <div class="col-md-1">
                                                        <a id="div_image"
                                                           data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=image_{{$i}}&lang=vi&akey=@filemanager_get_key()"
                                                           class="iframe-btn" data-fancybox data-fancybox data-type="iframe"
                                                           href="javascript:;">
                                                            @if(isset($config->slider[$i]->image) && $config->slider[$i]->image != '')
                                                                <img style="width: 200px" id="preview_thumbnail_{{$i}}" class="img-fluid"
                                                                     src="{{ $config->slider[$i]->image }}" style="min-height: 45px">
                                                            @else
                                                                <img style="width: 200px" id="preview_thumbnail_{{$i}}" class="img-fluid"
                                                                     src="admin/images/upload-thumbnail.png">
                                                            @endif
                                                        </a>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <input type="hidden" name="slider[{{$i}}][image]" id="image_{{$i}}" value="{{$config->slider[$i]->image}}"/>
                                                        <input class="form-control" placeholder="Nhập tiêu đề vào đây" name="slider[{{$i}}][title]" id="title_{{$i}}" value="{{isset($config->slider[$i]->title) ? $config->slider[$i]->title : ''}}"/>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input class="form-control" placeholder="Nhập nội dung slider vào đây" name="slider[{{$i}}][content]" id="content_{{$i}}" value="{{isset($config->slider[$i]->content) ? $config->slider[$i]->content : ''}}"/>
                                                    </div>
                                                </div>
                                                @endfor
                                            </div>
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="content" class="col-12 col-lg-12 col-xl-3 col-form-label"></label>
                                        <button type="button" class="btn btn-primary" id="btn_edit"><i
                                                    class="la la-save"></i> Lưu dữ liệu
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- end:: Content -->
    </div>
@endsection
@section('script')
    <!--end::Page Vendors -->
    <script src="assets/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="admin/js/pages/block/config/config-about-block-3.js?v1.4" type="text/javascript"></script>
    <script src="admin/plugins/fancybox/jquery.fancybox.min.js"></script>
    <script src="assets/js/pages/crud/file-upload/dropzonejs.js?v2" type="text/javascript"></script>
    <script src="admin/plugins/fancybox/jquery.observe_field.js"></script>


    <script>

        $("#upload").dropzone({
            url: "/upload-image",
            paramName: "file",
            maxFiles: 1,
            maxFilesize: 5,
            addRemoveLinks: !0,
            sending: function(file, xhr, formData) {
                formData.append("_token", "{{ csrf_token() }}");
            },
            removedfile: function(file)
            {
                var name = $("#image_1").val();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type: 'POST',
                    url: "destroy-image",
                    data: {filename: name},
                    success: function (data){
                        $("#image_1").val('');
                    },
                    error: function(e) {
                        console.log(e);
                    }});
                var fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
            success: function(file, response) {
                if(response.success) {

                } else {
                    alert('Có lỗi xảy ra, vui lòng thử lại sau');
                }
            },
            error: function(file, response) {
                alert('Có lỗi xảy ra, vui lòng thử lại sau');
            }
        });

        $('.iframe-btn').fancybox({
            'iframe': {
                'css': {
                    'width': '90%',
                    'height': '90%',
                }
            },
        });

        $(document).ready(function(){
            $("#image_0").observe_field(1, function () {
                $('#preview_thumbnail_0').attr('src', this.value).show();
            });

            $("#image_1").observe_field(1, function () {
                $('#preview_thumbnail_1').attr('src', this.value).show();
            });

            $("#image_2").observe_field(1, function () {
                $('#preview_thumbnail_2').attr('src', this.value).show();
            });

            $("#background").observe_field(1, function () {
                $('#preview_thumbnail_background').attr('src', this.value).show();
            });
        })

    </script>
@endsection