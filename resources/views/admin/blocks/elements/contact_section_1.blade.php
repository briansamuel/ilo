@extends('admin.index')
@section('page-header', 'Block')
@section('page-sub_header', 'Config Block')
@section('style')
<link rel="stylesheet" href="admin/plugins/fancybox/jquery.fancybox.min.css" />
@endsection
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    {{$blockInfo->name}} </h3>
            </div>
        </div>
    </div>

    <!-- end:: Subheader -->

    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <form id="kt_edit_form">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="kt-portlet">
                        <div class="kt-portlet__head kt-portlet__head--right">
                            <div class="kt-portlet__head-label ">
                                <span class="kt-font-danger"><i class="fa fa-star"></i> Bắt buộc phải nhập / chọn nội dung</span>
                            </div>
                        </div>
                        <!--begin::Form-->
                        <div class="kt-form">
                            <div class="kt-portlet__body">
                                <div class="form-group row">
                                    <label for="title" class="col-12 col-lg-12 col-xl-3 col-form-label">Tiêu
                                        đề:</label>
                                    <div class="col-12 col-lg-12 col-xl-9">
                                        {{csrf_field()}}
                                        <input type="hidden" name="_id" id="_id" value="{{$blockInfo->id}}" />
                                        <input class="form-control" type="text" value="{{isset($config->title) ? $config->title : ''}}" id="title" name="title" placeholder="Tiêu đề bắt buộc phải nhập nội dung">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="sub_title" class="col-12 col-lg-12 col-xl-3 col-form-label">Sub Title:</label>
                                    <div class="col-12 col-lg-12 col-xl-9">
                                        <input class="form-control" type="text" value="{{isset($config->sub_title) ? $config->sub_title : ''}}" id="sub_title" name="sub_title" placeholder="Mô tả bắt buộc phải nhập nội dung">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="content" class="col-12 col-lg-12 col-xl-3 col-form-label">Nội dung:</label>
                                    <div class="col-12 col-lg-12 col-xl-9">
                                        <textarea id="content" name="content" placeholder="Mô tả bắt buộc phải nhập nội dung" class="form-control" cols="30" rows="10">{{isset($config->content) ? $config->content : ''}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="instructor" class="col-12 col-lg-12 col-xl-3 col-form-label">Hướng dẫn:</label>
                                    <div class="col-12 col-lg-12 col-xl-9">
                                        <input class="form-control" type="text" value="{{isset($config->instructor) ? $config->instructor : ''}}" id="instructor" name="instructor" placeholder="Hướng dẫn bắt buộc phải nhập nội dung">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="background" class="col-12 col-lg-12 col-xl-3 col-form-label">Background:</label>
                                    <div class="col-12 col-lg-12 col-xl-9">
                                        <a id="div_image"
                                           data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=background&lang=vi&akey=@filemanager_get_key()"
                                           class="iframe-btn" data-fancybox data-fancybox data-type="iframe"
                                           href="javascript:;">
                                            @if(isset($config->background) && $config->background != '')
                                                <img style="width: 200px" id="preview_thumbnail" class="img-fluid"
                                                     src="{{ $config->background }}" style="min-height: 45px">
                                            @else
                                                <img style="width: 200px" id="preview_thumbnail" class="img-fluid"
                                                     src="admin/images/upload-thumbnail.png">
                                            @endif
                                            <input type="hidden" name="background" id="background" value="{{isset($config->background) ? $config->background : ''}}"/>
                                        </a>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="content" class="col-12 col-lg-12 col-xl-3 col-form-label"></label>
                                    <button type="button" class="btn btn-primary" id="btn_edit"><i class="la la-save"></i> Lưu dữ liệu
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- end:: Content -->
</div>
@endsection
@section('script')
<!--end::Page Vendors -->
<script src="/assets/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js" type="text/javascript"></script>
<script src="/admin/js/pages/block/config/config-contact-block-1.js?v1.4" type="text/javascript"></script>
<script src="/admin/plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="/assets/js/pages/crud/file-upload/dropzonejs.js?v2" type="text/javascript"></script>
<script src="/admin/plugins/fancybox/jquery.observe_field.js"></script>

<script>

    $("#upload").dropzone({
        url: "/upload-image",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: 5,
        addRemoveLinks: !0,
        sending: function(file, xhr, formData) {
            formData.append("_token", "{{ csrf_token() }}");
        },
        removedfile: function(file)
        {
            var name = $("#image_1").val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                },
                type: 'POST',
                url: "destroy-image",
                data: {filename: name},
                success: function (data){
                    $("#image_1").val('');
                },
                error: function(e) {
                    console.log(e);
                }});
            var fileRef;
            return (fileRef = file.previewElement) != null ?
                fileRef.parentNode.removeChild(file.previewElement) : void 0;
        },
        success: function(file, response) {
            if(response.success) {

            } else {
                alert('Có lỗi xảy ra, vui lòng thử lại sau');
            }
        },
        error: function(file, response) {
            alert('Có lỗi xảy ra, vui lòng thử lại sau');
        }
    });

    $('.iframe-btn').fancybox({
        'iframe': {
            'css': {
                'width': '90%',
                'height': '90%',
            }
        },
    });

    $(document).ready(function() {
        $("#background").observe_field(1, function () {
            $('#preview_thumbnail').attr('src', this.value).show();
        });
    });

</script>
@endsection