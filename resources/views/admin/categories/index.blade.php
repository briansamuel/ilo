@extends('admin.index')
@section('page-header', 'Danh mục tin tức')
@section('page-sub_header', 'Thêm Danh Mục')
@section('style')

@endsection
@section('content')
<div class="row">
    @if ($errors->any())
    @foreach ($errors->all() as $error)
    <div class="col-12">
        <div class="alert alert-solid-danger alert-bold" role="alert">
            <div class="alert-text">{{ $error }}</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>
    </div>
    @endforeach
    @endif

    <div class="col-12">
        @include('admin.elements.alert_flash')
    </div>
</div>
<div class="row">

    <div class="col-4">
        <form method="POST" action="{{ route('category.save')}}">
            <div class="kt-portlet" data-ktportlet="true">
                <div class="kt-portlet__body">

                    {{ csrf_field()}}
                    <input type="hidden" id="category_type"  name="category_type" value="{{ $type }}" />
                    <div class="form-group">
                        <label>Tên danh mục:</label>
                        <input type="text" class="form-control" placeholder="Nhập tên danh mục" name="category_name">
                    </div>
                    <div class="form-group">
                        <label>Slug danh mục:</label>
                        <input type="text" class="form-control" placeholder="Nhập slug danh mục" name="category_slug">
                    </div>
                    <div class="form-group">
                        <label>Danh mục cha:</label>
                        <select class="form-control kt-selectpicker" name="category_parent">
                            <option value="0">Để trống</option>
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Mô tả:</label>
                        <textarea type="text" class="form-control" placeholder="" rows="5" name="category_description"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Ngôn ngữ:</label>
                        <select class="form-control kt-selectpicker" name="language">
                            @foreach($arrayLang as $key=>$lang)
                                <option value="{{$key}}">{{$lang}}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
                <div class="kt-portlet__foot kt-align-right p-3">
                    <div>
                        <button type="submit" class="btn btn-primary"><i class="la la-save"></i> Thêm danh mục</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="col-8">
        <div class="kt-portlet" data-ktportlet="true">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        TÌM KIẾM CATEGORY
                    </h3>
                </div>

            </div>
            <form class="kt-form" method="POST">
                <div class="kt-portlet__body">

                    <!--begin: Search Form -->
                    <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                        <div class="row align-items-center">
                            <div class="col-xl-12 order-2 order-xl-1">
                                <div class="row align-items-center">
                                    <div class="col-md-12 kt-margin-b-20-tablet-and-mobile">

                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control" placeholder="Vui lòng nhập từ khóa..." id="category_name">
                                            <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                                <span><i class="la la-search"></i></span>
                                            </span>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>

                    <!--end: Search Form -->

                    <!--begin: Selected Rows Group Action Form -->
                    <div class="kt-form kt-form--label-align-right kt-margin-t-20 collapse" id="kt_datatable_group_action_form1">
                        <div class="row align-items-center">
                            <div class="col-xl-12">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label kt-form__label-no-wrap">
                                        <label class="kt-font-bold kt-font-danger-">Chọn
                                            <span id="kt_datatable_selected_number1">0</span> dòng:</label>
                                        {{csrf_field()}}
                                    </div>
                                    <div class="kt-form__control">
                                        <div class="btn-toolbar">
                                            
                                            <button class="btn btn-sm btn-danger" type="button" id="kt_datatable_delete_all">Xóa Tất Cả</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end: Selected Rows Group Action Form -->
                </div>
                <div class="kt-portlet__body kt-portlet__body--fit">
                    
                    <!--begin: Datatable -->
                    <div class="kt-datatable" id="server_category_selection"></div>

                    <!--end: Datatable -->
                </div>

            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<!--begin::Page Vendors(used by this page) -->


<!--end::Page Vendors -->
<!-- <script src="admin/js/news.js" type="text/javascript"></script> -->
<script src="assets/js/pages/components/portlets/tools.js" type="text/javascript"></script>
<script src="assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
<script src="assets/js/pages/crud/forms/widgets/bootstrap-select.js" type="text/javascript"></script>
<script src="admin/js/pages/news/categories.js" type="text/javascript"></script>
<!-- <script src="assets/js/pages/crud/metronic-datatable/advanced/record-selection.js" type="text/javascript"></script> -->

@endsection