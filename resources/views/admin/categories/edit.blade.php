@extends('admin.index')
@section('page-header', 'Danh mục tin tức')
@section('page-sub_header', 'Thêm Danh Mục')
@section('style')

@endsection
@section('content')
<div class="row">
    @if ($errors->any())
    @foreach ($errors->all() as $error)
    <div class="col-12">
        <div class="alert alert-solid-danger alert-bold" role="alert">
            <div class="alert-text">{{ $error }}</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>
    </div>
    @endforeach
    @endif

    <div class="col-12">
        @include('admin.elements.alert_flash')
    </div>
</div>
<div class="row">

    <div class="col-xl-8 col-sm-12 col-md-12">
        <form method="POST" action="{{ route('category.save')}}">
            <div class="kt-portlet" data-ktportlet="true">
                <div class="kt-portlet__body">

                    {{ csrf_field()}}
                    <input type="hidden" name="id" value="{{ $category->id }}" />
                    <input type="hidden" name="category_type" value="category_of_news" />
                    <div class="form-group">
                        <label>Tên danh mục:</label>
                        <input type="text" class="form-control" placeholder="Nhập tên danh mục" name="category_name" value="{{ $category->category_name }}">
                    </div>
                    <div class="form-group">
                        <label>Slug danh mục:</label>
                        <input type="text" class="form-control" placeholder="Nhập slug danh mục" name="category_slug" value="{{ $category->category_slug }}">
                    </div>
                    <div class="form-group">
                        <label>Danh mục cha:</label>
                        <select class="form-control kt-selectpicker" name="category_parent">
                            <option value="0">Để trống</option>
                            @foreach($categories as $category_i)
                            <option value="{{ $category_i->id }}">{{ $category_i->category_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Mô tả:</label>
                        <textarea type="text" class="form-control" placeholder="" rows="5" name="category_description" >{{ $category->category_description }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Ảnh đại diện:</label>
                        
                    </div>
                    <div class="form-group">
                        <label>Ngôn ngữ:</label>
                        <select class="form-control kt-selectpicker" name="language">
                            @foreach($arrayLang as $key=>$lang)
                                <option value="{{$key}}" {{ $category->language === $key ? 'selected' : '' }}>{{$lang}}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
                <div class="kt-portlet__foot kt-align-right p-3">
                    <div>
                        <button type="submit" class="btn btn-primary"><i class="la la-save"></i> Lưu danh mục</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    
</div>
@endsection
@section('script')
<!--begin::Page Vendors(used by this page) -->


<!--end::Page Vendors -->
<!-- <script src="admin/js/news.js" type="text/javascript"></script> -->
<script src="assets/js/pages/components/portlets/tools.js" type="text/javascript"></script>
<script src="assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
<script src="assets/js/pages/crud/forms/widgets/bootstrap-select.js" type="text/javascript"></script>
<script src="admin/js/pages/news/categories.js" type="text/javascript"></script>
<!-- <script src="assets/js/pages/crud/metronic-datatable/advanced/record-selection.js" type="text/javascript"></script> -->

@endsection