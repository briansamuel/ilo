@extends('admin.index')
@section('page-header', 'Cài Đặt')
@section('page-sub_header', 'Cài đặt thông báo')
@section('style')

@endsection
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Cài Đặt Thông Báo </h3>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-md-12">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">

                        <form class="kt-form" id="kt_edit_form">
                            <div class="kt-portlet__body">
                                <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-3x nav-tabs-bold nav-tabs-line-primary" role="tablist">
                                    @foreach($settings as $group_key => $group)
                                        <li class="nav-item">
                                            <a class="nav-link <?= $group_key === 'contact' ? 'active' : '' ?>" data-toggle="tab" href="#{{$group_key}}" role="tab" aria-selected="false">{{$group['name']}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="tab-content">
                                    {{csrf_field()}}
                                    @foreach($settings as $group_key => $group)
                                    <div class="tab-pane <?= $group_key === 'contact' ? 'active' : '' ?>" id="{{$group_key}}" role="tabpanel">
                                        @foreach($group['data'] as $key => $setting)
                                            <div class="form-group row">
                                                <label class="col-lg-3 col-form-label">{{$setting['name']}}:</label>
                                                <div class="col-lg-6">
                                                    @if(isset($dbSetting['notification'][$group_key]['data'][$key]))
                                                        <?php $setting_data =  $dbSetting['notification'][$group_key]['data'][$key] ?>
                                                        @if($setting['view'] === 'textarea')
                                                            <textarea class="tox-target" rows="5" name="{{$setting['description']}}">{{$setting_data->setting_value}}</textarea>
                                                        @elseif($setting['view'] === 'file')
                                                            <div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="{{$key}}">
                                                                <label></label>
                                                                <div class="kt-avatar__holder" style="background-image: url('{{$setting_data->setting_value}}')"></div>
                                                                <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change logo">
                                                                    <i class="fa fa-pen"></i>
                                                                    <input type="file" name="{{$setting['description']}}" accept="image/*">
                                                                </label>
                                                                <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel logo">
                                                                    <i class="fa fa-times"></i>
                                                                </span>
                                                            </div>
                                                        @elseif($setting['view'] === 'checkbox')
                                                            <span class="kt-switch">
                                                                <label>
                                                                    <input type="checkbox" class="form-control" name="{{$setting['description']}}" <?= $setting_data->setting_value === 'on' ? 'checked' : '' ?>>
                                                                    <span></span>
                                                                </label>
                                                            </span>
                                                        @else
                                                            <input type="{{$setting['view']}}" class="form-control" name="{{$setting['description']}}" value="{{$setting_data->setting_value}}">
                                                        @endif
                                                    @else
                                                        @if($setting['view'] === 'textarea')
                                                            <textarea class="tox-target" rows="5" name="{{$setting['description']}}"></textarea>
                                                        @elseif($setting['view'] === 'file')
                                                            <div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="{{$key}}">
                                                                <label></label>
                                                                <div class="kt-avatar__holder"></div>
                                                                <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change logo">
                                                                    <i class="fa fa-pen"></i>
                                                                    <input type="file" name="{{$setting['description']}}" accept="image/*">
                                                                </label>
                                                                <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel logo">
                                                                    <i class="fa fa-times"></i>
                                                                </span>
                                                            </div>
                                                        @elseif($setting['view'] === 'checkbox')
                                                            <span class="kt-switch">
                                                                <label>
                                                                    <input type="checkbox" class="form-control" name="{{$setting['description']}}">
                                                                    <span></span>
                                                                </label>
                                                            </span>
                                                        @else
                                                            <input type="{{$setting['view']}}" class="form-control" name="{{$setting['description']}}">
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <button type="button" id="btn_edit" class="btn btn-primary">Cập Nhập</button>
                                    <button type="reset" class="btn btn-secondary">Hủy bỏ</button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>

                    <!--end::Portlet-->
                </div>
            </div>
        </div>

        <!-- end:: Content -->
    </div>
@endsection
@section('vendor-script')

    <script src="assets/plugins/custom/tinymce/tinymce.bundle.js" type="text/javascript"></script>
@endsection
@section('script')
    <!--end::Page Vendors -->
    <script src="assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="assets/js/pages/components/portlets/tools.js" type="text/javascript"></script>
    <script src="assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
    <script src="admin/js/pages/settings/setting-notification.js?v1" type="text/javascript"></script>

    <script>
        ! function(t) {
            var e = {};

            function n(i) {
                if (e[i]) return e[i].exports;
                var r = e[i] = {
                    i: i,
                    l: !1,
                    exports: {}
                };
                return t[i].call(r.exports, r, r.exports, n), r.l = !0, r.exports
            }
            n.m = t, n.c = e, n.d = function(t, e, i) {
                n.o(t, e) || Object.defineProperty(t, e, {
                    enumerable: !0,
                    get: i
                })
            }, n.r = function(t) {
                "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
                    value: "Module"
                }), Object.defineProperty(t, "__esModule", {
                    value: !0
                })
            }, n.t = function(t, e) {
                if (1 & e && (t = n(t)), 8 & e) return t;
                if (4 & e && "object" == typeof t && t && t.__esModule) return t;
                var i = Object.create(null);
                if (n.r(i), Object.defineProperty(i, "default", {
                    enumerable: !0,
                    value: t
                }), 2 & e && "string" != typeof t)
                    for (var r in t) n.d(i, r, function(e) {
                        return t[e]
                    }.bind(null, r));
                return i
            }, n.n = function(t) {
                var e = t && t.__esModule ? function() {
                    return t.default
                } : function() {
                    return t
                };
                return n.d(e, "a", e), e
            }, n.o = function(t, e) {
                return Object.prototype.hasOwnProperty.call(t, e)
            }, n.p = "", n(n.s = 677)
        }({
            677: function(t, e, n) {
                "use strict";
                var i = {
                    init: function() {
                        tinymce.init({
                            selector: ".tox-target"
                        })
                    }
                };
                jQuery(document).ready((function() {
                    i.init()
                }))
            }
        });
    </script>
@endsection