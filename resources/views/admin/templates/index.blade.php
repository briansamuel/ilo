@extends('admin.index')
@section('page-header', 'Giao diện')
@section('page-sub_header', 'Chỉnh sửa Template')
@section('style')
    <link href="assets/plugins/codemirror/lib/codemirror.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/codemirror/theme/css/material.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                    Chỉnh sửa Template </h3>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-md-12">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">

                        <form class="kt-form" id="kt_edit_form">
                            <input type="hidden" id="locale-template" value="{{ $language }}">
                            <div class="kt-portlet__body">
                                <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-3x nav-tabs-bold nav-tabs-line-primary" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#bar_service" role="tab" aria-selected="false">Bar Service</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#spa_service" role="tab" aria-selected="false">Spa Service</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#coffee_serrvice" role="tab" aria-selected="false">Coffee - Restaurent Service</a>
                                        </li>
                                       
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#resort_service" role="tab" aria-selected="false">Resort - Hotel Service</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#human_service" role="tab" aria-selected="false">Human Resource Service</a>
                                        </li>
                                </ul>
                                <div class="tab-content">
                                    {{csrf_field()}}
                                        <div class="tab-pane active" id="bar_service" role="tabpanel">
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <textarea id="input_bar_service" name="bar_service">{{$bar_service}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="spa_service" role="tabpanel">
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <textarea id="input_spa_service" name="spa_service">{{$spa_service}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="coffee_serrvice" role="tabpanel">
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <textarea id="input_coffee_service" name="coffee_serrvice">{{$coffee_service}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="resort_service" role="tabpanel">
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <textarea id="input_resort_service" name="resort_service">{{$resort_service}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="human_service" role="tabpanel">
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <textarea id="input_human_service" name="human_service">{{$human_service}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <p class="kt-font-primary">Using Ctrl + Space to auto complete.</p>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <button type="button" id="btn_edit" class="btn btn-primary">Cập Nhập</button>
                                    <button type="reset" class="btn btn-secondary">Hủy bỏ</button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>

                    <!--end::Portlet-->
                </div>
            </div>
        </div>

        <!-- end:: Content -->
    </div>
@endsection

@section('script')
    <!--end::Page Vendors -->   
    <script src="assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
    <script src="assets/plugins/codemirror/lib/codemirror.js" type="text/javascript"></script>
    <script src="assets/plugins/codemirror/lib/javascript.js" type="text/javascript"></script>
    <script src="assets/plugins/codemirror/lib/css.js" type="text/javascript"></script>
    <script src="assets/plugins/codemirror/lib/autorefresh.js" type="text/javascript"></script>
    <script src="assets/plugins/codemirror/addon/hint/show-hint.js" type="text/javascript"></script>
    <script src="assets/plugins/codemirror/addon/hint/css-hint.js" type="text/javascript"></script>
    <link href="assets/plugins/codemirror/addon/hint/show-hint.css" rel="stylesheet" type="text/css" />
    <script src="assets/plugins/codemirror/addon/edit/closebrackets.js" type="text/javascript"></script>
    <script src="admin/js/pages/templates/template.js?v2" type="text/javascript"></script>
    <style>
    .CodeMirror {
    
       
        min-height: 700px;
    }
    </style>
    <!-- END PAGE LEVEL PLUGINS -->
    <script>
        function editor(id)
        {
            return CodeMirror.fromTextArea(document.getElementById(id), {
                lineNumbers: true,
                matchBrackets: !0,
                indentUnit: 4,
                autoCloseBrackets: true,
                styleActiveLine: true,
                tabSize: 5,
                theme: "material",
                mode: "htmlmixed",
                autoRefresh: true,
                extraKeys: {
                    "Ctrl-Space": "autocomplete"
                },
                autohint: true,
                readOnly: false,
                lineWrapping: true,
            });
        }
        var bar = editor('input_bar_service');
        var spa = editor('input_spa_service');
        var resort = editor('input_resort_service')
        var coffee = editor('input_coffee_service')
        var human = editor('input_human_service')
    </script>
@endsection