<header id="header_site" class="fixed-top bottom_to_top">
    <div class="container">
        <div class="position-relative">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href=".">
                    <img src="assets/images/logo.png" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <img src="assets/images/menu.png" alt="">
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul id="iloMenu" class="navbar-nav mr-auto">
                        @if($menus)
                            @foreach($menus as $menu)
                                @php
                                $parrent_active = '';
                                $is_has_child = '';
                                @endphp
                                @if( $menu['child'] ) 
                                @php
                                $is_has_child = 'has-children';
                                
                                foreach( $menu['child'] as $child ) {
                                    if(Request::url().'/' == $child['link']) {
                                        $parrent_active = "active";
                                    }
                                }
                                @endphp
                                @endif
                                <li class="nav-item {{ Request::url() == $menu['link'] ? 'active' : $parrent_active }} {{ $is_has_child }}" >
                                
                                    <a class="nav-link" href="{{ $menu['link'] }}" title="{{ $menu['label'] }}">{{ $menu['label'] }}</a>
                                    @if( $menu['child'] )
                                   
                                    <ul class="sub-menu">
                                        @foreach( $menu['child'] as $child )
                                            
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ $child['link'] }}">{{ $child['label'] }}</a>
                                            </li>
                                        @endforeach
                                        
                                        
                                    </ul>                                 
                                    @endif
                                </li>
                            @endforeach
                        @endif
                        <li class="nav-item d-md-none social">
                            <a href="https://www.facebook.com/iloacademy/" target="_blank">
                                <svg id="fb_icon" data-name="Facebook" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11.6 20.7">
                                    <defs>
                                        <style>
                                            .cls-1 {
                                                fill: #6f6f6f;
                                            }
                                        </style>
                                    </defs>
                                    <title>Facebook</title>
                                    <path class="cls-1" d="M7.2,20.7H3.9a1,1,0,0,1-1-1V12.2H1a1,1,0,0,1-1-1V8A1,1,0,0,1,1,7H2.9V5.4A5.64,5.64,0,0,1,4.3,1.5,5.2,5.2,0,0,1,8.1,0h2.5a1,1,0,0,1,1,1V4a1,1,0,0,1-1,1H8.9a1.73,1.73,0,0,0-.7.1,2.18,2.18,0,0,0-.1.6V7h2.4a.9.9,0,0,1,.5.1,1.05,1.05,0,0,1,.5.9v3.2a1,1,0,0,1-1,1H8.2v7.5A1,1,0,0,1,7.2,20.7ZM4.1,19.5H7V11.7a.68.68,0,0,1,.7-.7h2.7V8.2H7.7A.68.68,0,0,1,7,7.5V5.7a1.79,1.79,0,0,1,.4-1.4A1.94,1.94,0,0,1,9,3.8h1.5V1.2H8.2A3.86,3.86,0,0,0,4.1,5.4V7.5a.68.68,0,0,1-.7.7H1.2V11H3.5a.68.68,0,0,1,.7.7v7.8ZM10.7,1.2Z" />
                                </svg>
                            </a>
                            <a href="https://www.instagram.com/iloacademy/" target="_blank">
                                <svg id="ins_icon" data-name="Instagram" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <defs>
                                        <style>
                                            .cls-1 {
                                                fill: #6f6f6f;
                                            }
                                        </style>
                                    </defs>
                                    <title>Instagram</title>
                                    <path class="cls-1" d="M14.6,0H5.4A5.38,5.38,0,0,0,0,5.4v9.2A5.38,5.38,0,0,0,5.4,20h9.2A5.38,5.38,0,0,0,20,14.6V5.4A5.38,5.38,0,0,0,14.6,0Zm4.3,14.5a4.23,4.23,0,0,1-4.2,4.2H5.5a4.23,4.23,0,0,1-4.2-4.2V5.3A4.23,4.23,0,0,1,5.5,1.1h9.2a4.23,4.23,0,0,1,4.2,4.2Z" />
                                    <path class="cls-1" d="M10,4.5A5.5,5.5,0,1,0,15.5,10,5.48,5.48,0,0,0,10,4.5Zm0,9.8A4.3,4.3,0,1,1,14.3,10,4.33,4.33,0,0,1,10,14.3Z" />
                                    <circle class="cls-1" cx="15.8" cy="4.5" r="1" />
                                </svg>
                            </a>
                            <a href="https://www.youtube.com/channel/UCWizKyF_gKVu7CNh2YPKPfQ" target="_blank">
                                <svg id="yb_icon" data-name="Youtube" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.91 15.2">
                                    <defs>
                                        <style>
                                            .cls-1 {
                                                fill: #6f6f6f;
                                            }
                                        </style>
                                    </defs>
                                    <title>Youtube</title>
                                    <path class="cls-1" d="M2.91,15.2H17a2.9,2.9,0,0,0,2.9-2.9V2.9A2.9,2.9,0,0,0,17,0H2.91A2.9,2.9,0,0,0,0,2.9v9.4A2.84,2.84,0,0,0,2.91,15.2ZM1.11,2.9a1.79,1.79,0,0,1,1.8-1.8H17a1.79,1.79,0,0,1,1.8,1.8v9.4A1.79,1.79,0,0,1,17,14.1H2.91a1.79,1.79,0,0,1-1.8-1.8Z" />
                                    <path class="cls-1" d="M7,3.7v8l7.1-4Zm1.2,2,3.5,1.9-3.5,2Z" />
                                </svg>
                            </a>
                        </li>
                    </ul>
                    <ul class="form-inline navbar-nav my-2 my-lg-0">
                        <li class="nav-item"><a href=""><i class="fa fa-search"></i></a></li>
                        <li class="nav-item"><a href="">{{ __('frontsite.header.show_by') }} <img src="assets/images/kipina.png" alt=""></a></li>
                        <li class="nav-item"><a href="">{{ __('frontsite.header.performed_by') }} <img src="assets/images/ila.png" alt=""></a></li>
                    </ul>
                </div>
            </nav>
            <ul class="menu_mobile d-md-none d-flex align-items-center">
                <li><a href="">{{ __('frontsite.header.show_by') }} <img src="assets/images/kipina.png" alt=""></a></li>
                <li><a href="">{{ __('frontsite.header.performed_by') }} <img src="assets/images/ila.png" alt=""></a></li>
                <li><a href=""><img src="assets/images/search.png" alt=""></a></li>
            </ul>
        </div>
    </div>
</header>