@extends('frontsite.index')
@section('title', __('seo.about.title'))
@section('title_ab', __('seo.about.title'))
@section('keyword', __('seo.about.keyword'))
@section('description', __('seo.about.description'))
@section('image', __('seo.about.image'))
@section('style')

@endsection
@section('content')
<div id="fullpage">
    @include('frontsite.about.elements.section_first')
    @include('frontsite.about.elements.section_second')
    @include('frontsite.about.elements.section_third')
    @include('frontsite.elements.contact')
</div>
@endsection
@section('script')

@endsection