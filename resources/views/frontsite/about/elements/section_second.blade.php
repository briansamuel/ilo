@if(isset($section2))
<div class="section">
    <div class="img_bg">
        <img src="{{isset($section2->background) ? $section2->background : ''}}" alt="">
    </div>
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-md-6 order-md-1">
                <div class="content_speacial mb-md-4 mb-3 left_to_right">
                    {{isset($section2->description) ? $section2->description : ''}}
                </div>
                <div class="more_content right_to_left">
                    {{isset($section2->content) ? $section2->content : ''}}
                </div>
            </div>
            <div class="col-md-5 order-md-0">
                <div class="img_wrapper bottom_to_top">
                    <img src="{{isset($section2->image) ? $section2->image : ''}}" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
@endif