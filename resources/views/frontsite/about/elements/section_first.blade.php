@if(isset($section1))
<div id="section_first" class="section bg_green">
    <div class="content_middle">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="title mb-4 left_to_right">
                        <strong>ILO Academy</strong><br>
                        {{isset($section1->title) ? $section1->title : ''}}
                    </div>
                    <div class="description right_to_left">
                        {{isset($section1->content) ? $section1->content : ''}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="img_fullheight right_to_left">
            <img src="{{isset($section1->image) ? $section1->image : ''}}" class="img-fluid" alt="">
        </div>
    </div>
</div>
@endif