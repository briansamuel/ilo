@if(isset($section3))
<div class="section">
    <div class="img_bg">
        <img src="{{isset($section3->background) ? $section3->background : ''}}" alt="">
    </div>
    <div class="content_bottom">
        <div class="container">
            <div class="title mb-5 left_to_right">
                <strong>ILO Academy</strong> <br>
                {!! isset($section3->title) ? $section3->title : '' !!}
            </div>
            <div class="row">
                @if(isset($section3->slider))
                    @foreach($section3->slider as $slider)
                    <div class="col-md-4">
                        <div class="item_port bottom_to_top">
                            <div class="icon_port">
                                <img src="{{isset($slider->image) ? $slider->image : ''}}" class="img-fluid" alt="">
                            </div>
                            <div class="port_title">
                                {{isset($slider->title) ? $slider->title : ''}}
                            </div>
                            <div class="content">
                                {{isset($slider->content) ? $slider->content : ''}}
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
@endif