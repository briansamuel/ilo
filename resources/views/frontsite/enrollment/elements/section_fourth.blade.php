<div id="last_section" class="section">
    <div class="container">
        <div class="row align-items-end">
            <div class="col-md-6">
                <div class="title mb-4 right_to_left">
                    Ghé thăm ILO Academy nhé!
                </div>
                <div class="description mb-4 left_to_right">
                    “Vui lòng liên hệ khi bạn cần thêm thông tin chi tiết.”
                </div>
                <div class="hotline mb-4 right_to_left">
                    <strong>Hotline:</strong> 0707 518 655
                </div>
                <div class="email mb-4 right_to_left">
                    <strong>Email:</strong> ilo@academy.com
                </div>
                <div class="form_contact right_to_left">
                    <div class="title_contact">
                        Đặt lịch tham quan:
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="Tên *">
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="Email *">
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="Số điện thoại *">
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="datepicker" class="form-control" placeholder="Ngày">
                        </div>
                        <div class="col-md-3 col-4">
                            <img src="assets/images/captcha.jpg" class="img-fluid" alt="">
                        </div>
                        <div class="col-md-9 col-8">
                            <input type="text" placeholder="Nhập mã *" class="form-control">
                        </div>
                    </div>
                    <div class="wrapper_submit">
                        <button type="submit" class="submit_button">Xác nhận</button>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="map_img right_to_left">
                    <img src="assets/images/map.png" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="copyright ff-italic">
        Copyright &copy; 2020. Website by <a href="https://commamedia.vn" target="_blank">Comma.</a>
    </div>
</div>