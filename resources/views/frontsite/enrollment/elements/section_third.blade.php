@if(isset($section3))
<div id="offer_section" class="section">
    <div class="content_bottom">
        <div class="container">
            <div class="title mb-4 right_to_left">
                {{ isset($section3->title) ? $section3->title : '' }}
            </div>
            <div class="description mb-4 left_to_right">
                {{ isset($section3->content) ? $section3->content : '' }}
            </div>
            <div class="slide_offer bottom_to_top">
                @if(isset($section3->slider))
                @foreach($section3->slider as $key => $banner)
                <div class="item">
                    <img src="{{ isset($banner->image) ? $banner->image : '' }}" class="img-fluid" alt="">
                    <div class="meta_info">
                        <h3 class="title">
                            <a href="">{{ isset($banner->title) ? $banner->title : '' }}</a>
                        </h3>
                        <div class="description_post">
                            {{ isset($banner->content) ? $banner->content : '' }}
                        </div>
                        <a href="{{ isset($banner->link) ? $banner->link : '' }}" class="see_more">{{ __('frontsite.enrollment.load_more')}}</a>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
@endif