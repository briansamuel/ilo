@if($section1)
<div id="section_first" class="section">
    <div class="img_bg opt-50">
        <img src="assets/images/banner-erollment.jpg" class="img-fluid" alt="">
    </div>
    <div class="content_top">
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-md-5">
                    <div class="title mb-4 left_to_right show">
                        <strong>
                            {{ $section1->title }}
                        </strong> <br>
                        {{ $section1->sub_title }}
                    </div>
                    <div class="mb-4 right_to_left show">
                        <ul class="list_step">
                            @if(isset($section1->step))
                            @foreach($section1->step as $key => $step)
                            <li>
                                <div class="step">
                                    Step <span>0{{ $key+1  }}</span>
                                </div>
                                <div class="step_info">
                                    <div class="title_step">{{ $step->title }}</div>
                                    <div class="step_desc">{{ $step->content }}</div>
                                </div>
                            </li>
                            @endforeach
                            @endif
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif