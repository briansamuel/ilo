@if(isset($section2))
<div class="section">
    <div class="img_bg opt-50">
        <img src="assets/images/bg-erollment.jpg" class="img-fluid" alt="">
    </div>
    <div class="content_top">
        <div class="container">
            <div class="title mb-4 right_to_left">
                <strong>ILO Academy</strong><br>
                Học phí
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="nav flex-column left_to_right" id="v-enroll-tab" role="tablist" aria-orientation="vertical">
                        @if(isset($section2->tab))
                        @foreach($section2->tab as $key => $tab)
                        <a class="nav-link {{ $key == 0 ? 'active' : ''}}" id="v-tab-{{ $key }}" data-toggle="pill" href="#v-{{ $key }}" role="tab" aria-controls="v-enroll-tuition" aria-selected="true">{{ $tab }}</a>
                        @endforeach
                        @endif
                        
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="tab-content bottom_to_top" id="v-enroll-tabContent">
                        <div class="tab-pane fade show active" id="v-0" role="tabpanel" aria-labelledby="v-enroll-tuition-tab">
                            <div class="table-responsive">
                                @if(isset($section2->fee))
                                <table class="table table-bordered">
                                    
                                    <thead>

                                        <tr>
                                            @if(isset($section2->fee[0]))
                                            @foreach($section2->fee[0] as $key => $fee)
                                            <th scope="col">{{ $fee }}</th>
                                            @endforeach
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($section2->fee as $i => $row)
                                        @if($i > 0)
                                        <tr>
                                            @foreach($row as $j => $col)
                                            <th {{ $j === 0 ? 'scope="row"' : ''}}>{{ $col }}</th>
                                            @endforeach

                                        </tr>
                                        @endif
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                                @endif
                            </div>
                            {!! isset($section2->note) ? $section2->note : '' !!}
                        </div>
                        <div class="tab-pane fade" id="v-1" role="tabpanel" aria-labelledby="v-enroll-promotion-tab">
                            <div class="content_promotion">
                                {!! isset($section2->fee_service_other) ? $section2->fee_service_other : '' !!}
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-2" role="tabpanel" aria-labelledby="v-enroll-services-tab">
                            <div class="content_promotion">
                                {!! isset($section2->incentives) ? $section2->incentives : '' !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif