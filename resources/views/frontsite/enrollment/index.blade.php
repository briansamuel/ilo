@extends('frontsite.index')
@section('title', __('seo.enrollment.title'))
@section('title_ab', __('seo.enrollment.title'))
@section('keyword', __('seo.enrollment.keyword'))
@section('description', __('seo.enrollment.description'))
@section('image', __('seo.enrollment.image'))
@section('style')

@endsection
@section('content')
<div id="fullpage">
    @include('frontsite.enrollment.elements.section_first')
    
    @include('frontsite.enrollment.elements.section_second')
    
    @include('frontsite.enrollment.elements.section_third')
    
    @include('frontsite.enrollment.elements.section_fourth')
</div>
@endsection
@section('script')

@endsection