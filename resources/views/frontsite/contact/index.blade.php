@extends('frontsite.index')
@section('title', __('seo.contact.title'))
@section('title_ab', __('seo.contact.title'))
@section('keyword', __('seo.contact.keyword'))
@section('description', __('seo.contact.description'))
@section('image', __('seo.contact.image'))
@section('style')

@endsection
@section('content')
<div id="fullpage">
    @include('frontsite.elements.contact_advisory')
    @include('frontsite.contact.elements.section_second')
    @include('frontsite.contact.elements.section_third')
    @include('frontsite.elements.contact')
</div>
@endsection