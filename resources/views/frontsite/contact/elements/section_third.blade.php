<div id="jobs_section" class="section">
    <div class="container">
        <div class="title mb-4 left_to_right">
            <strong>{{ isset($section3->title) ? $section3->title : ''}}</strong><br>
            {{ isset($section3->subtitle) ? $section3->subtitle : ''}}
        </div>
        <div class="description right_to_left">
            {!! isset($section3->content) ? $section3->content : '' !!}
        </div>
        <div class="info_joinus mt-5">
            <div class="row">
                <div class="col-md-6">
                    <table class="table">
                        <tbody>
                            @if(isset($section3->row))
                            @foreach($section3->row as $key => $row)
                            <tr>
                                <td>
                                    <h3>{{ $row->title }}</h3>
                                    <p><strong>Amount:</strong> {{ $row->amount }}</p>
                                </td>
                                <td>
                                    <p><strong>Job description</strong></p>
                                </td>
                                <td>
                                    <p><a class="download" download href="{{ $row->file }}">Download file</a></p>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                            
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table">
                        <tbody>
                            @if(isset($section3->row))
                                @foreach($section3->row as $key => $row)
                                <tr>
                                    <td>
                                        <h3>{{ $row->title }}</h3>
                                        <p><strong>Amount:</strong> {{ $row->amount }}</p>
                                    </td>
                                    <td>
                                        <p><strong>Job description</strong></p>
                                    </td>
                                    <td>
                                        <p><a class="download" download href="{{ $row->file }}">Download file</a></p>
                                    </td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <p>If you feel the position fitness where we are recruiting, please send your CV and porfolio to: <a href=""><strong>touch@commamedia.vn</strong></a></p>
    </div>
</div>