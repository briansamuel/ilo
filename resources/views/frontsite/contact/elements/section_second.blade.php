@if(isset($section2))
<div class="section bg-green">
    <div class="my-4 my-md-0">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-5">
                    <div class="title mb-4 right_to_left">
                        <strong{{isset($section2->title) ? $section2->title : ''}}></strong> <br>
                        {{isset($section2->sub_title) ? $section2->sub_title : ''}}
                    </div>
                    <select name="" class="form-control" id="">
                        <option value="TPHCM">Hồ Chí Minh</option>
                        <option value="HN">Hà Nội</option>
                    </select>
                    <div class="area_location my-3 my-md-0">
                        @foreach($section2->place as $key => $place)
                        <div class="item_location">
                            <div class="map_title">
                                <h4>{{isset($place->name) ? $place->name : ''}}</h4>
                            </div>
                            <div class="map_address">
                                <p>{{isset($place->address) ? $place->address : ''}}</p>
                            </div>
                            <div class="phone">
                                <p><strong>Hotline:</strong> {{isset($place->hotline) ? $place->hotline : ''}}</p>
                            </div>
                            <div class="email">
                                <p><strong>Email:</strong> {{isset($place->email) ? $place->email : ''}}</p>
                            </div>
                            <a href="javascript:;" data-centermap="{{isset($place->lat) ? $place->lat : ''}}~{{isset($place->long) ? $place->long : ''}}~{{$key}}" class="see_more">Xem bản đồ</a>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-7">
                    <div id="map">
                        <ul>
                            @foreach($section2->place as $place)
                            <li>
                                <div class="map_title">
                                    <h4>{{isset($place->name) ? $place->name : ''}}</h4>
                                </div>
                                <div class="map_address">
                                    <p>{{isset($place->address) ? $place->address : ''}}</p>
                                </div>
                                <div class="latitude">{{isset($place->lat) ? $place->lat : ''}}</div>
                                <div class="longitude">{{isset($place->long) ? $place->long : ''}}</div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif