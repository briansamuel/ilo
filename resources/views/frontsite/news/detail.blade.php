@extends('frontsite.index')
@section('title', $news->post_title)
@section('title_ab', $news->post_title)
@section('keyword', $news->post_seo_keyword ? $news->post_seo_keyword : $news->post_title)
@section('description', $news->post_seo_description ? $news->post_seo_description : $news->post_title)
@section('image', $news->post_thumbnail ? $news->post_thumbnail : __('seo.news.image'))
@section('style')

@endsection
@section('content')
<div id="singlepage">
    <div class="content_post">
        <div class="container">
            <div class="img_featured">
                <img src="{{ $news->post_thumbnail }}" class="img-fluid w-100" alt="">
            </div>
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="date_post text-center">
                        {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $news->created_at)->format('d/m/Y') }}
                    </div>
                    <h1>{{ $news->post_title }}</h1>

                    {!! $news->post_content !!}
                </div>
            </div>

        </div>
    </div>
    <div class="post_related">
        <div class="img_bg opt-50">
            <img src="assets/images/bgnews.jpg" class="img-fluid" alt="">
        </div>
        <div class="content_bottom">
            <div class="container">
                <div class="title text-center bottom_to_top mb-4">
                    {{ __('frontsite.news.related_news')}}
                </div>
                <div class="slide_offer bottom_to_top">
                    @foreach($related_news as $news_item)
                    <div class="item">
                        <figure>
                            <img src="{{ str_replace('uploads/files/tin-tuc/', 'slideImage/', $news_item->post_thumbnail) }}" class="img-fluid" alt="">
                            <div class="tag {{ $news_item->post_type }}">{{ __('frontsite.news.type_'.$news_item->post_type) }}</div>
                        </figure>
                        <div class="meta_info">
                            <div class="date_post">
                                {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $news_item->created_at)->format('d/m/Y') }}
                            </div>
                            <h3 class="title">
                                <a href="{{ $news_item->post_slug }}.html">Lorem ipsum dolor sit amet, consectetur onsectetur</a>
                            </h3>
                            <div class="description_post">
                                {{ Str::limit(html_entity_decode(strip_tags($news_item->post_description)), 100) }}...
                            </div>
                            <a href="{{ $news_item->post_slug }}.html" class="see_more">{{ __('frontsite.news.load_more') }}</a>
                        </div>
                    </div>
                    @endforeach
                    
                </div>
            </div>
        </div>

    </div>
    @include('frontsite.elements.contact')
</div>
@endsection
@section('script')

@endsection