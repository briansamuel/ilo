@extends('frontsite.index')
@section('title', __('seo.news.title'))
@section('title_ab', __('seo.news.title'))
@section('keyword', __('seo.news.keyword'))
@section('description', __('seo.news.description'))
@section('image', __('seo.news.image'))
@section('style')

@endsection
@section('content')
<div id="fullpage">
    <div id="section_first" class="section">
        <div class="content_top">
            <div class="container">
                <div class="title text-center bottom_to_top mb-4">
                {{ __('frontsite.news.new_feature') }}
                </div>
                <div class="row">
                    <div class="col-md-6">
                        @foreach($feature_news as $news_item)
                        <div class="item_post_featured">
                            <figure>
                                <img src="{{ $news_item->post_thumbnail }}" class="img-fluid" alt="">
                                <div class="tag {{ $news_item->post_type }}">{{ __('frontsite.news.type_'.$news_item->post_type) }}</div>
                            </figure>
                            <div class="meta_info">
                                <div class="date_post">
                                {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $news_item->created_at)->format('d/m/Y') }}
                                </div>
                                <h3 class="title">
                                    <a href="{{ $news_item->post_slug }}.html">{{ $news_item->post_title }}</a>
                                </h3>
                                <div class="description_post">
                                {{ Str::limit(html_entity_decode(strip_tags($news_item->post_description)), 240) }}...
                                </div>
                                <a href="{{ $news_item->post_slug }}.html" class="see_more">{{ __('frontsite.news.load_more') }}</a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="col-md-6">
                        @foreach($news as $news_item)
                        <div class="item_post">
                            <div class="row">
                                <div class="col-md-4">
                                    <figure>
                                        <img src="{{ $news_item->post_thumbnail }}" class="img-fluid" alt="">
                                        <div class="tag {{ $news_item->post_type }}">{{ __('frontsite.news.type_'.$news_item->post_type) }}</div>
                                    </figure>
                                </div>
                                <div class="col-md-8">
                                    <div class="meta_info">
                                        <div class="date_post">
                                        {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $news_item->created_at)->format('d/m/Y') }}
                                        </div>
                                        <h3 class="title">
                                            <a href="{{ $news_item->post_slug }}.html">{{   $news_item->post_title }}</a>
                                        </h3>
                                        <div class="description_post">
                                        {{ Str::limit(html_entity_decode(strip_tags($news_item->post_description)), 100) }}...
                                        </div>
                                        <a href="{{ $news_item->post_slug }}.html" class="ff-heavy">{{ __('frontsite.news.load_more') }}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="img_bg opt-50">
            <img src="assets/images/bgnews.jpg" class="img-fluid" alt="">
        </div>
        <div class="content_bottom">
            <div class="container">
                <div class="title text-center bottom_to_top mb-4">
                {{ __('frontsite.news.new_and_event') }}
                </div>
                <ul class="category_menu list-inline text-center">
                    <li class="list-inline-item active"><a href="">{{ __('frontsite.news.all') }}</a></li>
                    <li class="list-inline-item"><a href="">Tin tức của ILO</a></li>
                    <li class="list-inline-item"><a href="">Dành cho cha mẹ</a></li>
                    <li class="list-inline-item"><a href="">Kỹ năng ngoại khoá</a></li>
                    <li class="list-inline-item"><a href="">{{ __('frontsite.news.type_event') }}</a></li>
                </ul>
                <div class="slide_offer bottom_to_top">
                    @foreach($slide_news as $news_item)
                    <div class="item">
                        <figure>
                            <img src="{{ str_replace('uploads/files/tin-tuc/', 'slideImage/', $news_item->post_thumbnail) }}" class="img-fluid" alt="">
                            <div class="tag {{ $news_item->post_type }}">{{ __('frontsite.news.type_'.$news_item->post_type) }}</div>
                        </figure>
                        <div class="meta_info">
                            <div class="date_post">
                                {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $news_item->created_at)->format('d/m/Y') }}
                            </div>
                            <h3 class="title">
                                <a href="{{ $news_item->post_slug }}.html">Lorem ipsum dolor sit amet, consectetur onsectetur</a>
                            </h3>
                            <div class="description_post">
                                {{ Str::limit(html_entity_decode(strip_tags($news_item->post_description)), 100) }}...
                            </div>
                            <a href="{{ $news_item->post_slug }}.html" class="see_more">{{ __('frontsite.news.load_more') }}</a>
                        </div>
                    </div>
                    @endforeach
                    
                </div>
            </div>
        </div>
    </div>

    <div id="gallery-section" class="section">
        <div class="content_bottom">
            <div class="container">
                <div class="title text-center bottom_to_top mb-4">
                {{ $section3->title }}
                </div>
                <ul class="category_menu list-inline text-center">
                    <li class="list-inline-item active"><a href="">{{ __('frontsite.news.all') }}</a></li>
                    <li class="list-inline-item"><a href="">Học viên</a></li>
                    <li class="list-inline-item"><a href="">Cơ sở vật chất</a></li>
                    <li class="list-inline-item"><a href="">Chương trình ngoại khóa</a></li>
                </ul>
                <div class="slide_gallery bottom_to_top">
                    @if(isset($section3->slider))
                    @foreach($section3->slider as $key => $gallery)
                    <div class="item">
                        <figure>
                            <img src="{{ $gallery->image ? $gallery->image : '' }}" class="img-fluid" alt="">
                        </figure>
                        <div class="meta_info">
                            <h3 class="title">
                                {{ $gallery->title ? $gallery->title : '' }}
                            </h3>
                            <a href="{{ $gallery->link ? $gallery->link : '#' }}" class="see_more" data-fancybox="gallery">{{ __('frontsite.news.load_more') }}</a>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('frontsite.elements.contact')
</div>
@endsection
@section('script')

@endsection