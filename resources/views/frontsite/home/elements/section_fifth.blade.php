@if(isset($section5))
<div id="offer_section" class="section">
    <div class="content_bottom">
        <div class="container">
            <div class="title mb-4 right_to_left">
                {{isset($section5->title) ? $section5->title : ''}}
            </div>
            <div class="description mb-4 left_to_right">
                {{isset($section5->content) ? $section5->content : ''}}
            </div>
            <div class="slide_offer bottom_to_top">
                @if(isset($section5->slider))
                    @foreach($section5->slider as $key => $slider)
                        <div class="item">
                            <img src="{{$slider->image}}" class="img-fluid" alt="">
                            <div class="meta_info">
                                <h3 class="title">
                                    {{$key + 1}}. {{isset($slider->title) ? $slider->title : ''}}
                                </h3>
                                <div class="description_post">
                                    {{isset($slider->content) ? $slider->content : ''}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
@endif