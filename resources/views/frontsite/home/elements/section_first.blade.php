@if(isset($banners))
<div id="section_first" class="section">
    <div class="slider_banner">
        @foreach($banners as $banner)
            <div class="item">
                <img src="{{isset($banner->image) ? $banner->image : ''}}" class="img-fluid" alt="">
                <div class="meta_info">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="title bottom_to_top mb-4">
                                    <strong>{{ __('frontsite.home.preschool') }}</strong><br>
                                    <strong>{{ __('frontsite.home.bilingual') }}</strong><br>
                                    <strong>ILO Academy</strong><br>
                                    {{isset($banner->title) ? $banner->title : ''}}
                                </div>
                                <div class="description right_to_left">
                                    {{isset($banner->description) ? $banner->description : ''}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="nav_slider">
        <div class="prev_slider">
            <div class="arrow">
                <img src="assets/images/left.png" alt="">
            </div>
        </div>
        <div class="next_slider">
            <div class="arrow">
                <img src="assets/images/right.png" alt="">
            </div>
        </div>
    </div>
</div>
@endif