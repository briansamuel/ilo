@if(isset($section2))
<div id="section_second" class="section">
    <div class="content_abs">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="title mb-4 left_to_right">
                        {{isset($section2->title) ? $section2->title : ''}}
                    </div>
                    <div class="description mb-4 left_to_right">
                        “{{isset($section2->description) ? $section2->description : ''}}”
                    </div>
                    <div class="content left_to_right">
                        {{isset($section2->content) ? $section2->content : ''}}
                    </div>
                    <a href="{{isset($section2->link) ? $section2->link : ''}}" class="see_more right_to_left">Xem thêm</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row_custom">
        <div class="col_md_50">
        </div>
        <div class="col_md_50">
            <div class="slider_about right_to_left">
                @foreach($section2->slider as $key=>$slider)
                    @if($key % 2 === 0)
                    <div class="item">
                        <div class="row_custom">
                            <div class="col_50">
                                <div class="img-60">
                                    <img src="{{isset($slider->image) ? $slider->image : ''}}" class="img-fluid" alt="">
                                </div>
                                <div class="content-40 bg-pink">
                                    <div class="title">
                                        {{isset($slider->content) ? $slider->content : ''}}
                                    </div>
                                    <div class="content">
                                        &nbsp;
                                    </div>
                                    <div class="number">
                                        {{str_pad($key + 1, 2, "0", STR_PAD_LEFT)}}
                                    </div>
                                </div>
                            </div>
                    @else
                            <div class="col_50">
                                <div class="content-40 bg-green">
                                    <div class="title">
                                        {{isset($slider->content) ? $slider->content : ''}}
                                    </div>
                                    <div class="content">
                                        &nbsp;
                                    </div>
                                    <div class="number">
                                        {{str_pad($key + 1, 2, "0", STR_PAD_LEFT)}}
                                    </div>
                                </div>
                                <div class="img-60">
                                    <img src="{{isset($slider->image) ? $slider->image : ''}}" class="img-fluid" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
            <div class="nav_about">
                <div class="prev_slider">
                    <div class="arrow">
                        <img src="assets/images/left.png" alt="">
                    </div>
                </div>
                <div class="next_slider">
                    <div class="arrow">
                        <img src="assets/images/right.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif