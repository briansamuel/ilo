@if(isset($section4))
<div class="section">
    <div class="content_bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="title mb-4 left_to_right">
                        {{isset($section4->title) ? $section4->title : ''}}
                    </div>
                    <div class="description mb-4 right_to_left">
                        “{{isset($section4->content) ? $section4->content : ''}}”
                    </div>
                </div>
            </div>
            <div class="gallery_wrapper">
                @foreach($section4->gallery as $key=>$image)
                <div class="item_{{$key}} {{$section4->position_image[$key]}}">
                    <img src="{{$image}}" class="img-fluid" alt="">
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif