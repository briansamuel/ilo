@if(isset($section3))
<div class="section">
    <div class="img_bg">
        <img src="{{isset($section3->background) ? $section3->background : ''}}" class="img-fluid" alt="">
    </div>
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-md-6">
                <div class="slider_value bottom_to_top">
                    @foreach($section3->slider as $slider)
                    <div class="item">
                        <div class="title mb-md-4 mb-2">
                            {{$slider->title}}
                        </div>
                        <div class="description mb-4">
                            “{{$slider->content}}”
                        </div>
                        <div class="content">

                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endif