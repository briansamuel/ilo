@extends('frontsite.index')
@section('title', __('seo.home.title'))
@section('title_ab', __('seo.home.title'))
@section('keyword', __('seo.home.keyword'))
@section('description', __('seo.home.description'))
@section('style')

@endsection
@section('content')
<div id="fullpage">
    @include('frontsite.home.elements.section_first')
    @include('frontsite.home.elements.section_second')
    @include('frontsite.home.elements.section_third')
    @include('frontsite.home.elements.section_fourth')
    @include('frontsite.home.elements.section_fifth')
    @include('frontsite.elements.contact')
</div>
@endsection
@section('script')
@endsection