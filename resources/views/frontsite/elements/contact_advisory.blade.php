@if(isset($section1))
<div id="section_first" class="section">
    <div class="img_bg opt-50">
        <img src="{{isset($section1->background) ? $section1->background : ''}}" class="img-fluid" alt="">
    </div>
    <div class="content_middle">
        <div class="container">
            <div class="row justify-content-end advisory">
                <div class="col-md-7">
                    <div class="title mb-4 left_to_right show">
                        <strong>
                        {{ isset($section1->title) ? $section1->title : '' }}
                        </strong> <br>
                        {{ isset($section1->sub_title) ? $section1->sub_title : '' }}
                    </div>
                    <p>{{ isset($section1->content) ? $section1->content : '' }}</p>
                    <p><strong>{{ isset($section1->instructor) ? $section1->instructor : '' }}</strong></p>
                    <div class="row">
                        <div class="col-md-6">
                            <input name="name" id="name" type="text" class="form-control" placeholder="{{ __('frontsite.elements.contact.name') }}">
                        </div>
                        <div class="col-md-6">
                            <input name="email" id="email" type="email" class="form-control" placeholder="{{ __('frontsite.elements.contact.email') }}">
                        </div>
                        <div class="col-md-6">
                            <input name="phone_number" id="phone_number" type="text" class="form-control" placeholder="{{ __('frontsite.elements.contact.phone') }}">
                        </div>
                        <div class="col-md-6">
                            <input name="content" id="content" type="text" class="form-control" placeholder="{{ __('frontsite.elements.contact_advisory.advisory_about') }}">
                        </div>
                        <div class="col-md-3 col-4" id="div_captcha_advisory">
                            {!! captcha_img('inverse') !!}
                        </div>
                        <div class="col-md-9 col-8">
                            <input type="text" name="captcha" id="captcha" placeholder="{{ __('frontsite.elements.contact.type_captcha') }}" class="form-control">
                        </div>
                    </div>
                    <div class="form-group" style="display:none" id="show_error">
                        <div>
                            <span class="text text-danger" id="content_error"></span>
                        </div>
                    </div>
                    <div class="form-group" style="display:none" id="show_success">
                        <div>
                            <span class="text text-success" id="content_success"></span>
                        </div>
                    </div>
                    <div class="wrapper_submit">
                        <button type="submit" onclick="sendContactAdvisory()" class="submit_button">{{ __('frontsite.elements.contact.register_now') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    function checkSendContactAdvisory(){
        var email = $(".advisory #email").val();
        var name = $(".advisory #name").val();
        var phone_number = $(".advisory #phone_number").val();
        var content = $(".advisory #content").val();
        var captcha = $(".advisory #captcha").val();
        if(name === ""){
            $(".advisory #show_error").show();
            $(".advisory #content_error").text("Bạn chưa nhập họ tên !");
            $(".advisory #name").focus();
            return false;
        }
        if(captcha === ""){
            $(".advisory #show_error").show();
            $(".advisory #content_error").text("Bạn chưa nhập captcha !");
            $(".advisory #captcha").focus();
            return false;
        }
        if(email === ""){
            $(".advisory #show_error").show();
            $(".advisory #content_error").text("Bạn chưa nhập Email !");
            $(".advisory #email").focus();
            return false;
        }
        if(!validateEmail(email)){
            $(".advisory #show_error").show();
            $(".advisory #content_error").text("Email không đúng định dạng !");
            $(".advisory #email").focus();
            return false;
        }
        if(phone_number === ""){
            $(".advisory #show_error").show();
            $(".advisory #content_error").text("Bạn chưa nhập số điện thoại !");
            $(".advisory #phone_number").focus();
            return false;
        }
        if(content === ""){
            $(".advisory #show_error").show();
            $(".advisory #content_error").text("Bạn chưa nhập nội dung cần tư vấn !");
            $(".advisory #content").focus();
            return false;
        }

        return true;
    }

    function sendContactAdvisory(){
        if(checkSendContactAdvisory()){
            //display notice
            $(".advisory #btn_contact").text("Đang xử lý....");
            $(".advisory #btn_contact").removeAttr("onclick");

            $.post('/send-contact', {
                email: $(".advisory #email").val(),
                phone_number: $(".advisory #phone_number").val(),
                date_picker: $(".advisory #date_picker").val(),
                content: $(".advisory #content").val(),
                captcha: $(".advisory #captcha").val(),
                type: 'advisory',
                name: $(".advisory #name").val(),
                _token: "{{csrf_token()}}"
            }, function(res) {
                $(".advisory #btn_contact").text("Xác nhận");
                $(".advisory #btn_contact").attr("onclick", "return sendContact();");
                if (res.success) {
                    $(".advisory #show_error").hide();
                    $(".advisory #show_success").show();
                    $(".advisory #content_success").text(res.message);
                    clear_form_advisory();
                    return false;
                } else {
                    $(".advisory #show_success").hide();
                    $(".advisory #show_error").show();
                    $(".advisory #content_error").text(res.error.message);
                    reset_captcha_advisory();
                    return false;
                }
            }).fail(function(res) {
                $(".advisory #show_success").hide();
                $(".advisory #btn_contact").text("Xác nhận");
                $(".advisory #btn_contact").attr("onclick", "return sendContact();");
                $(".advisory #show_error").hide();
                $(".advisory #content_error").text("");
                alert('Hệ thống gặp lỗi, vui lòng thử lại sau.');
                return false;
            });
        }
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function reset_captcha_advisory(){
        $.ajax({
            type:'GET',
            url:'refresh-captcha',
            success:function(data){
                $(".advisory #div_captcha_advisory").html(data.captcha);
            }
        });
    }

    function clear_form_advisory() {
        $(".advisory #email").val('');
        $(".advisory #name").val('');
        $(".advisory #date_picker").val('');
        $(".advisory #phone_number").val('');
        $(".advisory #captcha").val('');
    }

</script>
@endif