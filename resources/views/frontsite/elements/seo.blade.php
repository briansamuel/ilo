
<meta http-equiv="content-language" content="{{ App::getLocale() }}">
<meta name="og:title" content="@yield('title', __('seo.home.title'))"" />
<meta name="og:type" content="@yield('type', 'article')" />
<meta name="og:url" content="{{ Request::url() }}" />
<meta name="og:image" content="@yield('image', 'assets/images/logo.png')" />
<meta name="og:site_name" content="__('seo.site_name'))"/>
<meta name="og:description" content="@yield('description', __('seo.home.description'))">
<meta name="og:keyword" content="@yield('keyword', __('seo.home.keyword'))">
<meta name="og:locale" content="{{ App::getLocale() }}">