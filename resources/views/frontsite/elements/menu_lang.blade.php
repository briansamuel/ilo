<div id="menu_lang">
    <ul>
        <li class="{{ App::getLocale() == 'en' ? 'active' : ''}}"><a href="{{ route('lang', ['locale' => 'en']) }}">ENG</a></li>
        <li class="{{ App::getLocale() == 'vi' ? 'active' : ''}}"><a href="{{ route('lang', ['locale' => 'vi']) }}">VI</a></li>
    </ul>
</div>