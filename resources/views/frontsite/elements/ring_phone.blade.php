<div class="phone">
    <a href="tel:{{$setting->get('theme_option::general::ring_phone_number', '0707518655')}}">
        <div class="quick-alo-ph-circle"></div>
        <div class="quick-alo-ph-circle-fill"></div>
        <div class="quick-alo-ph-img-circle"></div>
    </a>
</div>