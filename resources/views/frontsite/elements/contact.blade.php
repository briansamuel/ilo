<div id="last_section" class="section">
    <div class="container">
        <div class="row align-items-end">
            <div class="col-md-6">
                <div class="title mb-4 right_to_left">
                    <!-- Ghé thăm ILO Academy nhé! -->
                    {{ __('frontsite.elements.contact.title') }}
                </div>
                <div class="description mb-4 left_to_right">
                    <!-- “Vui lòng liên hệ khi bạn cần thêm thông tin chi tiết.” -->
                    {{ __('frontsite.elements.contact.description') }}
                </div>
                <div class="hotline mb-4 right_to_left">
                    <!-- <strong>Hotline:</strong> 0707 518 655 -->
                    <strong>Hotline:</strong> 0707 518 655
                </div>
                <div class="email mb-4 right_to_left">
                    <strong>Email:</strong> ilo@academy.com
                </div>
                <div class="form_contact right_to_left visit">
                    <div class="title_contact">
                        <!-- Đặt lịch tham quan: -->
                        {{ __('frontsite.elements.contact.title_contact') }}
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <input type="hidden" name="content" id="content" value="" />
                            <input name="name" id="name" type="text" class="form-control" placeholder="{{ __('frontsite.elements.contact.name') }}">
                        </div>
                        <div class="col-md-6">
                            <input name="email" id="email" type="text" class="form-control" placeholder="{{ __('frontsite.elements.contact.email') }}">
                        </div>
                        <div class="col-md-6">
                            <input name="phone_number" id="phone_number" type="text" class="form-control" placeholder="{{ __('frontsite.elements.contact.phone') }}">
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="date_picker" name="date_picker" class="form-control" placeholder=" {{ __('frontsite.elements.contact.date') }}">
                        </div>
                        <div class="col-md-3 col-4 div_captcha_visit">
                            {!! captcha_img('inverse') !!}
                        </div>
                        <div class="col-md-9 col-8">
                            <input name="captcha" id="captcha" type="text" placeholder="{{ __('frontsite.elements.contact.type_captcha') }}" class="form-control">
                        </div>
                    </div>
                    <div class="form-group" style="display:none" id="show_error">
                        <div>
                            <span class="text text-danger" id="content_error"></span>
                        </div>
                    </div>
                    <div class="form-group" style="display:none" id="show_success">
                        <div>
                            <span class="text text-success" id="content_success"></span>
                        </div>
                    </div>
                    <div class="wrapper_submit">
                        <button onclick="sendContactVisit()" type="button" class="submit_button"> {{ __('frontsite.elements.contact.comfirm') }}</button>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="map_img right_to_left">
                    <img src="assets/images/map.png" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    function checkSendContactVisit(){
        var email = $(".visit #email").val();
        var name = $(".visit #name").val();
        var phone_number = $(".visit #phone_number").val();
        var captcha = $(".visit #captcha").val();
        if(name === ""){
            $(".visit #show_error").show();
            $(".visit #content_error").text("Bạn chưa nhập họ tên !");
            $(".visit #name").focus();
            return false;
        }
        if(captcha === ""){
            $(".visit #show_error").show();
            $(".visit #content_error").text("Bạn chưa nhập captcha!");
            $(".visit #captcha").focus();
            return false;
        }
        if(email === ""){
            $(".visit #show_error").show();
            $(".visit #content_error").text("Bạn chưa nhập Email !");
            $(".visit #email").focus();
            return false;
        }
        if(!validateEmail(email)){
            $(".visit #show_error").show();
            $(".visit #content_error").text("Email không đúng định dạng !");
            $(".visit #email").focus();
            return false;
        }
        if(phone_number === ""){
            $(".visit #show_error").show();
            $(".visit #content_error").text("Bạn chưa nhập số điện thoại !");
            $(".visit #phone_number").focus();
            return false;
        }

        return true;
    }

    function sendContactVisit(){
        if(checkSendContactVisit()){
            //display notice
            $(".advisory #btn_contact").text("Đang xử lý....");
            $(".advisory #btn_contact").removeAttr("onclick");

            $.post('/send-contact', {
                email: $(".visit #email").val(),
                phone_number: $(".visit #phone_number").val(),
                date_picker: $(".visit #date_picker").val(),
                content: $(".visit #content").val(),
                type: 'visit',
                name: $(".visit #name").val(),
                captcha: $(".visit #captcha").val(),
                _token: "{{csrf_token()}}"
            }, function(res) {
                $(".visit #btn_contact").text("Xác nhận");
                $(".visit #btn_contact").attr("onclick", "return sendContact();");
                if (res.success) {
                    $(".visit #show_error").hide();
                    $(".visit #show_success").show();
                    $(".visit #content_success").text(res.message);
                    clear_form_visit();
                    return false;
                } else {
                    $(".visit #show_success").hide();
                    $(".visit #show_error").show();
                    $(".visit #content_error").text(res.error.message);
                    reset_captcha_visit();
                    return false;
                }
            }).fail(function() {
                $(".visit #show_success").hide();
                $(".visit #btn_contact").text("Xác nhận");
                $(".visit #btn_contact").attr("onclick", "return sendContact();");
                $(".visit #show_error").hide();
                $(".visit #content_error").text("");
                alert('Hệ thống gặp lỗi, vui lòng thử lại sau.');
                return false;
            });
        }
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function reset_captcha_visit(){
        $.ajax({
            type:'GET',
            url:'refresh-captcha',
            success:function(data){
                $(".visit .div_captcha_visit").html(data.captcha);
            }
        });
    }

    function clear_form_visit() {
        $(".visit #email").val('');
        $(".visit #name").val('');
        $(".visit #date_picker").val('');
        $(".visit #phone_number").val('');
        $(".visit #captcha").val('');
    }

</script>