@if(isset($section3))
<div class="section">
    <div class="img_bg opt-50">
        <img src="{{isset($section3->background) ? $section3->background : ''}}" class="img-fluid" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="title mb-4 left_to_right">
                    <strong>
                        ILO Academy
                    </strong> <br>
                    {{isset($section3->title) ? $section3->title : ''}}
                    @if(isset($section3->description))
                    <div class="fs-25">({{isset($section3->description) ? $section3->description : ''}})</div>
                    @endif
                </div>
                <div class="description mb-4 right_to_left">
                    {{isset($section3->content) ? $section3->content : ''}}
                </div>
            </div>
        </div>
    </div>
</div>
@endif