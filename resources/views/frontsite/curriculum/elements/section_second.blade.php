@if(isset($section2))
<div class="section">
    <div class="content_bottom">
        <div class="container">
            @if(isset($section2->slider))
            <div class="slider_img mb-4">
                @foreach($section2->slider as $slider)
                <div class="item">
                    <img src="{{isset($slider->image) ? $slider->image : ''}}" class="img-fluid" alt="">
                </div>
                @endforeach
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="slider_description mb-3">
                        @foreach($section2->slider as $slider)
                            <div class="item text-center">
                                <div class="title_item">
                                    <strong>{!! isset($slider->title) ? $slider->title : '' !!}</strong>
                                </div>
                                {!! isset($slider->content) ? $slider->content : '' !!}
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endif