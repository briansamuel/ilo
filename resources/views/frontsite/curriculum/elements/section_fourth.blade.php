@if(isset($section4))
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="slider_skill">
                    @foreach($section4->slider as $key => $slider)
                    <div class="item">
                        <img src="{{isset($slider->image) ? $slider->image : ''}}" class="img-fluid" alt="">
                        <div class="meta_info">
                            <div class="number">
                                {{str_pad($key + 1, 2, "0", STR_PAD_LEFT)}}
                            </div>
                            <div class="title_slider">
                                {{isset($slider->title) ? $slider->title : ''}}
                            </div>
                            <div class="description_slider">
                                {!! $slider->content !!}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="bottom_slider">
        @foreach($section4->slider as $slider)
        <div class="item">
            <img src="{{isset($slider->thumbnail) ? $slider->thumbnail : ''}}" class="img-fluid" alt="">
        </div>
        @endforeach
    </div>
</div>
@endif