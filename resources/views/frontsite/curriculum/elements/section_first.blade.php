@if(isset($section1))
<div id="section_first" class="section">
    <div class="content_middle">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="title mb-4 left_to_right">
                        <strong>ILO Academy</strong><br>
                        {{isset($section1->title) ? $section1->title : ''}}
                    </div>
                    <div class="description mb-4 right_to_left">
                        {!!isset($section1->description) ? $section1->description : ''!!}
                    </div>
                    <div class="more_content left_to_right">
                        {!!isset($section1->content) ? $section1->content : ''!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="img_bg w-60">
        <img src="{{isset($section1->image) ? $section1->image : ''}}" class="img-fluid" alt="">
    </div>
</div>
@endif