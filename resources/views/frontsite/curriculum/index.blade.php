@extends('frontsite.index')
@section('title', __('seo.curriculum.title'))
@section('title_ab', __('seo.curriculum.title'))
@section('keyword', __('seo.curriculum.keyword'))
@section('description', __('seo.curriculum.description'))
@section('image', __('seo.curriculum.image'))
@section('style')

@endsection
@section('content')
<div id="fullpage">
    @include('frontsite.curriculum.elements.section_first')
    @include('frontsite.curriculum.elements.section_second')
    @include('frontsite.curriculum.elements.section_third')
    @include('frontsite.curriculum.elements.section_fourth')
    @include('frontsite.elements.contact')
</div>
@endsection
@section('script')

@endsection