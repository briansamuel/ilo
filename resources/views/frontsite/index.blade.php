<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>@yield('title', __('seo.home.title')) | {{$setting->get('general::admin_appearance::site-names', __('seo.home.site-names'))}}</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('frontsite.elements.seo')
    <link rel="stylesheet" href="{{asset('')}}assets/css/normalize.min.css">
    <!--<link rel="stylesheet" href="assets/css/jquery-ui.min.css">-->
    <link rel="stylesheet" href="{{asset('')}}assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/slick.css">
    <!--Slider css-->
    <link rel="stylesheet" href="{{asset('')}}assets/css/slick-theme.css">
    <!--Slider css-->
    <link rel="stylesheet" href="{{asset('')}}assets/css/jquery.mCustomScrollbar.css">
    <!--Slider css-->
    <link rel="stylesheet" href="{{asset('')}}assets/css/animate.min.css">
    <!--Animate css-->
    <link rel="stylesheet" href="{{asset('')}}assets/css/font-awesome.min.css">
    <!--Custom css-->
    <link rel="stylesheet" href="{{asset('')}}assets/css/fullpage.min.css">
    <!--Custom css-->
    <link rel="stylesheet" href="{{asset('')}}assets/css/main.css?v=2.0">
    <!--Custom css-->
    @yield('style')
    <link rel="stylesheet" href="{{asset('')}}assets/css/customs/desktop.style.integration.css">
    <link rel="stylesheet" href="{{asset('')}}assets/css/customs/table.style.integration.css">
    <link rel="stylesheet" href="{{asset('')}}assets/css/customs/mobile.style.integration.css">
    <script src="{{asset('')}}assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

    @include('frontsite.header')

    <!-- Main Fullpage -->
    @yield('content')

    <!-- Menu Sidebar -->
    @include('frontsite.elements.menu_sidebar')
    <!-- End Menu Sidebar -->

    <!-- Menu Lang-->
    @include('frontsite.elements.menu_lang')
    <!-- End Menu Lang-->

    <!-- Ring Phone -->
    @include('frontsite.elements.ring_phone')
    <!-- End Ring Phone  -->
    
    
    @include('frontsite.footer')
    
    @yield('script')
</body>

</html>